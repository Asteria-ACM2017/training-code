#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<bitset>
using namespace std;

const int N = 5e4 + 5;
int n, m, Q, ans[N], f[N];
bitset<N> a, b;
bool p[N], q[N];

int main() {
	int T; scanf("%d", &T);
	int x;
	while (T--) {
		scanf("%d%d%d", &n, &m, &Q);
		
		a = 0; int maxa = 0;
		for (int i = 1; i <= n; i++) {
			scanf("%d", &x);
			maxa = max(maxa, x);
			a[x] = 1;
		}
		
		b = 0; int maxb = 0;
		memset(p, 0, sizeof(p));
		for (int i = 1; i <= m; i++) {
			scanf("%d", &x);
			maxb = max(maxb, x);
			for (int j = 0; j <= maxa; j += x)
				b[j] = b[j] ^ 1;
			p[x] = 1;
		}
		
		memset(q, 0, sizeof(q));
		for (int i = 1; i <= Q; i++) {
			scanf("%d", &x);
			q[x] = 1;
			f[i] = x;
		}
		
		for (int i = 0; i <= maxb; i++) {
			if (p[i]) {
				for (int j = 0; j <= maxa; j += i)
					b[j] = b[j] ^ 1;
			}
			/*for (int i = 0; i <= maxb; i++)
				printf("%d", (int) b[i]); puts("");*/
			if (q[i]) ans[i] = (int)((b << i) & a).count() & 1;
		}
		for (int i = 1; i <= Q; i++)
			printf("%d\n", ans[f[i]]);
	}
	return 0;
}
