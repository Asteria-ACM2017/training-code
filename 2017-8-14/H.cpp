#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cassert>
using namespace std;

const int N = 55;
const int M = 1e4 + 5;
int n, m, t;
long long cnt[M], now[M], tmp[M];
int ans[N];

void solve(int x, int k) {
	for (int i = 1; i <= k; i++)
		ans[++t] = x;
	for (int i = 1; i <= m; i++)
		tmp[i] = 0;
	
	int sum = 0;
	long long p = 1;
	for (int i = 1; i <= k; i++) {
		sum += x;
		p = p * (k - i + 1) / i;
		for (int j = sum; j <= m; j++)
			tmp[j] += now[j - sum] * p;
	}
	for (int i = x; i <= m; i++)
		now[i] += tmp[i];
}

int main() {
	int T; scanf("%d", &T);
	while (T--) {
		scanf("%d%d", &n, &m);
		for (int i = 0; i <= m; i++)
			scanf("%lld", &cnt[i]);
		for (int i = 0; i <= m; i++)
			now[i] = 0;
			
		t = 0; now[0] = 1;
		for (int i = 1; i <= m; i++)
			if (cnt[i] > now[i])
				solve(i, cnt[i] - now[i]);
		assert(t == n);
		for (int i = 1; i <= n; i++)
			printf("%d%c", ans[i], " \n"[i == n]);
	}
	return 0;
}
