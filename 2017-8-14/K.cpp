#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

const int N = 1e5 + 5;
int n, k, a[N];

int main() {
	int T; scanf("%d", &T);
	while (T--) {
		scanf("%d%d", &n, &k);
		for (int i = 1; i <= n; i++)
			scanf("%d", &a[i]);
		sort(a + 1, a + n + 1);
		
		int cnt = 1;
		for (int i = n - 1; i >= 1; i--) {
			if (a[i + 1] - a[i] <= k) cnt++;
			else break;
		}
		printf("%d\n", cnt);
	}
}
