#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

const int maxn = 1e6 + 5;

int T;
ll n, m, ans;
ll s[maxn];

int main()
{
	ll l, r, x, y;
	scanf("%d", &T);
	while(T--)
	{
		scanf("%lld%lld", &n, &m);
		for(int i = 1; i <= n; i++)
			s[i] = s[i - 1] + n - i;
		l = 0, r = n;
		while(l < r)
		{
			ll mid = ((l + r) >> 1) + 1;
			if(s[mid] <= m)
				l = mid;
			else
				r = mid - 1;
		}
		if(l == 0)
			ans = 2 * m * m + (n - m - 1) * n * (n + m);
		else if(l == n)
			ans = 1ll * n * (n - 1);
		else
		{
			ans = s[l] << 1;
			x = m - s[l];
			y = n - l - 1 - x;
			ans += (x << 1) + (y << 2);
			ans += (1ll * (x + y) * (x + y - 1)) << 1;
		}
		printf("%lld\n", ans);
	}
	return 0;
}
