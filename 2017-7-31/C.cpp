#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

const int maxm = 262144 + 10;
const int G = 10;
const int mod = 786433;

int m, inm, n, h;
ll f[20][maxm];
ll a[maxm], b[maxm], c[maxm], rev[maxm];

ll power(ll b, int k)
{
	ll res = 1;
	for(; k; k >>= 1, b = b * b % mod)
		if(k & 1)
			res = res * b % mod;
	return res;
}

void ntt(ll *a, int f)
{
	for(int i = 0; i < m; i++)
		if(rev[i] < i)
			swap(a[i], a[rev[i]]);
	for(int l = 2, h = 1; l <= m; h = l, l <<= 1)
	{
		ll ur;
		if(f == 1)
			ur = power(G, (mod - 1) / l);
		else
			ur = power(G, mod - 1 - (mod - 1) / l);
		for(int i = 0; i < m; i += l)
		{
			ll w = 1;
			for(int k = i; k < i + h; k++, w = w * ur % mod)
			{
				ll x = a[k], y = 1ll * a[k + h] * w % mod;
				a[k] = (x + y) % mod;
				a[k + h] = (x - y + mod) % mod;
			}
		}
	}
	if(f == -1)
	{
		for(int i = 0; i < m; i++)
			a[i] = a[i] * inm % mod;
	}
}

void cal(ll *a1, ll *b1, int N)
{
	for(m = 1; m <= 2 * N; m <<= 1);
	inm = power(m, mod - 2);
	for(int i = 0; i < m; i++)
	{
		a[i] = a1[i];
		b[i] = b1[i];
	}
	for(int i = 1; i < m; i++)
	{
		rev[i] = rev[i - 1];
		for(int j = m >> 1; (rev[i] ^= j) < j; j >>= 1);
	}
	ntt(a, 1);
	ntt(b, 1);
	for(int i = 0; i < m; i++)
		c[i] = 1ll * a[i] * b[i] % mod;
	ntt(c, -1);
	/*for (int i = 0; i < N * 2; i++) c[i] = 0;
	for (int i = 0; i < N; i++)
		for (int j = 0; j < N; j++)
			c[i + j] = (c[i + j] + a1[i] * b1[j]) % mod;*/
}

int main()
{
	scanf("%d%d", &n, &h); h++;
	memset(f, 0, sizeof(f));
	f[0][0] = 1;
	f[1][1] = 1;
	for (int  i = 2; i <= h; i++) {
		cal(f[i - 1], f[i - 1], 1 << (i - 1));
		for (int j = 1; j < (1 << i); j++)
			f[i][j] = (f[i][j] + c[j - 1]) % mod;
		cal(f[i - 1], f[i - 2], 1 << (i - 1));
		for (int j = 1; j < (1 << i); j++)
			f[i][j] = (f[i][j] + 2 * c[j - 1]) % mod;
	}
	printf("%lld\n", f[h][n]);
	return 0;
}
