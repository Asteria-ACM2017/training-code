#include <bits/stdc++.h>
using namespace std;

const int maxn = 205;

bool mp[maxn][maxn];
int n, match[maxn], q[maxn], head, tail;
int pred[maxn], base[maxn], st, fi, nbase;
bool inq[maxn], inb[maxn];
vector <int> e[maxn];

void push(int u)
{
	q[tail++] = u;
	inq[u] = 1;
}

int pop()
{
	return q[head++];
}

void ins(int u, int v)
{
	e[u].push_back(v);
	e[v].push_back(u);
}

int findcs(int u, int v)
{
	bool inp[maxn];
	for(int i = 0; i < n; i++)
		inp[i] = 0;
	while(true)
	{
		u = base[u];
		inp[u] = 1;
		if(u == st)
			break;
		u = pred[match[u]];
	}
	while(true)
	{
		v = base[v];
		if(inp[v])
			break;
		v = pred[match[v]];
	}
	return v;
}

void reset(int u)
{
	int v;
	while(base[u] != nbase)
	{
		v = match[u];
		inb[base[u]] = inb[base[v]] = 1;
		u = pred[v];
		if(base[u] != nbase)
			pred[u] = v;
	}
}

void blossom(int u, int v)
{
	nbase = findcs(u, v);
	for(int i = 0; i < n; i++)
		inb[i] = 0;
	reset(u);
	reset(v);
	if(base[u] != nbase)
		pred[u] = v;
	if(base[v] != nbase)
		pred[v] = u;
	for(int i = 0; i < n; i++)
		if(inb[base[i]])
		{
			base[i] = nbase;
			if(!inq[i])
				push(i);
		}
}

bool findpath(int u)
{
	bool found = 0;
	for(int i = 0; i < n; i++)
	{
		pred[i] = -1, base[i] = i;
		inq[i] = 0;
	}
	st = u, fi = -1;
	head = tail = 0;
	push(st);
	while(head < tail)
	{
		int u = pop();
		for(int i = e[u].size() - 1; i >= 0; i--)
		{
			int v = e[u][i];
			if(base[u] != base[v] && match[u] != v)
			{
				if(v == st || (match[v] >= 0 && pred[match[v]] >= 0))
					blossom(u, v);
				else if(pred[v] == -1)
				{
					pred[v] = u;
					if(match[v] >= 0)
						push(match[v]);
					else
					{
						fi = v;
						return 1;
					}				
				}
			}
		}
	}
	return found;
}

void path()
{
	int u = fi, v, w;
	while(u >= 0)
	{
		v = pred[u];
		w = match[v];
		match[v] = u;
		match[u] = v;
		u = w;
	}
}

int maxmatching()
{
	int res = 0;
	for(int i = 0; i < n; i++)
		match[i] = -1;
	for(int i = 0; i < n; i++)
		if(match[i] == -1)
			if(findpath(i))
				path();
	for(int i = 0; i < n; i++)
		if(match[i] != -1)
			res++;
	return res;
}

int main()
{
	int x, y;
	scanf("%d", &n);
	for(int i = 0; i < n; i++)
	{
		scanf("%d", &x);
		for(int j = 1; j <= x; j++)
		{
			scanf("%d", &y);
			mp[i][y - 1] = 1;
		}
	}
	for(int i = 0; i < n; i++)
		for(int j = i + 1; j < n; j++)
			if(mp[i][j] && mp[j][i])
				ins(i, j);
	if(maxmatching() == n)
		puts("YES");
	else
		puts("NO");
	return 0;
}
