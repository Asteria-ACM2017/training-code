#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

int n, a, b, ans, ansx, ansy;

int main() {
	scanf("%d%d%d", &n, &a, &b);
	ans = 0; ansx = ansy = 1;
	for (int x = 1; x <= n; x++)
		for (int y  = x & (x - 1); y; (--y) &= x) {
			int t = (a * x + b * y) ^ (a * y + b * x);
			//printf("%d %d %d\n", x, y, t);
			if (t > ans) {
				ans = t;
				ansx = x;
				ansy = y;
			}
		}
	printf("%d %d\n", ansx, ansy);
}
