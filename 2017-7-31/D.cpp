#include <bits/stdc++.h>
using namespace std;

#define mp make_pair

typedef pair <int, int> pii;

const int maxn = 3005;
const int maxm = maxn << 1;

int n, m, cnt, now;
int cost[10], dx[5], dy[5];
int f[maxn][maxn], g[maxn], pos[maxn], op[maxn][maxn], ans[maxm];
char a[maxn], b[maxn], opt[5] = {0, 'B', 'I', 'D', 'C'};
pii pre[maxn][maxn], v[maxm];

void update(int u, int v, int i, int j, int k)
{
	if(i < 0 || j < 0)
		return;
	if(f[u][v] > f[i][j] + cost[k])
	{
		f[u][v] = f[i][j] + cost[k];
		pre[u][v] = mp(i, j);
		op[u][v] = k;
	}
}

void print(int x, int y)
{
	if(!x && !y)
		return;
	if(op[x][y] > 1)
	{
		ans[++now] = op[x][y];
		print(pre[x][y].first, pre[x][y].second);
	}
	else
	{
		cnt++;
		v[cnt] = mp(pre[x][y].first + 1, x);
		print(pre[x][y].first, y);
	}
}

int main()
{
	dx[4] = dy[4] = dy[2] = dx[3] = 1;
	for(int i = 1; i <= 4; i++)
		scanf("%d", &cost[i]);
	scanf("%s%s", a + 1, b + 1);
	n = strlen(a + 1);
	m = strlen(b + 1);
	memset(f, 127, sizeof(f));
	memset(g, 127, sizeof(g));
	f[0][0] = 0;
	g[0] = 0;
	for(int j = 0; j <= m; j++)
		for(int i = 0; i <= n; i++)
		{
			if(i + j == 0)
				continue;
			for(int k = 2; k <= 3; k++)
			{
				update(i, j, i - dx[k], j - dy[k], k);
			}
			if(a[i] == b[j])
				update(i, j, i - dx[4], j - dy[4], 4);
			if(f[i][j] > g[j] + cost[1])
				{
					f[i][j] = g[j] + cost[1];
					pre[i][j] = mp(pos[j], i);
					op[i][j] = 1;
				}
			if(f[i][j] < g[j])
			{
				g[j] = f[i][j];
				pos[j] = i;
			}
		}
	printf("%d\n", f[n][m]);
	print(n, m);
	printf("%d\n", cnt);
	for(int i = cnt; i >= 1; i--)
		printf("%d %d\n", v[i].first, v[i].second);
	for(int i = now; i >= 1; i--)
		printf("%c", opt[ans[i]]);
	return 0;
}
