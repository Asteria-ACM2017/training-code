#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;
const int maxn = 1e5 + 10;
const int INF = 1e9;
int tot = 0, ans = 0;
struct Stack {
	int flag;
	char ch;
	int id;
}stack[maxn];
struct node {
	int m[3][3];
}a[maxn];
char s[maxn];
node merge(char ch, node u, node v) {
	node ret;
	ret.m[0][0] = 0;
	ret.m[0][1] = 1;
	ret.m[1][0] = 1;
	ret.m[1][1] = -INF;
	if(ch == 'S') {
		for (int i = 0; i <= 1; i++) {
			for(int j = 0; j <= 1; j++) {
				for (int k = 0; k <= 1; k++) {
					ret.m[i][k] = max(ret.m[i][k], u.m[i][j] + v.m[j][k] - (j == 1));
				}
			}
		}
	}
	if(ch == 'P') {
		for (int i = 0; i <= 1; i++) {
			for (int j = 0; j <= 1; j++) {
				ret.m[i][j] = max(ret.m[i][j], u.m[i][j] + v.m[i][j] - (j == 1) - (i == 1));
			}	
		}
	}
	return ret;
}
int main() {

	scanf("%s", s + 1);
	int n = strlen(s + 1);
	int top = 0;
	for (int i = 1; i <= n; i++) {
		if(s[i] == 'g') {
			tot++;
			a[tot].m[0][0] = 0;
			a[tot].m[0][1] = 1;
			a[tot].m[1][0] = 1;
			a[tot].m[1][1] = -INF;
			stack[++top].flag = 1;
			stack[top].id = tot;
		}
		else {
			stack[++top].flag = 0;
			stack[top].ch = s[i];
		} 
		while(top > 2 && stack[top].flag && stack[top - 1].flag && !stack[top - 2].flag) {
			a[++tot] = merge(stack[top - 2].ch, a[stack[top - 1].id], a[stack[top].id]);
			top -= 3;
			stack[++top].flag = true;
			stack[top].id = tot;
		}
	}
	for (int i = 1; i <= top; i++) {
		for (int j = 0; j <= 1; j++) {
			for (int k = 0; k <= 1; k++)
				ans = max(ans, a[stack[top].id].m[j][k]);
		}
		
	}
	printf("%d\n", ans);
	return 0;
}
