#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

const int inf = 1e9 + 7;
const int N = 305;

int n, m, f[N][N], g[N][N];

int main() {
	scanf("%d%d", &n, &m);
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++) {
			f[i][j] = (i == j) ? 0 : inf;
			g[i][j] = (i == j) ? 0 : 1;
		}
	for (int i = 1; i <= m; i++) {
		int x, y, z;
		scanf("%d%d%d", &x, &y, &z);
		f[x][y] = f[y][x] = min(f[x][y], z);
	}
	
	for (int k = 1; k <= n; k++)
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= n; j++) {
				if (f[i][k] + f[k][j] < f[i][j] || f[i][k] + f[k][j] == f[i][j] && g[i][k] + g[k][j] < g[i][j]) {
					f[i][j] = f[i][k] + f[k][j];
					g[i][j] = g[i][k] + g[k][j];
				}
			}
	
	int ans = 0;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++)
			ans += g[i][j];
	printf("%.10f\n", 1.0 * ans / n / (n - 1));
	return 0;
}
