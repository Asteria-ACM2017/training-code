#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<vector>
using namespace std;

const int N = 1e5 + 5;

int n, x[N], y[N], z[N], p[N];
int c[N], id[N], pos[N];
bool b[N];

bool cmp(int i, int j) {
	return z[i] < z[j] || z[i] == z[j] && p[i] < p[j];
}

void add(int x, int k) {
	for (; x <= n + 1; x += x & (-x))
		c[x] += k;
}
int getsum(int x) {
	int res = 0;
	for (; x; x -= x & (-x))
		res += c[x];
	return res;
}
int getsum(int p, int q) {
	if (p > q) {
		int t = p; p = q; q = t;
	}
	p++; q--;
	if (p > q) return 0;
	return getsum(q) - getsum(p - 1);
}

void solve() {
	sort(id, id + n + 1, cmp);
	memset(c, 0, sizeof(c));
	for (int i = 0; i <= n; i++) {
		add(i + 1, 1);
		pos[id[i]] = i + 1;
	}
	for (int i = 1; i <= n; i++) {
		add(pos[i - 1], -1);
		if (z[i] != z[i - 1]) continue;
		if (getsum(pos[i], pos[i - 1]) == 0) b[i] = 1;
	}
}

int main() {
	scanf("%d", &n);
	x[0] = y[0] = 0;
	for (int i = 1; i <= n; i++)
		scanf("%d%d", &x[i], &y[i]);
	
	for (int i = 0; i <= n; i++) b[i] = 0;
	for (int i = 0; i <= n; i++) id[i] = i;
	
	for (int i = 0; i <= n; i++)
		z[i] = x[i], p[i] = y[i];
	solve();
	for (int i = 0; i <= n; i++)
		z[i] = y[i], p[i] = x[i];
	solve();
	for (int i = 0; i <= n; i++)
		z[i] = x[i] + y[i], p[i] = x[i];
	solve();
	for (int i = 0; i <= n; i++)
		z[i] = x[i] - y[i], p[i] = x[i];
	solve();
	
	int ans = 0;
	for (int i = 1; i <= n; i++)
		if (b[i]) ans = i;
		else break;
	printf("%d\n", ans);
	return 0;
}
