#include <bits/stdc++.h>
using namespace std;

#define pii pair<int, int>

const int maxn = 405;
const int inf = 1e9;

int n, m;
int N, M, S;
int a[maxn][maxn];
int mp[maxn][maxn];
int x[maxn], y[maxn], match[maxn], slack[maxn], f[maxn], deg[maxn];
pii c[maxn], b[maxn];
bool px[maxn], py[maxn];
vector <int> e[maxn];

bool find(int k)
{
	if(k == 0)
		return 0;
	px[k] = 1;
	for(int i = 1; i <= m; i++)
		if(!py[i])
		{
			int t = x[k] + y[i] - a[k][i];
			if(!t)
			{
				py[i] = 1;
				if(!match[i] || find(match[i]) == 1)
				{
					match[i] = k;
					return 1;
				}
			}
			else
				slack[i] = min(slack[i], t);
		}
	return 0;
}

void revise()
{
	int d = inf;
	for(int i = 1; i <= m; i++)
		if(!py[i])
			d = min(d, slack[i]);
	for(int i = 1; i <= n; i++)
		if(px[i])
			x[i] -= d;
	for(int i = 1; i <= m; i++)
		if(py[i])
			y[i] += d;
		else
			slack[i] -= d;
}

void solve()
{
	for(int i = 1; i <= n; i++)
	{
		for(int j = 1; j <= m; j++)
			slack[j] = inf;
		while(true)
		{
			memset(px, 0, sizeof(px));
			memset(py, 0, sizeof(py));
			if(find(i) == 1)
				break;
			revise();
		}
	}
	for(int i = 1; i <= m; i++)
		if(match[i])
			f[match[i]] = i;
}

int dis(pii a, pii b)
{
	return abs(a.first - b.first) + abs(a.second - b.second);		
}

int val(pii a, pii b)
{
	return dis(a, b) * 10000 + b.first * 100 + b.second;
}

void ins(int u, int v)
{
//	printf("%d %d\n", u, v);
	e[u].push_back(v);
	deg[v]++;
}

void topsort()
{
	queue <int> q;
	for(int i = 1; i <= n; i++)
		if(!deg[i])
			q.push(i);
	while(!q.empty())
	{
		int u = q.front();
		q.pop();
		printf("%d ", u);
		for(int i = e[u].size() - 1; i >= 0; i--)
		{
			int v = e[u][i];
			deg[v]--;
			if(deg[v] == 0)
				q.push(v);
		}
	}
}

int main()
{
	int X, Y;
	scanf("%d%d", &N, &M);
	scanf("%d%d", &n, &S);
	m = N * M - n - S;
	for(int i = 1; i <= n; i++)
	{
		scanf("%d%d", &X, &Y);
		mp[X][Y] = 1;
		b[i] = make_pair(X, Y);
	}
	for(int i = 1; i <= S; i++)
	{
		scanf("%d%d", &X, &Y);
		mp[X][Y] = 2;
	}
	int now = 0;
	for(int i = 1; i <= N; i++)
		for(int j = 1; j <= M; j++)
			if(!mp[i][j])
			{
				c[++now] = make_pair(i, j);
			}
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= m; j++)
			a[i][j] = -val(b[i], c[j]);
	solve();
	for(int i = 1; i <= n; i++)
	{
		int val = a[i][f[i]];
//		printf("%d ", val);
//		puts("");
		for(int j = 1; j <= m; j++)
			if(a[i][j] > val)
				if(match[j])
					ins(match[j], i);
	}	
	topsort();
	return 0;
}
