#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cmath>
#include<cassert>
using namespace std;

const double pi = acos(-1.0);
const double eps = 1e-6;

struct point {
	double x, y;
	point(double x = 0, double y = 0) : x(x), y(y) {}
	point operator + (const point &rhs) const {
		return point(x + rhs.x, y + rhs.y);
	}
	point operator - (const point &rhs) const {
		return point(x - rhs.x, y - rhs.y);
	}
	point operator * (const double &k) const {
		return point(x * k, y * k);
	}
	point operator / (const double &k) const {
		return point(x / k, y / k);
	}
	double len2() {
		return x * x + y * y;
	}
	double len() {
		return sqrt(len2());
	}
};
double dot(const point &a, const point &b) {
	return a.x * b.x + a.y * b.y;
}
double det(const point &a, const point &b) {
	return a.x * b.y - a.y * b.x;
}

struct line {
	point a, b;
	line(point a, point b) : a(a), b(b) {}
};
point isLL(const line &l1, const line &l2) {
	double s1 = det(l2.b - l2.a, l1.a - l2.a);
	double s2 = -det(l2.b - l2.a, l1.b - l2.a);
	return (l1.a * s2 + l1.b * s1) / (s1 + s2);
}
point projection(const line &l, const point &p) {
	return l.a + (l.b - l.a) * (dot(p - l.a, l.b - l.a) / (l.b - l.a).len2());
}
point symmetry(const point &a, const point &b) {
	return a + a - b;
}
point reflection(const line &l, const point &p) {
	return symmetry(projection(l, p), p);
}
double point_to_segment(const point &p, const line &l) {
	if (dot(l.a - p, l.b - l.a) * dot(l.b - p, l.b - l.a) < eps)
		return fabs(det(l.b - l.a, p - l.a)) / (l.b - l.a).len();
	return min((l.a - p).len(), (l.b - p).len());
}


int r, w, l, h;

point calc(const point &a, const point &b) {
	return point(a + (a - b) / (a - b).len() * 2 * r);
}

bool check(const point &p) {
	return !(p.x > r && p.x < w - r && p.y > r && p.y < l - r);            	 
}

bool bump(const point &p, const line &l) {
	return point_to_segment(p, l) < r * 2;
}

void print(const point &a, const point &b) {
	printf("%.2f ", a.x);
	double t = dot(b - a, point(1, 0)) / (b - a).len();
	printf("%.2f\n", acos(t) / pi * 180);
}

int main() {
	int x1, x2, x3, y1, y2, y3;
	point a1, a2, a3, p0, p1, p, u, v;
	scanf("%d%d", &w, &l);
	scanf("%d%d%d%d%d%d%d%d", &r, &x1, &y1, &x2, &y2, &x3, &y3, &h);
	a1 = point(x1, y1);
	a2 = point(x2, y2);
	a3 = point(x3, y3);
	
	p1 = calc(a3, point(w, l));
	p0 = calc(a2, point(0, l));
	if ((a1 - p1).len2() < eps) p = a1 + a1 - a3; else p = calc(a1, p1);
	if (check(p1) || check(p0) || check(p) || bump(a2, line(a1, p1)) || bump(a3, line(p, p0))) {
		puts("impossible");
		return 0;
	} 
	
	u = a1 - p;
	v = p0 - p;
	//printf("%.2f %.2f %.2f\n", v.x, v.y, v.len2());
	assert(v.len2() > eps);
	if (dot(u, v) > -eps || dot(a2 - p0, p0 - p) < eps || dot(a3 - p1, p1 - a1) < eps || (p - p0).len2() < eps) {
		puts("impossible");
		return 0;
	}

	point tmp = reflection(line(a1, p), p0);
	point ans = isLL(line(point(0, h), point(1, h)), line(p, tmp));
	
	if (dot(tmp - p, ans - p) > eps && ans.x > r && ans.x < w - r && !bump(a2, line(ans, p)) && !bump(a3, line(ans, p)))
		print(ans, p);
	else puts("impossible");
	
	return 0;
}
