#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

const int N = 105;
const int dx[4] = {0, 0, -1, 1};
const int dy[4] = {-1, 1, 0, 0};

int n, m, a[N][N], rst, b[N][N];
char s[N];

int main() {
	scanf("%d%d", &n, &m);
	memset(a, 0, sizeof(a));
	rst = n * m;
	for (int i = 1; i <= n; i++) {
		scanf("%s", s + 1);
		for (int j = 1; j <= m; j++)
			if (s[j] == '.') a[i][j] = 0, rst--;
			else a[i][j] = -1;
	}
	
	int now = 0;
	while (rst) {
		now++;
		for (int i = 0; i <= n + 1; i++)
			for (int j = 0; j <= m + 1; j++)
				b[i][j] = a[i][j];
		for (int i = 0; i <= n + 1; i++)
			for (int j = 0; j <= m + 1; j++) {
				if (b[i][j] != -1) {
					for (int k = 0; k < 4; k++) {
						int x = i + dx[k];
						int y = j + dy[k];
						if (x < 1 || x > n || y < 1 || y > m) continue;
						if (a[x][y] == -1) a[x][y] = now, rst--;
					}
				}
			}
	}
	
	if (now < 10) {
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= m; j++)
				if (a[i][j] == 0) printf("..");
				else printf(".%d", a[i][j]);
			printf("\n");
		}
	} else {
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= m; j++)
				if (a[i][j] == 0) printf("...");
				else if (a[i][j] < 10) printf("..%d", a[i][j]);
				else printf(".%d", a[i][j]);
			printf("\n");
		}
	}
	return 0;
}
