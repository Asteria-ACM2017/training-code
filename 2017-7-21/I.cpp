#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

const int N = 12;
int n, now;
char a[N][N], s[N * N], ans[N][N], b[N][N];

bool work() {
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++)
			b[j][n - i + 1] = a[i][j];
	
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++)
			if (a[i][j] == '.') {
				now++;
				if (ans[i][j] && ans[i][j] != s[now]) return 0;
				ans[i][j] = s[now];
			}
	
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++)
			a[i][j] = b[i][j];
	
	return 1;
}

bool solve() {
	memset(ans, 0, sizeof(ans));
	if (!work()) return 0;
	if (!work()) return 0;
	if (!work()) return 0;
	if (!work()) return 0;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= n; j++)
			if (ans[i][j] == 0) return 0;
	return 1;
}

int main() {
	scanf("%d", &n);
	for (int i = 1; i <= n; i++)
		scanf("%s", a[i] + 1);
	scanf("%s", s + 1);
	
	now = 0;
	if (!solve()) puts("invalid grille");
	else {
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= n; j++)
				printf("%c", ans[i][j]);
		}
	} 
	return 0;
}
