#include <bits/stdc++.h>
using namespace std;

typedef unsigned long long ll;

const int maxn = 30;

int n, m;
bool vst[maxn][maxn];
ll f[maxn][maxn];

bool check(int x, int y, int s)
{
    if(x > n || y > 3)
	return 0;
    if(vst[x][y])
	return 0;
    if(s & (1 << (y - 1)))
	return 0;
    return 1;
}

void dfs(int k, int s, int t, ll val, int r)
{
    if(k > 3)
    {
	f[r][t] += val;
	return;
    }
    dfs(k + 1, s, t, val, r);
    if(!check(r, k, s))
	return;
    if(check(r, k + 1, s))
	dfs(k + 2, s, t, val, r);
    if(check(r + 1, k, t))
    	dfs(k + 1, s, t | (1 << (k - 1)), val, r);
}

int main()
{
    double x, y;
    scanf("%d%d", &n, &m);
    for(int i = 1; i <= m; i++)
    {
	scanf("%lf%lf", &x, &y);
	x++, y++;
	vst[(int)x][(int)y] = 1;
    }
    f[0][0] = 1;
    for(int i = 0; i < n; i++)
	for(int s = 0; s < 8; s++)
	    if(f[i][s])
		dfs(1, s, 0, f[i][s], i + 1);
    //cout << f[n][0];
    printf("%llu\n", f[n][0]);
    return 0;
}
