#include <bits/stdc++.h>
using namespace std;

#define pii pair <int, int>

const int maxn = 20;

int n, m, tar, ans, s0;
int vst[2][1500], bin[1500], l[2][maxn], r[2][maxn], b[maxn][maxn], c[maxn][maxn];
char op[5];
bool p[maxn], st[2][maxn];
pii a[maxn];
int pn[maxn];

void dfs1(int k, int tar)
{
    if(k == m)
    {
		//if(tar == 0)ans ++;
		
		int x = a[k].first, y = a[k].second;
		if(tar <= n && tar > 0)
		//printf("%d %d %d\n", tar, ((1 << tar) & vst[0][x]), ((1 << tar) & vst[1][y]));
		ans += ((((1 << tar) & vst[0][x]) == 0) && (((1 << tar) & vst[1][y]) == 0));
		return;
    }
    /*if(tar > r[0][k])
		return;
    if(tar < l[0][k])
		return;*/
    int x = a[k].first, y = a[k].second;
    int t = (vst[0][x] ^ s0) & (vst[1][y] ^ s0);
    int L = max(tar - (m - k) * n, 1), R = min(n, tar - (m - k));
    for(int j = t & -t; t; t -= j, j = t & -t)
    {
    	
		int i = bin[j];
		if(i < L || i > R) continue;
		if(i > tar)
	   		return;
		vst[0][x] |= j;
		vst[1][y] |= j;
		dfs1(k + 1, tar - i);
		vst[0][x] ^= j;
		vst[1][y] ^= j;
    }
}

void dfs2(int k, int tar)
{
    if(k == m)
    {
		int x = a[k].first, y = a[k].second;
		if(tar <= n && tar > 0)
		//printf("%d %d %d\n", tar, ((1 << tar) & vst[0][x]), ((1 << tar) & vst[1][y]));
		ans += ((((1 << tar) & vst[0][x]) == 0) && (((1 << tar) & vst[1][y]) == 0));
		return;
    }   
    int L = max(1, tar / pn[m - k]), R = min(n, tar);
    int x = a[k].first, y = a[k].second;
    int t = (vst[0][x] ^ s0) & (vst[1][y] ^ s0);
    for(int j = t & -t; t; t -= j, j = t & -t)
    {
	int i = bin[j];
	if(tar % i == 0)
	{
	    vst[0][x] |= j;
	    vst[1][y] |= j;
	    dfs2(k + 1, tar / i);
	    vst[0][x] ^= j;
	    vst[1][y] ^= j;
	}
    }
}

int main()
{
//    freopen("c.in", "r", stdin);
    for(int i = 1; i <= 9; i++)
	bin[1 << i] = i;
	pn[0] = 1;
    scanf("%d%d%d%s", &n, &m, &tar, op);
    for (int i = 1; i <= 10; i++) {
		pn[i] = pn[i - 1] * n;
	}
    s0 = (1 << (n + 1)) - 2;
    for(int i = 1; i <= m; i++)
	scanf("%d%d", &a[i].first, &a[i].second);
    sort(a + 1, a + 1 + m);
    l[1][m + 1] = r[1][m + 1] = 1;
    for(int i = m, mn = 1, mx = n; i >= 1; i--, mn = 1, mx = n)
    {
	memset(st, 0, sizeof(st));
	int x = a[i].first;
	int y = a[i].second;
	for(int u = 1; u <= n; u++)
	{
	    st[0][b[x][u]] = 1;
	    st[0][b[u][y]] = 1; //mx
	    st[1][c[x][u]] = 1;
	    st[1][c[u][y]] = 1; //mn
	}
	for(mx = n; st[0][mx]; mx--);
	for(mn = 1; st[1][mn]; mn++);
	
/*	if(a[i].first != a[i + 1].first)
	    mn = 1, mx = n;
	else
	    mn++, mx--;*/
	b[x][y] = mx;
	c[x][y] = mn;
//	printf("%d %d\n", mn, mx);
	l[0][i] = l[0][i + 1] + mn;
	r[0][i] = r[0][i + 1] + mx;
	l[1][i] = l[1][i + 1] * mn;
	r[1][i] = r[1][i + 1] * mx;
    }
    
    if(op[0] == '-')
    {
	for(int i = 1; i <= n; i++)
	    for(int j = 1; j < i; j++)
		if(i - j == tar)
		    ans += 2;
    }
    else if(op[0] == '/')
    {
	for(int i = 1; i <= n; i++)
	    for(int j = 1; j < i; j++)
		if(i % j == 0 && i / j == tar)
		    ans += 2;
    }
    else if(op[0] == '+')
	dfs1(1, tar);
    else
	dfs2(1, tar);
    printf("%d\n", ans);
    return 0;
}
