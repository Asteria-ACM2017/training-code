#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

const int maxn = 105;
const int maxm = maxn * maxn;

int n, m, s, t, cnt, now;
int tail[maxn];
ll ans;
ll f[2][maxn];
struct edge
{
    int v, next;
}e[maxm];

void ins(int u, int v)
{
    e[++cnt] = (edge){v, tail[u]};
    tail[u] = cnt;
}

int main()
{
    int u, v;
    scanf("%d%d%d%d", &n, &m, &s, &t);
    for(int i = 1; i <= m; i++)
    {
	scanf("%d%d", &u, &v);
	u++, v++;
	ins(u, v);
	ins(v, u);
    }
    f[now][s + 1] = 1;
    for(int k = 1; k <= t; k++)
    {
	memset(f[now ^ 1], 0, sizeof(f[0]));
	for(int u = 1; u <= n; u++)
	    if(f[now][u])
		for(int i = tail[u]; i; i = e[i].next)
		    f[now ^ 1][e[i].v] += f[now][u];
	now ^= 1;
    }
    for(int i = 1; i <= n; i++)
    {
	ans += f[now][i];
    }
    printf("%lld\n", ans);
    return 0;
}
