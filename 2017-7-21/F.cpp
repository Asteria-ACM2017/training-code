#include <bits/stdc++.h>
using namespace std;

const int inf = 1e6;
const int maxn = 4000;
const int maxm = 1300000;

int S, T, cnt = 1;
int tail[maxn], cur[maxn], d[maxn];
map<string, int> mp;
struct edge
{
    int v, c, next;
}e[maxm];

void ins(int u, int v, int c)
{
    e[++cnt] = (edge){v, c, tail[u]};
    tail[u] = cnt;
    e[++cnt] = (edge){u, 0, tail[v]};
    tail[v] = cnt;
}

int dfs(int u, int flow)
{
    if(u == T)
	return flow;
    int used = 0;
    for(int &i = cur[u]; i; i = e[i].next)
		if(d[e[i].v] == d[u] + 1 && e[i].c > 0)
		{
			int w = dfs(e[i].v, min(e[i].c, flow - used));
			e[i].c -= w;
			e[i ^ 1].c += w;
			used += w;
			if(flow == used)
			return flow;
		}
    if(!used)
		d[u] = -1;
    return used;
}

bool bfs()
{
    queue <int> q;
    memset(d, -1, sizeof(d));
    d[S] = 0;
    q.push(S);
    while(!q.empty())
    {
		int u = q.front();
		q.pop();
		for(int i = tail[u]; i; i = e[i].next)
			if(d[e[i].v] == -1 && e[i].c > 0)
			{
				d[e[i].v] = d[u] + 1;
				q.push(e[i].v);
			}
    }
    return d[T] != -1;
}

int dinic()
{
    int res = 0;
    while(bfs())
    {
		memcpy(cur, tail, sizeof(tail));
		res += dfs(S, inf);
    }
    return res;
}


int main()
{
	int s, r, f, t;
    scanf("%d%d%d%d", &s, &r, &f, &t);
    S = 1; T = 2;
    int mpid = 2, rs, rt, fs, ft, ts;
    rs = 3; rt = 2 + r;
    fs = rt + 1; ft = rt + f;
    ts = s + 3;
    
    char str[100000];
    string st;
    for (int i = 1; i <= r; i++) {
    	scanf("%s", str);
    	st = str;
    	mp[st] = ++mpid;
    	ins(S, mpid, 1);
    }
    for (int i = 1; i <= f; i++) {
    	scanf("%s", str);
    	st = str;
    	mp[st] = ++mpid;
    	ins(mpid, T, 1);
    }
    for (int i = 1; i <= t; i++) {
    	int k;
    	scanf("%d", &k);
    	int id0 = ts - 1 + i * 2 - 1;
    	int id1 = ts - 1 + i * 2;
    	ins(id0, id1, 1);
    	for (int j = 1; j <= k; j++) {
    		scanf("%s", str);
    		st = str;
    		if (mp.count(st) == 0) mp[st] = ++mpid;
    		int t = mp[st];
    		if (t >= rs && t <= rt) ins(t, id0, 1);
    		else if (t >= fs && t <= ft) ins(id1, t, 1);
    		else ins(t, id0, 1), ins(id1, t, 1);
    	}
    }
    printf("%d\n", dinic());
    return 0;
}


