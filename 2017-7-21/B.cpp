

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;
struct node {
	int x, y, flag, id;
}a[105], b[105];
int n, m, nc, nr;
int lsx[10005], cnt;
bool cmpx(node u, node v) {
	return u.x < v.x || (u.x == v.x && u.y < v.y);
}
bool cmpy(node u, node v) {
	return u.y < v.y || (u.y == v.y && u.x < v.x);
}
int main() {
	scanf("%d%d%d%d", &nr, &nc, &n, &m);
	for (int i = 1; i <= n; i++) {
		scanf("%d%d", &a[i].x, &a[i].y);
		a[i].id = i;
		a[i].x += 8;
		a[i].y += 4;
		a[i].flag = 1;
		b[i] = a[i];
		lsx[++cnt] = min(max(a[i].x, 0), nr);
	}
	for (int i = 1; i <= m; i++) {
		scanf("%d%d", &a[i + n].x, &a[i + n].y);
		a[i + n].x += 8;
		a[i + n].y += 4;
		a[i + n].flag = 0;
		a[i + n].id = i + n;
		b[i + n] = a[i + n];
		lsx[++cnt] = min(a[i + n].x + 1, nr);
		lsx[++cnt] = max(min(a[i + n].x - 1, nr), 0);
	}
	lsx[++cnt] = 0;
	lsx[++cnt] = nr;
	int ans = n + m;
	sort(lsx + 1, lsx + cnt + 1);
	cnt = unique(lsx + 1, lsx + cnt + 1) - lsx - 1;
	sort(a + 1, a + n + m + 1, cmpx);
	for (int i = 1; i <= cnt; i++) {
		for (int j = i + 1; j <= cnt; j++) {
			
			int tot = 0, l = lsx[i], r = lsx[j];
			//printf("[ %d %d ]\n", l, r);
			for (int k = 1; k <= n + m; k++) {
				if(l <= a[k].x && a[k].x <= r && a[k].y >= 0 && a[k].y <= nc) {
					b[++tot] = a[k];
				}
			}
			int minn = 0, now = 0;
			sort(b + 1, b + tot + 1, cmpy);
			for (int k = 1; k <= tot; k++) 
			{
				//printf("%d\n", now - minn);
				if(b[k].flag) now++;
				if(!b[k].flag) now--;
				if(k == tot || b[k].y != b[k + 1].y) {
					minn = min(minn, now);
					ans = min(ans, n - now + minn);
				}
			}
			//printf("ans = %d\n", ans);
		}
	}
	printf("%d\n", ans);
	return 0;
}
