#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

const int N = 505;
int n, fail[N][N], f[N][N];
char s[N];

int calc(int l, int r) {
	int t = r - fail[l][r];
	int len = r - l + 1;
	return (len % t == 0) ? len / t : 1;
}
int getlen(int x) {
	if (x < 10) return 1;
	if (x < 100) return 2;
	return 3;
}

int main() {
	int cs = 0;
	while (scanf("%s", s + 1), s[1] != '0') {
		n = strlen(s + 1);
		for (int k = 1; k <= n; k++) {
			fail[k][k] = k - 1;
			for (int j, i = k + 1; i <= n; i++) {
				for (j = fail[k][i - 1]; j != k - 1 && s[j + 1] != s[i]; j = fail[k][j]);
				fail[k][i] = j + (s[j + 1] == s[i]); 
			}
		}
		
		for (int len = 1; len <= n; len++) {
			for (int i = 1; i <= n - len + 1; i++) {
				int j = i + len - 1;
				f[i][j] = j - i + 1;
				int t = calc(i, j);
				for (int k = t; k >= 1; k--)
					if (t % k == 0)
						f[i][j] = min(f[i][j], f[i][i + len / k - 1] + (len == k ? 0 : 2) + getlen(k));
				for (int k = i; k < j; k++)
					f[i][j] = min(f[i][j], f[i][k] + f[k + 1][j]);
			}
		}
		printf("Case %d: %d\n", ++cs, f[1][n]);
	}
	return 0;
}


