#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
#include <queue>
#include <cmath>
using namespace std;

const int maxn = 255;
const int maxm = 30000;
const int inf = 1e6;

int s, t, cnt = 1;
int tail[maxn], cur[maxn], d[maxn];

int z[maxn], dx[maxn], dy[maxn];
char st[maxn][10];
int n, m, p, q;

struct edge
{
	int v, c, nex;
}e[maxm];

const double pi = acos(-1.0);
const double eps = 1e-6;
int sign(double x) {
	return (x < -eps) ? -1 : (x > eps);
}
struct point{
	double x, y;
	point (double x = 0, double y = 0) : x(x), y(y) {}
	point operator + (const point & rhs) const {
		return point(x + rhs.x, y + rhs.y);
	}
	point operator - (const point & rhs) const {
		return point(x - rhs.x, y - rhs.y);
	}
	point operator * (const double &k) const {
		return point(x * k, y * k); 
	}
	point operator / (const double &k) const {
		return point(x / k, y / k); 
	}
	double len2() const{
		return x * x + y * y;
	}
	double len() const {
		return sqrt(len2());
	}
	point turn90() {
		return point(-y, x);
	}
} pt[maxn];
double dot(const point &a, const point &b) {
	return a.x * b.x + a.y * b.y;
}
double det(const point &a, const point &b) {
	return a.x * b.y - a.y * b.x;
}
struct line {
	point a, b;
	line() {}
	line(point a, point b) : a(a), b(b) {}
} l[150];
struct circle {
	point o;
	double r;
	circle(point o, double r) : o(o), r(r) {}
};
struct arcs {
	point o, l, r;
	arcs() {}
	arcs(point o, point l, point r) : o(o), l(l), r(r) {}
} a[150];
bool isLL(const line &l1, const line &l2, point &p) {
	double s1 = det(l2.b - l2.a, l1.a - l2.a);
	double s2 = -det(l2.b - l2.a, l1.b - l2.a);
	if (!sign(s1 + s2)) return false;
	p = (l1.a * s2 + l1.b * s1) / (s1 + s2);	
	return true;
}
bool onSeg(const point &p, const line &l) {
	return sign(det(p - l.a, l.b - l.a)) == 0 && sign(dot(p - l.a, p - l.b)) <= 0;
}
bool isCL(circle a, line l, point &p1, point &p2) {
	double x = dot(l.a - a.o, l.b - l.a);
	double y = (l.b - l.a).len2();
	double d = x * x - y * ((l.a - a.o).len2() - a.r * a.r);
	if(sign(d) < 0) return false;
	d = max(d, 0.0);
	point p = l.a - ((l.b - l.a) * (x / y)), delta = (l.b - l.a) * (sqrt(d) / y);
	p1 = p + delta, p2 = p - delta;
	return true;
}
point getO(const point &p1, const point &p2, double dx, double dy) {
	point a = point(dx, dy).turn90();
	point b = point(p1 - p2).turn90();
	point m = (p1 + p2) / 2.0;
	point o; isLL(line(m, m + b), line(p1, p1 + a), o);
	return o;
}
double ang(const point &d1, const point &d2) {
	if (sign(det(d1, d2)) == 0)
		return (sign(dot(d1, d2)) > 0) ? 0 : pi;
	if (sign(det(d1, d2)) < 0)
		return acos(dot(d1, d2) / d1.len() / d2.len());
	else return 2 * pi - acos(dot(d1, d2) / d1.len() / d2.len());
}
bool onArcs(const point &p, const arcs &a) {
	return sign(ang(a.l - a.o, a.r - a.o) - ang(a.l - a.o, p - a.o)) > 0;
}


void ins(int u, int v, int c)
{
//printf("%d %d %d\n", u, v, c);
	e[++cnt] = (edge){v, c, tail[u]};
	tail[u] = cnt;
	e[++cnt] = (edge){u, 0, tail[v]};
	tail[v] = cnt;
}
bool bfs()
{
	queue <int> q;
	memset(d, -1, sizeof(d));
	d[s] = 0;
	q.push(s);
	while(!q.empty())
	{
		int u = q.front();
		q.pop();
		for(int i = tail[u]; i; i = e[i].nex)
			if(e[i].c > 0 && d[e[i].v] == -1)
			{
				d[e[i].v] = d[u] + 1;
				q.push(e[i].v);
			}
	}
	return d[t] != -1;
}
int dfs(int u, int flow)
{
	if(u == t)
		return flow;
	int used = 0;
	for(int &i = cur[u]; i; i = e[i].nex)
		if(e[i].c > 0 && d[e[i].v] == d[u] + 1)
		{
			int w = dfs(e[i].v, min(e[i].c, flow - used));
			e[i].c -= w;
			e[i ^ 1].c += w;
			used += w;
			if(used == flow)
				return flow;
		}
	if(!used)
		d[u] = -1;
	return used;
}
int dinic()
{
	int res = 0;
	while(bfs())
	{
		memcpy(cur, tail, sizeof(cur));
		res += dfs(s, inf);
	}
	return res;
}

int main()
{
	int cs = 0;
	int nl, na;
	int T = 0;
	while (scanf("%d%d%d", &n, &p, &q), n) {
		/*
			int tail[maxn], cur[maxn], d[maxn];
	
			int z[maxn], dx[maxn], dy[maxn];
			
			char st[maxn][10];
		*/
		memset(pt, 0, sizeof(pt));
		memset(a, 0, sizeof(a));
		memset(l, 0, sizeof(l));
		memset(cur, 0, sizeof(cur));
		memset(d, 0, sizeof(d));
		memset(z, 0, sizeof(z));
		memset(dx, 0, sizeof(dx));
		memset(dy, 0, sizeof(dy));
		memset(st, 0, sizeof(st));
		memset(e, 0, sizeof(e));
		T++;
		cnt = 1; 
		memset(tail, 0, sizeof(tail));
		s = 1; t = 2 + p + q;
		nl = 0; na = 0;
		for (int i = 1; i <= n; i++) {
			scanf("%d", &m);		
			for (int i = 1; i <= m; i++) {
				scanf("%lf%lf%s", &pt[i].x, &pt[i].y, st[i]);
				if (st[i][0] == 'c') scanf("%d%d", &dx[i], &dy[i]);
			}
			pt[m + 1] = pt[1];
			for (int i = 1; i <= m; i++) {
				if (st[i][0] == 's') l[++nl] = line(pt[i], pt[i + 1]);
				else {
					point O = getO(pt[i], pt[i + 1], dx[i], dy[i]);
					if (sign(det(O - pt[i], point(dx[i], dy[i]))) < 0)
						a[++na] = arcs(O, pt[i + 1], pt[i]); 
					else a[++na] = arcs(O, pt[i], pt[i + 1]); 
				}
			}
		}
		int sum = 0;
		for (int i = 2; i <= p + 1; i++) {
			scanf("%lf%lf%d", &pt[i].x, &pt[i].y, &z[i]);
			sum += z[i];
			ins(s, i, z[i]);
		}
		
		for (int i = p + 2, j = 1; i <= p + q + 1; i++, j++) {
			scanf("%lf%lf%d", &pt[i].x, &pt[i].y, &z[i]);
			ins(i, t, z[i]);
		}
		for (int i = 2; i <= p + 1; i++)
			for (int j = p + 2; j <= p + q + 1; j++) {
				bool bo = 1;
				line now = line(pt[i], pt[j]);
				for (int k = 1; k <= nl; k++) {
					point p;
					if (isLL(now, l[k], p))
						if (onSeg(p, now) && onSeg(p, l[k])) {
							bo = 0; break;
						}
				}
				//printf("%d %d %d\n", i, j, bo);
				for (int k = 1; k <= na; k++) {
					point p1, p2;
					if (isCL(circle(a[k].o, (a[k].o - a[k].l).len()), now, p1, p2))
						if (onArcs(p1, a[k]) && onSeg(p1, now) || onArcs(p2, a[k]) && onSeg(p2, now)) {
							bo = 0; break;
						}
				}
				if (bo) ins(i, j, 1);
			}
		printf("Case %d: ", ++cs);
		if (dinic() == sum) puts("Yes"); else puts("No");
		
	}
	return 0;
}

