#include <bits/stdc++.h>
using namespace std;

#define pii pair <int, int>


const int maxn = 55;
const int maxm = 5005;
const int maxl = 1e7;

int n, cs, cnt;
int a[maxn], val[maxn][maxn], s[maxn][maxn], t[maxn][maxn], b[maxl];
char ch[maxn];
bool vst[maxn][maxn], num[maxm];

void init()
{
	cnt = 0;
	for(int i = 1; i <= n; i++)
		for(int j = i; j <= n; j++)
		{
			vst[i][j] = 0;
		}
}

int cal(int l, int r)
{
	int now = a[l];
	for(int i = l + 1; i <= r; i++)
		if(now >= a[i])
			now += a[i];
		else
			now = a[i] - now;
	return now;
}

void solve(int l, int r)
{
	if(vst[l][r])
		return;
	vst[l][r] = 1;
	for(int i = l; i < r; i++)
	{
		solve(l, i);
		solve(i + 1, r);
	}
	b[++cnt] = val[l][r];
	s[l][r] = cnt;
	for(int i = l; i < r; i++)
	{
		for(int j = s[l][i]; j <= t[l][i]; j++)
			for(int k = s[i + 1][r]; k <= t[i + 1][r]; k++)
			{
				int u = b[j], v = b[k];
				if(u >= v)
					b[++cnt] = u + v;
				else
					b[++cnt] = v - u;
			}
	}
//	sort(b + s[l][r], b + 1 + cnt);
//	t[l][r] = cnt;
	//t[l][r] = s[l][r] + unique(b + s[l][r], b + 1 + cnt) - (b + s[l][r]) - 1;
	int now = s[l][r] - 1;
	memset(num, 0, sizeof(num));
	for(int i = s[l][r]; i <= cnt; i++)
		if(!num[b[i]])
		{
			num[b[i]] = 1;
			b[++now] = b[i];
		}
	t[l][r] = cnt = now;
//	cnt = t[l][r];
//	printf("%d %d: %d %d\n", l, r, s[l][r], t[l][r]);
}

int main()
{
	while(scanf("%s", ch + 1) && ch[1] != '0')
	{
		init();
		cs++;
		printf("Case %d: ", cs);
		n = strlen(ch + 1);
		for(int i = 1; i <= n; i++)
			if(ch[i] == 'I')
				a[i] = 1;
			else if(ch[i] == 'V')
				a[i] = 5;
			else if(ch[i] == 'X')
				a[i] = 10;
			else if(ch[i] == 'L')
				a[i] = 50;
			else if(ch[i] == 'C')
				a[i] = 100;
		for(int i = 1; i <= n; i++)
			for(int j = i; j <= n; j++)
				val[i][j] = cal(i, j);
		solve(1, n);
		sort(b + s[1][n], b + t[1][n] + 1);
		for(int i = s[1][n]; i <= t[1][n]; i++)
			printf("%d ", b[i]);
		puts("");
	}
	return 0;
}


