#include <bits/stdc++.h>
using namespace std;

#define mp make_pair
#define pii pair<int, int>

const int maxn = 10;

int cs, x, y, ans, flag, cnt, T;
int a[maxn][maxn], c[maxn], num[maxn], vst[maxn];
bool used[maxn][maxn];
pii nxt[maxn][maxn];
char op[maxn];

void update()
{
	while(a[x][y])
	{
		if(++y > 5)
		{
			x++;
			y = 1;
		}
	}
}

void init()
{
	x = y = 1;
	ans = 0;
	memset(a, 0, sizeof(a));
	memset(nxt, 0, sizeof(nxt));
	memset(used, 0, sizeof(used));
}

int getans()
{
	int res = 0;
	cnt = 0;
	memset(vst, 0, sizeof(vst));
	for(int i = 1; i <= 5; i++)
	{
		if(!vst[c[i]])
			num[++cnt] = c[i];
		vst[c[i]]++;
	}
	sort(num + 1, num + 1 + cnt);
	if(cnt == 1)
	{
		if(flag)
			return 100;
		flag = 1;
		return 50;
	}
	for(int i = 1; i <= cnt; i++)
		if(vst[num[i]] == 4)
			return 4 * num[i];
	if(cnt == 2)
		return 25;
	for(int i = 1; i <= cnt; i++)
		if(vst[num[i]] == 3)
			return 3 * num[i];
	for(int i = 1; i <= 2; i++)
		for(int j = i; j <= i + 5; j++)
		{
			if(j == i + 5)
				return 40;
			if(!vst[j])
				break;
		} 
	for(int i = 1; i <= 3; i++)
		for(int j = i; j <= i + 4; j++)
		{
			if(j == i + 4)
				return 30;
			if(!vst[j])
				break;
		} 
	return 0;	
}

void cal()
{
	flag = 0;
	int res = 0;
	for(int i = 1; i <= 5; i++)
	{
		for(int j = 1; j <= 5; j++)
			c[j] = a[i][j];
		res += getans();
		for(int j = 1; j <= 5; j++)
			c[j] = a[j][i];
		res += getans();
	}
	for(int i = 1; i <= 5; i++)
		c[i] = a[i][i];
	res += getans();
	for(int i = 1; i <= 5; i++)
		c[i] = a[i][6 - i];
	res += getans();
	ans = max(ans, res);
}

int main()
{
	int u, v;
	scanf("%d", &T);
	for(int cs = 1; cs <= T; cs++)
	{
		printf("Case %d: ", cs);
		init();
		for(int i = 1; i <= 13; i++)
		{
			scanf("\n%s", op);
			if(op[0] == 'V')
			{
				scanf("%d%d", &u, &v);
				a[x][y] = u;
				a[x + 1][y] = v;
				nxt[x][y] = mp(1, 0);
				if(u < v)
					used[u][v] = 1;
				else
					used[v][u] = 1;
			}
			else if(op[0] == 'H')
			{
				scanf("%d%d", &u, &v);
				a[x][y] = u;
				a[x][y + 1] = v;
				nxt[x][y] = mp(0, 1);
				if(u < v)
					used[u][v] = 1;
				else
					used[v][u] = 1;
			}
			else
				scanf("%d", &a[x][y]);
			update();
		}
		cal();
		for(int u = 1; u <= 6; u++)
			for(int v = u; v <= 6; v++)
				if(!used[u][v])
				{
					for(int x = 1; x <= 5; x++)
						for(int y = 1; y <= 5; y++)
							if(nxt[x][y] != mp(0, 0))
							{
								int i = x + nxt[x][y].first, j = y + nxt[x][y].second;
								pii ori = mp(a[x][y], a[i][j]);
								a[x][y] = u;
								a[i][j] = v;
								cal();
								swap(a[x][y], a[i][j]);
								cal();
								a[x][y] = ori.first;
								a[i][j] = ori.second;
							}
				}
		printf("%d\n", ans);
	}
	return 0;
}
