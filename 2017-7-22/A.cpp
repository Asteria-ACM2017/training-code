

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

const int maxn = 1e5;

int n, m, cnt, now, s1, s2, t;

ll gcd(ll a, ll b)
{
	if(!b)	
		return a;
	return gcd(b, a % b);
}

ll lcm(ll a, ll b)
{
	ll g = gcd(a, b);
	return a / g * b;
}

struct num
{
	ll i, next, x, y;
	num() {}
	num(ll i0, ll next0, ll x0, ll y0) {
		i = i0;
		next = next0;
		ll g = gcd(x0, y0);
		x = x0 / g;
		y = y0 / g;
	} 
	num operator + (const num & a) const {
		ll x1 = x, x2 = a.x, y1 = y, y2 = a.y;
		ll F = lcm(y1, y2);
		ll U = F / y1 * x1 + F / y2 * x2;
		ll g = gcd(F, U);
		U /= g;
		F /= g;
		return (num){0, 0, U, F};
	}
	num operator - (const num & a) const {
		ll x1 = x, x2 = a.x, y1 = y, y2 = a.y;
		ll F = lcm(y1, y2);
		ll U = F / y1 * x1 - F / y2 * x2;
		ll g = gcd(F, U);
		U /= g;
		F /= g;
		return (num){0, 0, U, F};
	}
	num operator * (const num & a) const {
		ll x1 = x, x2 = a.x, y1 = y, y2 = a.y;
		ll g1 = gcd(x1, y2);
		ll g2 = gcd(x2, y1);
		ll F = (y1 / g2) * (y2 / g1);
		ll U = (x1 / g1) * (x2 / g2);
		return (num){0, 0, U, F};
	}
	num operator / (const num & a) const {
		ll x1 = x, x2 = a.y, y1 = y, y2 = a.x;
		ll g1 = gcd(x1, y2);
		ll g2 = gcd(x2, y1);
		ll F = (y1 / g2) * (y2 / g1);
		ll U = (x1 / g1) * (x2 / g2);
		return (num){0, 0, U, F};
	}
}a[maxn];

void cal(int k)
{
	int nxt = a[k].next;
	if(nxt)
	{
		cal(nxt);
		a[k].y = a[nxt].x;
		a[k].x = a[nxt].x * a[k].i + a[nxt].y;
		ll g = gcd(a[k].y, a[k].x);
		a[k].x /= g;
		a[k].y /= g;
	}
	else
	{
		a[k].x = a[k].i;
		a[k].y = 1;
	}
}

void rewrite(num &b)
{
	if(b.y < 0)
		b.x = -b.x, b.y = -b.y;
	b.i = b.x / b.y;
	if(b.x % b.y == 0)
		return;
	if(b.i < 0 || b.x < 0)
		b.i--;
	b.next = ++cnt;
	a[cnt].x = b.y;
	a[cnt].y = b.x - b.i * b.y;
	rewrite(a[cnt]);
}

void print(num b)
{
	printf("%lld ", b.i);
	if(b.next)
		print(a[b.next]);
	else
		puts("");
}

int main()
{
	while(scanf("%d%d", &n, &m) && n + m != 0)
	{
		t++;
		printf("Case %d:\n", t);
		cnt = now = 0;
		memset(a, 0, sizeof(a));
		for(int i = 1; i <= n; i++)
		{
			now = ++cnt;
			scanf("%lld", &a[now].i);
			if(i < n)
				a[now].next = now + 1;
		}
		s1 = 1;
		s2 = now + 1;
		for(int i = 1; i <= m; i++)
		{
			now = ++cnt;
			scanf("%lld", &a[now].i);
			if(i < m)
				a[now].next = now + 1;
		}	
		cal(s1);
		cal(s2);
//		printf("%lld %lld\n", a[s1].x, a[s1].y);
//		printf("%lld %lld\n", a[s2].x, a[s2].y);
		
		now = ++cnt;
		a[now] = a[s1] + a[s2];
		rewrite(a[now]);
		print(a[now]);
		
		now = ++cnt;
		a[now] = a[s1] - a[s2];
		rewrite(a[now]);
		print(a[now]);
		
		now = ++cnt;
		a[now] = a[s1] * a[s2];
		rewrite(a[now]);
		print(a[now]);
		
		
		now = ++cnt;
		a[now] = a[s1] / a[s2];
		rewrite(a[now]);
		print(a[now]);
	}
	return 0;
}
