#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cmath>
using namespace std;

int main() {
	int a, b;
	char s[10];
	int T; scanf("%d", &T);
	for (int cs = 1; cs <= T; cs++) {
		scanf("%d%s%d", &a, s, &b);
		
		int t = (12 - b) * 30; 
		if (s[0] == 'a') t = a - t;
		else t = t - a;
		if (t <= 0) t += 360;
		t = t * 120;
		int ans = round(1.0 * t / 11);
		
		if (s[0] == 't') ans = 12 * 60 * 60 - ans;
		int hh = b, mm = 0, ss = 0;
		ss += ans;
		mm += ss / 60;
		ss %= 60;
		hh += mm / 60;
		mm %= 60;
		hh = (hh - 1) % 12 + 1;
		printf("Case %d: %d:%02d:%02d\n", cs, hh, mm, ss);
	}
	return 0;
}
