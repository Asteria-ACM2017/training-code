#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;

long long x, L, v;
int n;
struct node {
	long long x, v;
}a[105];

void update() {
	long long cnt = 1, tmp = v;
	for (int i = 1; i <= n; i++) {
		if(x - 4 <= a[i].x && a[i].x <= x) {
			tmp += a[i].v;
			cnt++;
		}
	}
	v = tmp / cnt;
}
long long nex() {
	long long re = -1;
	for (int i = 1; i <= n; i++) {
		if(a[i].v != v) {
			if(a[i].x <= x && a[i].x >= x - 4) {
				long long tmp = a[i].x + a[i].v;
				long long tmp2 = x + v;
				if(tmp <= tmp2 && tmp >= tmp2 - 4) re = 1;
			}
			if(a[i].x > x && a[i].v < v) {
				long long tmp = (a[i].x - x) / (v - a[i].v);
				if((a[i].x - x) % (v - a[i].v) != 0) tmp++;
				if(re == -1 || re > tmp) re = tmp;
			}
			if(a[i].x < x - 4 && a[i].v > v) {
				long long tmp = (x - 4 - a[i].x) / (a[i].v - v);
				if((x - 4 - a[i].x) % (a[i].v - v) != 0) tmp++;
				if(re == -1 || re > tmp) re = tmp;
			}
			
		}
	}
	return re;
}
int main() {
	int cas = 0;
	while(scanf("%lld%lld%lld%d", &x, &v, &L, &n) != EOF && (x || v || L || n)) {
		cas++;
		x *= 4;
		L *= 4;
		for (int i = 1; i <= n; i++) {
			scanf("%lld%lld", &a[i].x, &a[i].v);
			a[i].x *= 4;
		}
		update();
		long long tmp = nex();
		long long tim = 0ll;
		while(true) {
			if(tmp == -1 || x + tmp * v > L) break;
			tim += tmp;
			x += v * tmp;
			for (int i = 1; i <= n; i++) {
				a[i].x += a[i].v * tmp;
			}
			update();
			tmp = nex();
		}
		double ans = (double)(tim / 4.0 + (L - x) / 4.0 / v);
		printf("Case %d: Anna reaches her destination at time %.4f at a speed of %lld\n", cas, ans, v);

		
	}
	return 0;
}
