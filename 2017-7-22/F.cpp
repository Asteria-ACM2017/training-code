#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<queue>
using namespace std;

const char Dir[5] = "ENSW";
const int dx[4] = {0, -1, 1, 0};
const int dy[4] = {1, 0, 0, -1};
const int N = 105;
const int M = 10005;
const int inf = 1e7;

int n, m, a[N][N], b[N][N];
queue<int> Q;
char ch;

struct my {
	int tot, lst[M], id[M], nxt[M];
	int dis[M];
	void init() {
		tot = 0;
		memset(lst, 0, sizeof(lst));
	}
	void addedge(int x, int y) {
		tot++; id[tot] = y; nxt[tot] = lst[x]; lst[x] = tot;
	}
	void bfs(int S) {
		for (int i = 1; i <= n * m; i++) dis[i] = inf;
		while (!Q.empty()) Q.pop();
		Q.push(S); dis[S] = 0;
		while (!Q.empty()) {
			int x = Q.front(); Q.pop();
			for (int y, i = lst[x]; i; i = nxt[i]) {
				y = id[i];
				if (dis[y] == inf) {
					dis[y] = dis[x] + 1;
					Q.push(y);
				}
			}
		}
	}
} S, T;

int getdir(char ch) {
	if (ch == 'E') return 0;
	if (ch == 'N') return 1;
	if (ch == 'S') return 2;
	return 3;
}
int getid(int x, int y) {
	return (x - 1) * m + y;
}

int main() {
	int cs = 0;
	while (scanf("%d%d", &n, &m), (n || m)) {
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= m; j++) {
				if (i == n && j == m) break;
				scanf("%d%c", &a[i][j], &ch);
				b[i][j] = getdir(ch);
			}
		a[n][m] = 0; b[n][m] = 0;
		printf("Case %d: ", ++cs);
		
		S.init(); T.init();
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= m; j++) {
				int x = i + a[i][j] * dx[b[i][j]];
				int y = j + a[i][j] * dy[b[i][j]];
				 if (x < 1 || x > n || y < 1 || y > m) continue;
				y = getid(x, y); x = getid(i, j);
				S.addedge(x, y);
				T.addedge(y, x);
			}
		}
		int idS = getid(1, 1), idT = getid(n, m);
		S.bfs(idS); T.bfs(idT);
		
		int mn = max(n, m);
		int ans = S.dis[idT], ans_x, ans_y, ans_step, ans_dir;
		bool bo = 0;
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= m; j++) { 
				int p = getid(i, j);
				if (S.dis[p] < inf)
					for (int step = 1; step <= mn; step++)
						for (int dir = 0; dir < 4; dir++) {
							int x = i + step * dx[dir];
							int y = j + step * dy[dir];
							if (x < 1 || x > n || y < 1 || y > m) continue;
							int q = getid(x, y);
							if (T.dis[q] < inf) {
								int tmp = S.dis[p] + T.dis[q] + 1;
								if (tmp < ans) {
									ans = tmp;
									ans_x = i; ans_y = j;
									ans_step = step; ans_dir = dir;
									bo = 1;
								}
							}
						}
			}
		
		if (S.dis[idT] == inf) {
			if (bo) printf("%d %d %d%c %d\n", ans_x - 1, ans_y - 1, ans_step, Dir[ans_dir], ans);
			else puts("impossible");
		} else {
			if (bo) printf("%d %d %d%c %d\n", ans_x - 1, ans_y - 1, ans_step, Dir[ans_dir], ans);
			else printf("none %d\n", S.dis[idT]);
		}
	}
	return 0;
}
