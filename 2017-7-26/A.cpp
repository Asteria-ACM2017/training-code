#include <bits/stdc++.h>
using namespace std;

#define mp make_pair
#define pii pair<int, int>

const int maxn = 150005;

int n, m, Q, T, now;
int out[maxn], que[maxn];
char nm[maxn][205];
pii op[maxn];
struct node
{
	int val, id;
	bool operator < (const node &a) const{
		if(val != a.val)
			return val < a.val;
		return id > a.id;
	}
}a[maxn];
priority_queue <node> q;

int main()
{
	scanf("%d", &T);
	while(T--)
	{
		now = 0;
		scanf("%d%d%d", &n, &m, &Q);
		for(int i = 1; i <= n; i++)
		{
			scanf("\n%s%d", nm[i], &a[i].val);
			a[i].id = i;
		}
		for(int i = 1; i <= m; i++)
			scanf("%d%d", &op[i].first, &op[i].second);
		sort(op + 1, op + 1 + m);	
		for(int i = 1; i <= Q; i++)
		{
			scanf("%d", &que[i]);
		}
		for(int i = 1, j = 1; i <= n; i++)
		{
			q.push(a[i]);
			while(j <= m && op[j].first == i)
			{
				int cnt = op[j].second;
				while(!q.empty() && cnt)
				{
					out[++now] = q.top().id;
					q.pop();
					cnt--;
				}
				j++;
			}
		}
		while(!q.empty())
		{
			out[++now] = q.top().id;
			q.pop();
		}
		for(int i = 1; i < Q; i++)
			printf("%s ", nm[out[que[i]]]);
		if(Q)
			printf("%s", nm[out[que[Q]]]);
		puts("");
	}
	return 0;
}
