#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

const int maxn = 20005;
const int maxm = 110000;

int n, m, q, N, T;
int f[maxn], s[maxn];
ll res, ans[maxn];
struct edge
{
	int u, v, w, q;
}a[maxm];

bool cmp(edge a, edge b)
{
	if(a.w != b.w)
		return a.w < b.w;
	return a.q < b.q;
}

int find(int x)
{
	if(x == f[x])
		return x;
	return f[x] = find(f[x]);
}

int main()
{
	scanf("%d", &T);
	while(T--)
	{
		res = 0;
		scanf("%d%d%d", &n, &m, &q);
		for(int i = 1; i <= n; i++)
		{
			f[i] = i;
			s[i] = 1;
		}
		N = m + q;
		for(int i = 1; i <= m; i++)
		{
			scanf("%d%d%d", &a[i].u, &a[i].v, &a[i].w);
			a[i].q = 0;
		}
		for(int i = m + 1; i <= N; i++)
		{
			scanf("%d", &a[i].w);
			a[i].q = i - m;
		}
		sort(a + 1, a + 1 + N, cmp);
		for(int i = 1; i <= N; i++)
			if(!a[i].q)
			{
				int fx = find(a[i].u), fy = find(a[i].v);
				if(fx == fy)
					continue;
				f[fx] = fy;
				res += 1ll * s[fx] * s[fy];
				s[fy] += s[fx];
				s[fx] = 0;
			}
			else
			{
				ans[a[i].q] = res;
			}
		for(int i = 1; i <= q; i++)
			printf("%lld\n", ans[i] << 1);
	}
	return 0;
}


