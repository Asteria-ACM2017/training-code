#include <cstdio>
#include <algorithm> 
using namespace std;

int n, a[5005];
int main() {
	int T;
	scanf("%d", &T);
	for (int cas = 1; cas <= T; cas ++) {
		scanf("%d", &n);
		for (int i = 1; i <= n; i++) {
			scanf("%d", &a[i]);
		}
		int Q;
		scanf("%d", &Q);
		for (int q = 1, x, y; q <= Q; q++) {
			scanf("%d%d", &x, &y);
			int maxn = a[x];
			for (int i = x; i <= y; i++) {
				maxn = max(maxn, a[i]);
			}
			printf("%d\n", maxn);	
		}
		
	}
	return 0;
}


