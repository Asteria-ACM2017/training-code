#include <bits/stdc++.h>
using namespace std;

typedef long long LL;
typedef __int128 ll;

const int maxn = 1000;

int T, c1, c2;
ll f[maxn], g[maxn];
ll n, m;
map <ll, int> t;

void read(ll &x)
{
	string str;
	x = 0;
	cin >> str;
	int len = str.length();
	for(int i = 0; i < len; i++)
		x = x * 10 + str[i] - '0';
}

ll multi(ll x, ll y, ll M) {
	ll res = 0;
	for(; y; y >>= 1, x = (x + x) % M)
		if(y & 1)
			res = (res + x) % M;
	return res;
}

ll power(ll x, ll y, ll p)
{
	ll res = 1;
	for(; y; y >>= 1, x = multi(x, x, p))
		if(y & 1)
			res = multi(res, x, p);
	return res;
}

ll gcd(ll a, ll b)
{
	return !b ? a : gcd(b, a % b);
}

int primetest(ll n, int base)
{
	ll n2 = n - 1, res;
	int s = 0;
	while(!(n2 & 1))
		n2 >>= 1, s++;
	res = power(base, n2, n);
	if(res == 1 || res == n - 1)
		return 1;
	s--;
	while(s >= 0)
	{
		res = multi(res, res, n);
		if(res == n - 1)
			return 1;
		s--;
	}
	return 0;
}

static ll testNum[] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37};
static ll lim[] = {4, 0, 1373653ll, 25326001ll, 25000000000ll, 2152302898747ll, 3474749660383ll, 341550071728321ll, 0, 0, 0, 0};
	
int isprime(ll n)
{
	if(n < 2 || n == 3215031751ll)
		return 0;
	for(int i = 0; i < 12; i++)
	{
		if(n < lim[i])
			return 1;
		if(!primetest(n, testNum[i]))
			return 0;
	}
	return 1;
}

ll func(ll x, ll n)
{
	return (multi(x, x, n) + 1) % n;
}

ll pollard(ll n)
{
	ll i, x, y, p;
	if(isprime(n))
		return n;
	if(!(n & 1))
		return 2;
	for(i = 1; i < 20; i++)
	{
		x = i, y = func(x, n);
		p = gcd(y - x, n);
		while(p == 1)
		{
			x = func(x, n);
			y = func(func(y, n), n);
			p = gcd((y - x + n) % n, n) % n;
		}
		if(p == 0 || p == n)
			continue;
		return p;
	}
}

void factorize(ll n, ll *d, int &cnt)
{
	ll x;
	x = pollard(n);
	if(x == n)
	{
		d[++cnt] = x;
		return;
	}
	factorize(x, d, cnt);
	factorize(n / x, d, cnt);
}

LL solve(ll *d, int cnt)
{
	t.clear();
	LL res = 1;
	for(int i = 1; i <= cnt; i++)
		t[d[i]]++;
	for(int i = 1; i <= cnt; i++)
	{
		res *= t[d[i]];
		t[d[i]] = 1;
	}
	return res;
}

int main()
{
	scanf("%d", &T);
	while(T--)
	{
		c1 = c2 = 0;
		read(n);
		read(m);
		ll d = gcd(n, m);
		factorize(d, f, c1);
		for(int i = 1; i <= c1; i++)
			g[i] = f[i];
		c2 = c1;
		factorize(n / d, f, c1);
		factorize(m / d, g, c2);
		/*factorize(n, f, c1);
		sort(f + 1, f + 1 + c1);
		g[++c2] = f[c1];
		factorize(m / f[c1], g, c2);*/
		printf("%lld %lld\n", solve(f, c1), solve(g, c2));
	}
	return 0;
}
