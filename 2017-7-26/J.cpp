#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

const int N = 1e5 + 5;
long long n, m, cir;
int k;
long long a[20], b[20];
long long fac[N], rfac[N];

long long Mul(long long x, long long y, long long M) {
	long long res = 0;
	for (; y; y >>= 1, x = (x + x) % M)
		if (y & 1) res = (res + x) % M;
	return res;
}
long long Power(long long x, long long y, long long M) {
	long long res = 1;
	for (; y; y >>= 1, x = x * x % M)
		if (y & 1) res = res * x % M;
	return res;
}

long long exEuclid(long long a, long long b, long long &x, long long &y) {
	if (b == 0) {
		x = 1; y = 0; return a;
	} else {
		long long tmp = exEuclid(b, a % b, x, y);
		long long t = x; x = y; y = t - a / b * y;
		return tmp;
	}
}
long long CRT(long long a[], long long b[], int n, long long &cir) {
	long long x, y, ans;
	ans = 0; cir = 1;
	for (int i = 1; i <= n; i++) cir *= a[i];
	for (int i = 1; i <= n; i++) {
		long long tmp = cir / a[i];
		exEuclid(a[i], tmp, x, y);
		//cerr<<cir<< ' '<<a[i] << ' '<<b[i]<<endl;
		ans = (ans + Mul(Mul(y, tmp, cir), b[i], cir)) % cir;
	}
	return (cir + ans % cir) % cir;
}

void get(int p) {
	fac[0] = 1;
	for (int i = 1; i <= p - 1; i++)
		fac[i] = (fac[i - 1] * i) % p;
	/*rfac[p - 1] = Power(fac[p - 1], p - 2, p);
	for (int i = p - 1; i >= 1; i--)
		rfac[i - 1] = (i * rfac[i]) % p;*/
	//for (int i = 0; i <= p; i++)
		//printf("%d %lld %lld\n", i, fac[i], rfac[i]);
}

long long C(int n, int m, int p) {
	if (n < m) return 0;
	return fac[n] * Power(fac[m], p - 2, p) % p * Power(fac[n - m], p - 2, p) % p;
}

long long solve(long long n, long long m, int p) {
	long long res = 1;
	get(p);
	while (m) {
		res = res * C(n % p, m % p, p) % p;
		n /= p; m /= p;
	}
	return res;
}

int main() {
	int T; cin >> T;
	while (T--) {
		cin >> n >> m >> k;
		for (int i = 1; i <= k; i++) {
			cin >> a[i];
			b[i] = solve(n, m, a[i]);
			//cerr<<a[i]<<' '<<b[i]<<endl;
		}
		cout << CRT(a, b, k, cir) << endl;
	}
	return 0;
}
