#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;
const int N = 5e5 + 10;
long long mod = 1e9 + 7;
long long f[N], sum[N], g[N];
long long calc(long long x, long long y) {
	long long re = (x + y) * (y - x + 1) / 2 % mod;
	return re;
}
int main() {
	int T;
	scanf("%d", &T);
	f[1] = 1, f[2] = 2;
	int now = 1;
	for (int i = 2; now <= 5e5; i++) {
		for (int j = 1; now <= 5e5 && j <= f[i]; j++) {
			f[++now] = i;
		} 
	}
	for (int i = 1; i <= 5e5; i++) {
		sum[i] = sum[i - 1] + f[i];
		g[i] = g[i - 1] + (long long)i * calc(sum[i - 1] + 1, sum[i]) % mod;
	}
	for (int cas = 1; cas <= T; cas ++) {
		int n, pos;
		scanf("%d", &n);
		pos = upper_bound(sum + 1, sum + (int)5e5 + 1, n) - sum;
		pos--;
		long long left = n - sum[pos];
		long long ans = g[pos] + (long long)(pos + 1) * calc(sum[pos] + 1, n) % mod;
		ans %= mod;
		printf("%lld\n", ans);
	}
}
