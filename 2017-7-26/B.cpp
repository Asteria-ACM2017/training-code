#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

const int maxn = 1e4 + 5;
const int maxm = 2e5 + 5;

int n, m, cnt, size, T;
int deg[maxn], val[maxn], tail[maxn];
bool del[maxn];
ll ans, now;
struct edge
{
	int v, next;
}e[maxm];
queue <int> q;

void ins(int u, int v)
{
	e[++cnt] = (edge){v, tail[u]};
	tail[u] = cnt;
}

void bfs(int s)
{
	del[s] = 1;
	q.push(s);
	while(!q.empty())
	{
		int u = q.front();
		q.pop();
		size++;
		now += val[u];
		for(int i = tail[u]; i; i = e[i].next)
			if(!del[e[i].v])
			{
				q.push(e[i].v);
				del[e[i].v] = 1;
			}
	}
}

void init()
{
	cnt = ans = 0;
	memset(tail, 0, sizeof(tail));
	memset(deg, 0, sizeof(deg));
	memset(del, 0, sizeof(del));
}

int main()
{
	int u, v;
	scanf("%d", &T);
	while(T--)
	{
		init();
		scanf("%d%d", &n, &m);
		for(int i = 1; i <= n; i++)
			scanf("%d", &val[i]);
		for(int i = 1; i <= m; i++)
		{
			scanf("%d%d", &u, &v);
			ins(u, v);
			ins(v, u);
			deg[u]++;
			deg[v]++;
		}
		for(int i = 1; i <= n; i++)
			if(deg[i] < 2)
				q.push(i);
		while(!q.empty())
		{
			int u = q.front();
			q.pop();
			if(del[u])
				continue;
			del[u] = 1;
			for(int i = tail[u]; i; i = e[i].next)
				if(!del[e[i].v])
				{
					if(--deg[e[i].v] < 2)
						q.push(e[i].v);
				}
		}
		for(int i = 1; i <= n; i++)
			if(!del[i])
			{
				size = now = 0;
				bfs(i);
				if(size & 1)
					ans += now;
			}
		printf("%lld\n", ans);
	}
	return 0;
}


