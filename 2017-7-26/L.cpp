#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

const int N = 1e5 + 5;
const int M = 1e9 + 7;
int n, m, a[N], b[N];

int calc(int a[], int b[]) {
	int res = 0, now = 0, k = 1;
	for (int i = n; i > 1; i--) {
		now += 1LL * b[i] * k % M;
		now %= M;
		k = k * 2 % M;
	}
	for (int i = 1; i <= n; i++) {
		res += 1LL * a[i] * now % M;
		res %= M;
		now = ((now - 1LL * m * b[i % n + 1] % M + M) % M * 2 + b[i]) % M;
	}
	return res;
}

int main() {
	int T; scanf("%d", &T);
	while (T--) {
		scanf("%d", &n);
		m = 1;
		for (int i = 1; i <= n - 2; i++)
			m = m * 2 % M;
		for (int i = 1; i <= n; i++)	
			scanf("%d%d", &a[i], &b[i]);
		printf("%d\n", (calc(a, b) - calc(b, a) + M) % M);
	}
	return 0;
}


