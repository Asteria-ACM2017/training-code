#include <bits/stdc++.h>
using namespace std;

const int maxn = 1005;

int n, q, T, root, cnt;
int b[maxn];
struct node
{
	int l, r, c;
}a[maxn];

void build(int &k, int l, int r)
{
	if(l > r)
		return;
	k = ++cnt;
	a[k].c = b[l];
	int s = l;
	while(s < r && b[s + 1] <= b[l])
		s++;
	build(a[k].l, l + 1, s);
	build(a[k].r, s + 1, r);
}

void find(int u, int x)
{
	if(a[u].c == x)
	{
		puts("");
		return;
	}
	if(x < a[u].c)
	{
		printf("E");
		find(a[u].l, x);
	}
	else
	{
		printf("W");
		find(a[u].r, x);	
	}
}

int main()
{
	int x;
	scanf("%d", &T);
	while(T--)
	{
		cnt = 0;
		scanf("%d", &n);
		for(int i = 1; i <= n; i++)
			scanf("%d", &b[i]);
		build(root, 1, n);
		scanf("%d", &q);
		while(q--)
		{
			scanf("%d", &x);
			find(root, x);
		}
	}
	return 0;
}
