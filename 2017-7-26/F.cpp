#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

int n, m, i0, i1;
string s0, s1, t0, t1;
int fail[20005];

string find(int N, string s) {
	int i, j, k, l;
	for (i = 0, j = 1; j < N; ) {
		for (k = 0; k < N && s[i + k] == s[j +k]; k++);
		if (k >= N) break;
		if (s[i + k] > s[j + k]) j += k + 1;
		else l = i + k, i = j, j = max(l, j) + 1;
	}
	return s.substr(i, N);
}

void getfail(string s) {
	int n = s.length();
	fail[0] = -1;
	for (int j, i = 1; i < n; i++) {
		for (j = fail[i - 1]; j != -1 && s[j + 1] != s[i]; j = fail[j]);
		fail[i] = j + (s[j + 1] == s[i]);
	}
}

int kmp0(string s, string p) {
	int n = s.length(), m = p.length();
	for (int j = -1, i = 0; i < n; i++) {
		for (; j != -1 && p[j + 1] != s[i]; j = fail[j]);
		j = j + (p[j + 1] == s[i]);
		if (j == m - 1) return i - m + 1;
	}
	return 0;
}

int kmp1(string s, string p) {
	int ans;
	int n = s.length(), m = p.length();
	for (int j = -1, i = 0; i < n - 1; i++) {
		for (; j != -1 && p[j + 1] != s[i]; j = fail[j]);
		j = j + (p[j + 1] == s[i]);
		if (j == m - 1) ans = i - m + 1;
	}
	return ans;
}

int main() {
	cin >> n;
	for (int i = 1; i <= n; i++) {
		cin >> m;
		s0 = "";
		cin >> s0;
		s1 = "";
		for (int i = m - 1; i >= 0; i--) s1 += s0[i];
		s0 += s0; t0 = find(m, s0);
		s1 += s1; t1 = find(m, s1);
		getfail(t0);
		i0 = kmp0(s0, t0); i0 = i0 + 1;
		getfail(t1);
		i1 = kmp1(s1, t1); i1 = m - i1;
		if (t0 > t1 || (t0 == t1 && i0 <= i1)) printf("%d 0\n", i0);
		else printf("%d 1\n", i1);
	}	
}


