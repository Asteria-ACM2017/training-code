#include <bits/stdc++.h>
using namespace std;

#define mp make_pair

typedef unsigned long long ll;
typedef pair <int, int> pii;

const int maxn = 1005;
const int len = 22;
const int base = (1 << len + 1) - 1;

int n = 1000, m = 1000000, T;
int dx = 1000, dy = 1001 - len;
int a[maxn][maxn], b[maxn][maxn];
pii ans;
vector <pii> g[1 << len + 1];

inline unsigned sfr(unsigned h, unsigned x)
{
    return h >> x;
}

ll id(int x, int y)
{
    return x * 10000000ll + y;
}

int f(ll i, ll j)
{
    ll w = i * 1000000ll + j;
    int h = 0;
    for(int k = 0; k < 5; k++)
    {
        h += (int)((w >> (8 * k)) & 255);
        h += (h << 10);
        h ^= sfr(h, 6);
    }
    h += h << 3;
    h ^= sfr(h, 11);
    h += h << 15;
    return sfr(h, 27) & 1;
}

void add(int x, int y)
{
    int now = 0;
    for(int i = y; i < y + len; i++)
        now = (now << 1) + f(x, i);
//    s[now] = id(x, y);
    g[now].push_back(mp(x, y));
}

void init()
{
    for(int i = dx; i <= m; i += dx)
        for(int j = dy; j <= m; j += dy)
            add(i, j);
}

int cal(int x, int y)
{
    int now = 0;
    for(int i = y; i < y + len; i++)
        now = (now << 1) + a[x][i];
    return now;
}

bool check(int x, int y)
{
    for(int i = 1; i <= n; i++)
        for(int j = 1; j <= n; j++)
            if(a[i][j] != f(i + x - 1, j + y - 1))
		return 0;
    return 1;
}

pii find()
{
    for(int i = 1; i <= n; i++)
    {
//        int tmp = cal(i, 1);
        for(int j = 1; j + len < n; j++)
        {
	    int tmp = cal(i, j);
//            if(s.count(tmp))
	    for(int k = g[tmp].size() - 1; k >= 0; k--)
            {
//                ll now = s[tmp];
//               int x = now / 10000000 - i + 1, y = now % 10000000 - j + 1;
		int x = g[tmp][k].first - i + 1, y = g[tmp][k].second - j + 1;
                if(check(x, y))
                    return mp(x, y);
            }
//            tmp = ((tmp << 1) & base) + a[i][j + len];
        }
    }
    return mp(0, 0);
}

void make()
{
    freopen("b.in", "w", stdout);
    puts("1");
    for(int i = 1; i <= n; i++)
    {
        for(int j = 1; j <= n; j++)
            printf("%d", f(i+99999, j+99999));
        puts("");
    }
    exit(0);
}

int main()
{
    //make();
    init();
    scanf("%d", &T);
    for(int cs = 1; cs <= T; cs++)
    {
        for(int i = 1; i <= n; i++)
            for(int j = 1; j <= n; j++)
                scanf("%1d", &a[i][j]);
        ans = find();
        printf("Case #%d :%d %d\n", cs, ans.first, ans.second);
    }
    return 0;
}
