#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

const int maxn = 1e5 + 5;
const int lim = 1e5;
const int mod = 1e9 + 7;

ll ans;
int n, T, mx, x;
int a[maxn], mu[maxn];

void getmu()
{
    mu[1] = 1;
    for(int i = 1; i <= lim; i++)
        for(int j = i + i; j <= lim; j += i)
            mu[j] -= mu[i];
}

ll power(ll b, int k)
{
    ll res = 1;
    for(; k; k >>= 1, b = b * b % mod)
        if(k & 1)
            res = res * b % mod;
    return res;
}

void init()
{
    ans = 0;
    mx = lim;
    memset(a, 0, sizeof(a));
}

int main()
{
    getmu();
    scanf("%d", &T);
    for(int cs = 1; cs <= T; cs++)
    {
        init();
        scanf("%d", &n);
        for(int i = 1; i <= n; i++)
        {
            scanf("%d", &x);
            a[x]++;
            mx = min(mx , x);
        }
        for(int i = 1; i <= lim; i++)
            a[i] += a[i - 1];
        for(int d = 2; d <= mx; d++)
            if(mu[d])
            {
                ll tmp = -1;
                for(int i = 1; i * d <= lim; i++)
                {
                        tmp = tmp * power(i, a[min(lim, (i + 1) * d - 1)] - a[i * d - 1]) % mod;
//                        printf("%d %d %lld\n", d, i, tmp);
                }
                ans = (ans + tmp * mu[d] + mod) % mod;
            }
        printf("Case #%d: %lld\n", cs, ans);
    }
    return 0;
}
