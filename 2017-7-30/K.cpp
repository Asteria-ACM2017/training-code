#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cmath>
using namespace std;

const double pi = acos(-1.0);
int n, x[505], y[505], ans;
int f[205][205];

int main() {
	while (scanf("%d", &n) == 1) {
		memset(f, 0, sizeof(f));
		for (int i = 1; i <= n; i++) {
			scanf("%d%d", &x[i], &y[i]);
			f[x[i] + 100][y[i] + 100] = 1;
		}
		ans = 0;
		for (int i = 1; i <= n; i++) {
			for (int j = i + 1; j <= n; j++) {
				int dx = y[i] - y[j];
				int dy = x[j] - x[i];
				int p = x[i] + x[j] + dx;
				int q = y[i] + y[j] + dy;
				if ((p & 1) || (q & 1)) continue;
				p /= 2; q /= 2;
				if (p < -100 || q < -100 || p > 100 || q > 100) continue;
				p -= dx; q -= dy;
				if (p < -100 || q < -100 || p > 100 || q > 100) continue;	
				p += 100; q += 100;
				if (f[p][q] && f[p + dx][q + dy]) ans += 1;
			}
		}
		printf("%d\n", ans / 2);
	}
	return 0;
}
« Back

