#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

int n, m, p, T, t;
long long ans;

int main() {
	scanf("%d", &T);
	for (int i = 1; i <= T; i++) {
		scanf("%d%d%d", &n, &m, &p);
		n = n * m - 1;
		ans = 0;
		while (n >= p) {
			t = (n - 1) / p;
			ans += 1LL * t * (t + 1) / 2 * (p - 1);
			n -= t + 1;
		}
		if (ans & 1) puts("NO"); else puts("YES");
	}
	return 0;
}
« Back

