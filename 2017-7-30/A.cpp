#include <bits/stdc++.h>
using namespace std;

const int maxn = 3e5 + 5;

int n, x, y, k, ans, lim, T;
char a[maxn], b[maxn];

int main()
{
	scanf("%d", &T);
	while(T--)
	{
		k = ans = 0;
		scanf("%d%d%d\n", &n, &x, &y);
		scanf("%s%s", a + 1, b + 1);
		for(int i = 1; i <= n; i++)
			k += (a[i] == b[i]);
		lim = min(k, min(x, y));
		for(int i = 0; i <= lim; i++)
		{
			if(n - k >= x + y - i - i)
			{
				ans = 1;
				break;
			}
		}
		if(ans)
			puts("Not lying");
		else
			puts("Lying");
	}
	return 0;
}
