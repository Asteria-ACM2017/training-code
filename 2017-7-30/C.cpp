#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

const int maxn = 250005;
const int mod = 1e9 + 7;

ll ans;
int n, N;
int b[maxn], c[maxn << 1], a[maxn << 2], pos[maxn << 1];

void build(int k, int l, int r)
{
	if(l == r)
	{	
		pos[l] = k;
		a[k] = c[l];
		return;
	}
	int mid = (l + r) >> 1;
	build(k << 1, l, mid);
	build(k << 1 ^ 1, mid + 1, r);
	a[k] = max(a[k << 1], a[k << 1 ^ 1]);
}

int query(int k, int l, int r, int s, int t)
{
	if(l == s && r == t)
		return a[k];
	int mid = (l + r) >> 1;
	if(t <= mid)
		return query(k << 1, l, mid, s, t);
	if(s > mid)
		return query(k << 1 ^ 1, mid + 1, r, s, t);
	return max(query(k << 1, l, mid, s, mid), query(k << 1 ^ 1, mid + 1, r, mid + 1, t));
}

void update(int x)
{
	int k = pos[x];
	a[k] = c[x];
	for(int i = k / 2; i; i >>= 1)
		a[i] = max(a[i << 1], a[i << 1 ^ 1]);
}

int main()
{
	while(scanf("%d", &n) != EOF)
	{
		ans = 0;
		N = 2 * n;
		for(int i = 1; i <= n; i++)
		{
			scanf("%d", &c[i]);
			c[i] -= i;
		}
		for(int i = 1; i <= n; i++)
			scanf("%d", &b[i]);
		sort(b + 1, b + 1 + n);
		build(1, 1, N);
		for(int i = 1; i <= n; i++)
		{
			int tmp = query(1, 1, N, b[i], n + i - 1);
			c[n + i] = tmp - (n + i);
			ans += tmp + mod;
			ans %= mod;
			update(n + i);
		}
		printf("%lld\n", ans);																			
	}
	return 0;
}
« Back

