#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>

int n, a[20][20];
char s[10000000];

int id(char c) {
	if (c >= '0' && c <= '9') return c - '0';
	return c - 'A' + 10;
}

void print(int x) {
	if (x < 10) printf("%d", x);
	else printf("%c", (char)x - 10 + 'A');
}

void init() {
	for (int i = 0; i <= 3; i++) a[0][i] = i;
	a[0][5] = 9; a[0][7] = 127;
	for (int i = 11; i <= 15; i++) a[0][i] = i;
	for (int i = 0; i <= 3; i++) a[1][i] = i + 16;
	a[1][6] = 8; a[1][8] = 24; a[1][9] = 25;
	for (int i = 12; i <= 15; i++) a[1][i] = i + 16;
	a[2][5] = 10; a[2][6] = 23; a[2][7] = 27;
	for (int i = 13; i <= 15; i++) a[2][i] = i - 8;
	a[3][2] = 22; a[3][7] = 4; a[3][12] = 20; a[3][13] = 21; a[3][15] = 26;
	a[4][0] = 32; a[4][11] = 46; a[4][12] = 60;
	a[4][13] = 40; a[4][14] = 43;
	a[4][15] = 124; a[5][0] = 38;
	a[5][10] = 33; a[5][11] = 36;
	a[5][12] = 42; a[5][13] = 41;
	a[5][14]  = 59;
	
	a[6][0] = 45;
	a[6][1] = 47;
	a[6][11] = 44;
	a[6][12] = 37;
	a[6][13] = 95;
	a[6][14] = 62;
	a[6][15] = 63;
	
	a[7][9] = 96;
	a[7][10] = 58;
	a[7][11] = 35;
	a[7][12] = 64;
	a[7][13] = 39;
	a[7][14] = 61;
	a[7][15] = 34;
	
	for (int i = 1; i <= 9; i++) a[8][i] = i + 96;
	for (int i = 1; i <= 9; i++) a[9][i] = i + 105;
	a[10][1] = 126; for (int i = 2; i <= 9; i++) a[10][i] = i + 113;
	a[11][0] = 94; a[11][10] = 91; a[11][11] = 93;
	a[12][0] = 123; for (int i = 1; i <= 9; i++) a[12][i] = i + 64;
	a[13][0] = 125; for (int i = 1; i <= 9; i++) a[13][i] = i + 73;
	a[14][0] = 92; for (int i = 2; i <= 9; i++) a[14][i] = i + 81;
	for (int i = 0; i <= 9; i++) a[15][i] = i + 48;;
}

int main() {
	init();
	scanf("%s", s);
	n = strlen(s);
	for (int i = 0; i < n; i += 2) {
		int x = id(s[i]), y = id(s[i + 1]);
		print(a[x][y] / 16);
		print(a[x][y] % 16);
	}
	puts("");
}


