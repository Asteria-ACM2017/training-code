#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

const int maxn = 10;
const int mod = 1e9 + 7;

ll n, a0, ax, ay, b0, bx, by;
ll a[maxn], b[maxn][maxn], tmp[maxn][maxn], t[maxn];

void square()  //b
{
	memset(tmp, 0, sizeof(tmp));
	for(int i = 1; i <= 5; i++)
		for(int j = 1; j <= 5; j++)
			for(int k = 1; k <= 5; k++)
			{
				tmp[i][j] += b[i][k] * b[k][j] % mod;
				if(tmp[i][j] >= mod)
					tmp[i][j] -= mod;
			}
	memcpy(b, tmp, sizeof(b));
}

void multi()  // a * b
{
	memset(t, 0, sizeof(t));
	for(int j = 1; j <= 5; j++)
		for(int k = 1; k <= 5; k++)
		{
			t[j] += a[k] * b[k][j] % mod;
			if(t[j] >= mod)
				t[j] -= mod;
		}
	memcpy(a, t, sizeof(a));
}

void power(ll k)
{
	for(; k; k >>= 1, square())
		if(k & 1)
			multi();
}

int main()
{
	while(scanf("%lld", &n) != EOF)
	{
		scanf("%lld%lld%lld%lld%lld%lld", &a0, &ax, &ay, &b0, &bx, &by);
		memset(b, 0, sizeof(b));
		a[1] = a0;
		a[2] = b0;
		a[3] = a0 * b0 % mod;
		a[4] = 0;
		a[5] = 1;
		b[1][1] = ax;
		b[1][3] = ax * by % mod;
		b[2][2] = bx;
		b[2][3] = ay * bx % mod;
		b[3][3] = ax * bx % mod;
		b[3][4] = b[4][4] = b[5][5] = 1;
		b[5][1] = ay;
		b[5][2] = by;
		b[5][3] = ay * by % mod;
		power(n);
		printf("%lld\n", a[4]);
	}
	return 0;
}


