#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

const int N = 105;
const int M = 9 * 20;
int n;
long long K, a[N], b[N];
int bi[30], pre;
long long cnt[20][M + 5][2], flr[N][M + 5], f[N][M + 5], c[N][M + 5];
char s[N][5];

long long D(long long x) {
	if (x > K) return K + 1;
	return x;
}
long long C(long long a, long long b) {
	if (a == 0 || b == 0) return 0;
	if (a > K || b > K) return K + 1;
	if (K / a + 1 <= b) return K + 1;
	return D(a * b);
}

bool check(int x, char *s, int y) {
	int n = strlen(s);
	if (n == 1) {
		return (s[0] == '=' && x == y || s[0] == '<' && x < y || s[0] == '>' && x > y); 
	}
	return (s[0] == '!' && x != y || s[0] == '<' && x <= y || s[0] == '>' && x >= y);
}

void dp(long long n) {
	//printf("n = %lld\n", n);
	int t = 19;
	for (int i = 1; i <= 19; i++) bi[i] = 0;
	while (n) {
		bi[t--] = n % 10;
		n /= 10;
	}
	
	cnt[0][0][1] = 1;
	for (int i = 1; i <= 19; i++) {
		for (int j = 0; j <= 9 * i; j++)
			cnt[i][j][0] = cnt[i][j][1] = 0;
		for (int j = 0; j <= 9 * (i - 1); j++) {
			for (int k = 0; k <= 9; k++)
				cnt[i][j + k][0] += cnt[i - 1][j][0];
			for (int k = 0; k < bi[i]; k++)
				cnt[i][j + k][0] += cnt[i - 1][j][1];
			cnt[i][j + bi[i]][1] = cnt[i][j + bi[i]][1] + cnt[i - 1][j][1];
		}
	}
	//printf("re = %lld\n", cnt[19][1][0] + cnt[19][1][1]);
}

int digit_sum(long long x) {
	int sum = 0;
	while (x) {
		sum += x % 10;
		x /= 10;
	}
	return sum;
}
long long calc(int i, long long k) {
	//printf("...%d %lld %d\n", i, k, pre);
	long long tmp = 0;
	dp(k);
	for (int j = 0; j <= M; j++)
		if (i == 1 || check(pre, s[i - 1], j))
			tmp = D(tmp + C(cnt[19][j][0] + cnt[19][j][1] - flr[i][j], f[i][j]));
	//printf("%d %lld %lld\n", i, k, tmp);
	return tmp;
}

int main() {
	while (scanf("%d%lld", &n, &K) != EOF) {
		for (int i = 1; i <= n; i++) {
			scanf("%lld%lld", &a[i], &b[i]);
			if (i == n) continue;
			scanf("%s", s[i]);
		}
		for (int i = 1; i <= n; i++) {
			dp(a[i] - 1);
			for (int j = 0; j <= M; j++) flr[i][j] = cnt[19][j][0] + cnt[19][j][1];
			dp(b[i]);
			for (int j = 0; j <= M; j++)
				c[i][j] = D(cnt[19][j][0] + cnt[19][j][1] - flr[i][j]);
		}
		/*for (int i = 1; i <= n; i++)//
			for (int j = 0; j <= M; j++)
				if (f[i][j]) printf("%d %d %lld\n", i, j, f[i][j]);*/
		for (int j = 0; j <= M; j++) f[n][j] = 1;
		for (int i = n - 1; i >= 1; i--) {
			for (int j = 0; j <= M; j++) {
				f[i][j] = 0;
				for (int k = 0; k <= M; k++) {
					if (check(j, s[i], k)) f[i][j] = D(f[i][j] + C(c[i + 1][k], f[i + 1][k]));
				}
			}
		}
		/*puts("....");
		for (int i = 1; i <= n; i++)
			for (int j = 0; j <= M; j++)
				if (f[i][j]) printf("%d %d %lld\n", i, j, f[i][j]);*/

		if (calc(1, b[1]) < K) {
			puts("OUT OF RANGE");
			continue;
		}
		long long now = K;
		for (int i = 1; i <= n; i++) {
			long long l = a[i], r = b[i], mid;
			while (l < r) {
			//printf("%lld %lld\n", l, r);
				mid = (l + r) / 2;
				if (calc(i, mid) < now) l = mid + 1;
				else r = mid; 
			}
			printf("%lld%c", l, " \n"[i == n]);
			if (l > a[i]) now -= calc(i, l - 1);
			pre = digit_sum(l);
			//printf("%lld\n", now);
		}
	}
	return 0;
}
