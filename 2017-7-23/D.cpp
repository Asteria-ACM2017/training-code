

#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

const int maxn = 25;

int n;
char a[maxn];
ll f[maxn][maxn];

int main()
{
    while(scanf("%s", a + 1) != EOF)
    {
	n = strlen(a + 1);
	memset(f, 0, sizeof(f));
	f[0][0] = 1;
	for(int i = 1; i <= n; i++)
	    for(int j = 0; j <= i; j++)
	    {
		f[i][j] += f[i - 1][j] * j;
		if(a[i] == '+' && j)
		    f[i][j] += f[i - 1][j - 1];
		else if(a[i] == '-' && j <= n)
		    f[i][j] += f[i - 1][j + 1] * (j + 1) * (j + 1);
	    }
	printf("%lld\n", f[n][0]);
    }
    return 0;
}

