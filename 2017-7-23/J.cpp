#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <algorithm>
using namespace std;
const long long mod = 1e9 + 7;
long long f[205];
const int M = 205;
long long ans;
int up[205], down[205];
long long c[205], a[205];
int m;
int U, D;
long long n;

void linear_recurrence(long long n, int m) {
	long long v[M] = {1 % mod}, u[M << 1], msk = !!n;
	for (long long i = n; i > 1; i >>= 1) {
		msk <<= 1;
	}
	for (long long x = 0; msk; msk >>= 1, x <<= 1) {
		memset(u, 0, sizeof(u));
		int b = (!!(n & msk));
		x |= b;
		if(x < m) {
			u[x] = 1 % mod;
		}
		else {
			for (int i = 0; i < m; i++) {
				for (int j = 0, t = i + b; j < m; j++, t++) {
					u[t] = (u[t] + v[i] * v[j]) % mod;
				}
			}
			for (int i = ((m << 1) - 1); i >= m; i--) {
				for (int j = 0, t = i - m; j < m; j++, t++) {
					u[t] = (u[t] + c[j] * u[i]) % mod;
				}
			}
		}
		std::copy(u, u + m, v);
	}
	ans = 0ll;
	for (int i = 0; i < m; i++) {
		ans = (ans + v[i] * a[i] % mod) % mod;
	}

}
int main() {
	while(scanf("%lld", &n) != EOF) {
		m = 0;
		scanf("%d", &U);
		for (int i = 1; i <= U; i++) {
			scanf("%d", &up[i]);
		}
		scanf("%d", &D);
		for (int i = 1; i <= D; i++) {
			scanf("%d", &down[i]);
			m = max(m, down[i]);
		}
		memset(f, 0, sizeof(f));
		f[0] = 1ll;
		for (int i = 1; i <= m; i++) {
			for (int j = 1; j <= U; j++) {
				if(i >= up[j]) {
					f[i] = f[i] + f[i - up[j]];
					if(f[i] >= mod) f[i] -= mod;
				}
			}
		}
		memset(c, 0, sizeof(c));
		memset(a, 0, sizeof(a));
		for (int i = 1; i <= D; i++) {
			c[down[i]] = f[down[i]];
		}
		a[0] = 1;
		for (int i = 1; i < m; i++) {
			for (int j = 1; j <= D; j++)
			if(i >= down[j]) {
				a[i] += a[i - down[j]] * c[down[j]] % mod;
				if(a[i] >= mod) a[i] -= mod;
			}
		}
		for (int i = 0; i <= (m - 1)/ 2; i++) {
			swap(c[i], c[m - i]);
		}/**/
		linear_recurrence(n, m);
		printf("%lld\n", ans);
	}
	return 0;
}
