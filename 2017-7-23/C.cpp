#include <bits/stdc++.h>
using namespace std;

const int maxn = 300;
const double eps = 1e-8;
const double inf = 1e10;

int n, m;
double ans, w, h;
double f[maxn][maxn];

int sign(double x)
{
    return (x > eps) - (x < -eps);
}

double msqrt(double x)
{
    return sign(x) > 0 ? sqrt(x) : 0;
}

struct Point
{
    double x, y;
    Point(){}
    Point (double x, double y) : x(x), y(y){}
    bool operator == (const Point &a) const{
	return !sign(x - a.x) && !sign(y - a.y);
    }
    Point operator + (const Point &a) const{
	return Point(x + a.x, y + a.y);
    }
    Point operator - (const Point &a) const{
	return Point(x - a.x, y - a.y);
    }
    Point operator * (double v) const{
	return Point(x * v, y * v);
    }
    Point operator / (double v) const{
	return Point(x / v, y / v);
    }
    double len2() {
	return x * x + y * y;
    }
    double len() {
	return msqrt(len2());
    }
    
} c[maxn];
struct Line
{
    Point a, b;
    Line(){}
    Line (Point a, Point b) : a(a), b(b){}
} a[maxn], cut[10];

double dot(const Point &a, const Point &b)
{
    return a.x * b.x + a.y * b.y;
}

double det(const Point &a, const Point &b)
{
    return a.x * b.y - a.y * b.x;
}

bool isLL(const Line &l1, const Line &l2, Point &p)
{
    double s1 = det(l2.b - l2.a, l1.a - l2.a);
    double s2 = -det(l2.b - l2.a, l1.b - l2.a);
    if(!sign(s1 + s2))
	return 0;
    p = (l1.a * s2 + l1.b * s1) / (s1 + s2);
    return 1;
}

void update(Line l1, Line l2, Point &s, Point &t)
{
    Point p;
    if(!isLL(l1, l2, p))
	return;
    if(p == l1.a)
    {
	s = p;
	return;
    }
    if(p == l1.b)
    {
	t = p;
	return;
    }
    double la = (p - l1.a).len2(), lb = (p - l1.b).len2();
    if(sign(lb - la) < 0)
    {
	if(sign(lb - (t - l1.b).len2()) < 0)
	    t = p;
    }
    else
    {
	if(sign(la - (s - l1.a).len2()) < 0)
	    s = p;
    }
}

double cost(Line l, int now)
{
    Point dir = l.b - l.a;
    Point s = l.a - dir * inf;
    Point t = l.b + dir * inf;
    for(int i = 1; i <= now; i++)
	update(l, cut[i], s, t);
    return (t - s).len();
}

int main()
{
    while(scanf("%d%lf%lf", &n, &w, &h) != EOF)
    {
	ans = inf;
	for(int i = 1; i <= n; i++)
	    scanf("%lf%lf", &c[i].x, &c[i].y);
	for(int i = 1; i < n; i++)
	    a[i] = (Line){c[i], c[i + 1]};
	a[0] = a[n] = (Line){c[1], c[n]};
	for(int i = 1; i <= n; i++)
	    a[i + n] = a[i];
	m = 2 * n - 1;
	cut[1] = (Line){{0, 0}, {w, 0}};
	cut[2] = (Line){{w, 0}, {w, h}};
	cut[3] = (Line){{w, h}, {0, h}};
	cut[4] = (Line){{0, h}, {0, 0}};
	for(int i = 1; i <= m; i++)
	{
	    cut[5] = a[i - 1];
	    cut[6] = a[i + 1];
	    f[i][i] = cost(a[i], 6);
	}
	for(int l = 2; l < n; l++)
	    for(int i = 1; i + l - 1 <= m; i++)
	    {
		int j = i + l - 1;
		f[i][j] = inf;
		cut[5] = a[i - 1];
		cut[6] = a[j + 1];
		for(int k = i; k <= j; k++)
		    f[i][j] = min(f[i][j], f[i][k - 1] + f[k + 1][j] + cost(a[k], 6));
	    }
	for(int i = n; i <= m; i++)
	    ans = min(ans, cost(a[i], 4) + f[i - n + 1][i - 1]);
	printf("%.6lf\n", ans);
    }
    return 0;
}
