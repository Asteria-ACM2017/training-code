#include <bits/stdc++.h>
using namespace std;

const int maxn = 1e5 + 5;

int n, D, T, cnt, tot, timer, dep, ans, limf;
int val[maxn], tail[maxn], size[maxn], sz[maxn], f[maxn], g[maxn], t[maxn], F[maxn], G[maxn];
bool used[maxn];
struct edge
{
	int v, next;
}e[maxn << 1];

void init()
{
	cnt = timer = 0;
	ans = 1;
	memset(used, 0, sizeof(used));
	memset(tail, 0, sizeof(tail));
	memset(sz, 0, sizeof(sz));
}

void ins(int u, int v)
{
	e[++cnt] = (edge){v, tail[u]};
	tail[u] = cnt;
}

void dp(int u, int pa, int d)
{
	tot++;
	sz[u] = 0;
	size[u] = 1;
	dep = max(dep, d);
	for(int i = tail[u]; i; i = e[i].next)
		if(e[i].v != pa && !used[e[i].v])
		{
			dp(e[i].v, u, d + 1);
			size[u] += size[e[i].v];
			sz[u] = max(sz[u], size[e[i].v]);
		}
}

void find(int u, int pa, int &root, int mx)
{
	int cur = max(sz[u], tot - size[u]);
	if(cur < mx)
	{
		mx = cur;
		root = u;
	}
	for(int i = tail[u]; i; i = e[i].next)
		if(e[i].v != pa && !used[e[i].v])
			find(e[i].v, u, root, mx);
}

void dfs_f(int u, int pa, int len, int x) //increasing
{
	if(val[u] < x)
		return;
	if(t[len] != timer)
	{
		t[len] = timer;
		f[len] = val[u];
	}
	else
		f[len] = min(f[len], val[u]);
	for(int i = tail[u]; i; i = e[i].next)
		if(e[i].v != pa && !used[e[i].v])
			dfs_f(e[i].v, u, len + 1, val[u]);
}

void dfs_g(int u, int pa, int len, int x) //decreasing
{
	if(val[u] > x)
		return;
	if(t[len] != timer)
	{
		t[len] = timer;
		g[len] = val[u];
	}
	else
		g[len] = max(g[len], val[u]);
	for(int i = tail[u]; i; i = e[i].next)
		if(e[i].v != pa && !used[e[i].v])
			dfs_g(e[i].v, u, len + 1, val[u]);
}

void cal_f()
{
	for(int i = 0; t[i] == timer; i++)
	{
		limf = i;
		int l = 0, r = dep;
		while(l < r)
		{
			int mid = ((l + r) >> 1) + 1;
			if(G[mid] >= f[i] - D)
				l = mid;
			else
				r = mid - 1;
		}
		if(G[l] >= f[i] - D)
			ans = max(ans, i + l + 1);
	}
}

void cal_g()
{
	for(int i = 0; t[i] == timer; i++)
	{
		int l = 0, r = dep;
		while(l < r)
		{
			int mid = ((l + r) >> 1) + 1;
			if(F[mid] <= g[i] + D)
				l = mid;
			else
				r = mid - 1;
		}
		if(F[l] <= g[i] + D)
			ans = max(ans, i + l + 1);
	}
}

void update()
{
	for(int i = 0; i <= limf; i++)
		F[i] = min(F[i], f[i]);
	for(int i = 0; t[i] == timer; i++)
		G[i] = max(G[i], g[i]);
}

void solve(int u)
{
	tot = dep = 0;
	dp(u, 0, dep);
	if(tot == 1)
		return;
	find(u, 0, u, max(sz[u], tot - size[u]));
	for(int i = 1; i <= dep; i++)
		F[i] = 200001, G[i] = -100001;
	F[0] = G[0] = val[u];
	for(int i = tail[u]; i; i = e[i].next)
		if(!used[e[i].v])
		{
			timer++;
			t[0] = timer; f[0] = val[u];
			dfs_f(e[i].v, u, 1, val[u]);
			cal_f();
			
			timer++;
			t[0] = timer; g[0] = val[u];
			dfs_g(e[i].v, u, 1, val[u]);
			cal_g();
			
			update();
		}
	used[u] = 1;
	for(int i = tail[u]; i; i = e[i].next)
		if(!used[e[i].v])
			solve(e[i].v);
}

int main()
{
	int u, v;
	scanf("%d", &T);
	for(int cs = 1; cs <= T; cs++)
	{
		init();
		scanf("%d%d", &n, &D);
		for(int i = 1; i <= n; i++)
			scanf("%d", &val[i]);
		for(int i = 1; i < n; i++)
		{
			scanf("%d%d", &u, &v);
			ins(u, v);
			ins(v, u);
		}
		solve(1);
		printf("Case #%d: %d\n", cs, ans);
	}
	return 0;
}
