#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

const int N = 1e5;
const int M = 1e9 + 7;
int n, m, k, a[20], sum[20];
long long f[20][105], fac[N + 5], rfac[N + 5];

long long Power(long long x, int y) {
	long long res = 1;
	for (; y; y >>= 1, x = x * x % M)
		if (y & 1) res = res * x % M;
	return res;
}

void prework() {
	fac[0] = 1;
	for (int i = 1; i <= N; i++)
		fac[i] = fac[i - 1] * i % M;
	rfac[N] = Power(fac[N], M - 2);
	for (int i = N; i >= 1; i--)
		rfac[i - 1] = rfac[i] * i % M;
}

long long C(int x, int y) {
	return fac[x] * rfac[y] % M * rfac[x - y] % M;
}

int main() {
	prework();
	
	int T; scanf("%d", &T);
	for (int cs = 1; cs <= T; cs++) {
		scanf("%d%d%d", &n, &m, &k);
		sum[0] = 0;
		for (int i = 1; i <= m; i++) {
			scanf("%d", &a[i]);
			sum[i] = sum[i - 1] + a[i];
		}
		
		memset(f, 0, sizeof(f));
		f[0][k] = 1;
		int p, q;
		for (int i = 1; i <= m; i++) {
			for (int j = 0; j <= k; j++) {
				for (int h = j; h <= k; h++) {
					for (int l = h; l <= k; l++) {
						p = n - l * 2 - sum[i - 1]; q = a[i] - h - l + j * 2;
						if (f[i - 1][l] == 0 || q > p || q < 0) continue;
						f[i][j] = (f[i][j] + f[i - 1][l] * C(l, l - j) % M * C(l - j, l - h) % M * C(p, q)) % M;
					}
				}
			}
		}
		printf("Case #%d: %lld\n", cs, f[m][0]);
	}
	return 0;
}
