#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<set>
using namespace std;

const int N = 1e5 + 5;
struct troop {
	int a, d;
} a[N], b[N];
int n, m, f[N];
multiset<int> st;

bool cmpa(troop a, troop b) {
	return a.a < b.a || (a.a == b.a && a.d < b.d);
}

bool cmpd(troop a, troop b) {
	return a.d < b.d || (a.d == b.d && a.a > b.a);
}

int solve() {
	int p = 1;
	memset(f, 0, sizeof(f));
	for (int i = 1; i <= m; i++) {
		while (p <= n && a[p].a < b[i].d) p++;
		if (p > n) return -1;
		f[p] = i; p++;
	}
	
	int cnt = 0;
	p = m;
	st.clear();
	multiset<int>::iterator it;
	for (int i = n; i >= 1; i--) {
		st.insert(a[i].d);
		while (p && a[i - 1].a < b[p].d) {
			int tmp = b[p].a;
			it = st.upper_bound(tmp);
			if (it != st.end()) {
				cnt++;
				st.erase(it);
			} else st.erase(st.begin());
			p--;
		}
	}
	return cnt;
}

int main() {
	int T; scanf("%d", &T);
	for (int cs = 1; cs <= T; cs++) {
		scanf("%d%d", &n, &m);
		for (int i = 1; i <= n; i++)
			scanf("%d%d", &a[i].a, &a[i].d);
		sort(a + 1, a + n + 1, cmpa);
		a[0].a = 0, a[0].d = 0;
		for (int i = 1; i <= m; i++)
			scanf("%d%d", &b[i].a, &b[i].d);
		sort(b + 1, b + m + 1, cmpd);
		
		int ans = solve();
		printf("Case #%d: %d\n", cs, (ans == -1) ? -1 : n - m + ans);
	}
	return 0;
}
