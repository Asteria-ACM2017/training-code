#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

long long n, m, a, b, c;
long long tot, p, q, p0, q0, p1, q1;

int main() {
	int T; scanf("%d", &T);
	for (int cs = 1; cs <= T; cs++) {
		scanf("%lld%lld", &n, &m);
		scanf("%lld%lld%lld", &a, &b, &c);
		if (a < c) swap(a, c);
		
		tot = n - 1;
		p = m + 1; q = n - m + 1;
		p0 = p / 2; q0 = q / 2;
		printf("Case #%d: ", cs);
		if (b >= a) printf("%lld %lld\n", tot * b, (q & 1) ? (q0 * a + (tot - q0) * c) : (q0 * a + (tot - q0) * c));
		else if (c >= b) printf("%lld %lld\n", (p & 1) ? (p0 * c + (tot - p0) * a) : (p0 * c + (tot - p0) * a), tot * b);
		else if (a + c >= b * 2) {
			int tmp = n - m - 1 + p0;
			printf("%lld %lld\n", (p & 1) ? (tmp * a + (tot - tmp) * c) : ((tmp - 1) * a + b + (tot - tmp) * c), (n - m) * b + (m - 1) * c);
		} else {
			int tmp = m - 1 + q0;
			printf("%lld %lld\n", m * b + (n - m - 1) * a, (q & 1) ? (tmp * c + (tot - tmp) * a) : ((tmp - 1) * c + b + (tot - tmp) * a));
		}
	}
	return 0;
}

