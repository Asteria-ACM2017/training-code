#include <bits/stdc++.h>
using namespace std;

const int maxn = 100005;

int T, n, w, h, cx, cy, ans;
int vx[maxn << 1], vy[maxn << 1], sum[maxn << 1];
struct point
{
	int x, y, l, r;
	bool operator < (const point &a) const {
		return x < a.x;
	}
}p[maxn];
struct node
{
	int val, tag;
}a[maxn << 2];

void init()
{
	cx = cy = ans = 0;
	memset(sum, 0, sizeof(sum));
}

void update(int k, int l, int r)
{
	a[k].val = 0;
	if(a[k].tag)
		return;
	if(l == r)
		a[k].val = sum[l];
	else
	{
		if(!a[k << 1].tag)
			a[k].val = max(a[k].val, a[k << 1].val);
		if(!a[k << 1 ^ 1].tag)
			a[k].val = max(a[k].val, a[k << 1 ^ 1].val);
	}
}

void add(int k, int l, int r, int s, int t, int x)
{
	if(s <= l && t >= r)
	{
		a[k].tag += x;
		update(k, l, r);
		return;
	}
	int mid = (l + r) >> 1;
	if(s <= mid)
		add(k << 1, l, mid, s, t, x);
	if(t > mid)
		add(k << 1 ^ 1, mid + 1, r, s, t, x);
	update(k, l, r);
}

void build(int k, int l, int r)
{
	a[k].tag = 0;
	if(l == r)
	{
		a[k].val = sum[l];
		return;
	}
	int mid = (l + r) >> 1;
	build(k << 1, l, mid);
	build(k << 1 ^ 1, mid + 1, r);
	a[k].val = max(a[k << 1].val, a[k << 1 ^ 1].val);
}

int main()
{
 	int l, r, tmp;
	scanf("%d", &T);
	for(int cs = 1; cs <= T; cs++)
	{
		init();
		scanf("%d%d%d", &n, &w, &h);
		for(int i = 1; i <= n; i++)
		{
			scanf("%d%d", &p[i].x, &p[i].y);
			vx[++cx] = p[i].x - w + 1;
			vx[++cx] = p[i].x + 1;
			vy[++cy] = p[i].y - h + 1;
			vy[++cy] = p[i].y + 1;
		}
		vx[++cx] = -w;
		vy[++cy] = -h;
		sort(vx + 1, vx + 1 + cx);
		sort(vy + 1, vy + 1 + cy);
		cx = unique(vx + 1, vx + 1 + cx) - (vx + 1);
		cy = unique(vy + 1, vy + 1 + cy) - (vy + 1);
		for(int i = 1; i <= n; i++)
		{
			p[i].l = lower_bound(vy + 1, vy + 1 + cy, p[i].y - h + 1) - vy;
			p[i].r = upper_bound(vy + 1, vy + 1 + cy, p[i].y) - vy - 1;
			sum[p[i].l]++;
			sum[p[i].r + 1]--;
		}
		for(int i = 1; i <= cy; i++)
			sum[i] += sum[i - 1];
		build(1, 1, cy);

		sort(p + 1, p + 1 + n);
		l = 1, r = 1, tmp = 0;
		for(int i = 1; i <= cx; i++)
		{
			while(l <= n && p[l].x < vx[i])
			{
				tmp--;
				add(1, 1, cy, p[l].l, p[l].r, 1);
				l++;
			}
			while(r <= n && p[r].x - vx[i] + 1 <= w)
			{
				tmp++;
				add(1, 1, cy, p[r].l, p[r].r, -1);
				r++;
			}
			ans = max(ans, tmp + a[1].val);
		}
		printf("Case #%d: %d\n", cs, ans);
	}
	return 0;
}
