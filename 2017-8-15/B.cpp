#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<vector>
using namespace std;

const int N = 1e6 + 5;
int n, m, k;
vector<int> a[N];
char s[10];

void modify(int x1, int y1, int x2, int y2, int k) {
	a[x1][y1] += k;
	a[x1][y2 + 1] -= k;
	a[x2 + 1][y1] -= k;
	a[x2 + 1][y2 + 1] += k;
}

int main() {
	int T; scanf("%d", &T);
	for (int cs = 1; cs <= T; cs++) {
		scanf("%d%d%d", &n, &m, &k);
		
		for (int i = 0; i <= n + 1; i++) {
			a[i].clear(); a[i].push_back(0);
			for (int j = 1; j <= m + 1; j++)
				a[i].push_back(0);
		}
		
		int x = 0, y = 0, step;
		for (int i = 1; i <= k; i++) {
			scanf("%s%d", s, &step);
			if (s[0] == 'L') y -= step;
			else if (s[0] == 'R') y += step;
			else if (s[0] == 'D') {
				modify(x + 1, 1, x + step, y, 1);
				modify(x + 1, y + 1, x + step, m, -1);
				x += step;
			} else {
				modify(x - step + 1, 1, x, y, -1);
				modify(x - step + 1, y + 1, x, m, 1);
				x -= step;
			}
		}
		
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= m; j++)
				a[i][j] += a[i][j - 1];
		for (int j = 1; j <= m; j++)
			for (int i = 1; i <= n; i++)
				a[i][j] += a[i - 1][j];
		
		long long ans = 0;
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= m; j++)
				ans += a[i][j] * a[i][j] / 4;
		printf("Case #%d: %lld\n", cs, ans);
	}
	return 0;
}
