#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

const int N = 1e6 + 5;
int n, h[N << 2], b[N], ans[N];
pair<int, int> a[N];

void init(int x, int l, int r) {
	if (l == r) h[x] = 1, b[l] = x;
	else {
		int m = (l + r) / 2, L = x * 2, R = L + 1;
		init(L, l, m); init(R, m + 1, r);
		h[x] = h[L] + h[R];
	}
}

int getpos(int x, int l, int r, int k) {
	if (l == r) return l;
	int m = (l + r) / 2, L = x * 2, R = L + 1;
	if (h[L] < k) return getpos(R, m + 1, r, k - h[L]);
	else return getpos(L, l, m, k);
}

void modify(int x) {
	while (x) {
		h[x]--;
		x /= 2;
	}
}

bool solve() {
	sort(a + 1, a + n + 1);
	for (int i = 1; i <= n; i++) {
		int t = min(a[i].second, n - i - a[i].second) + 1;
		if (t <= 0) return 0;
		int pos = getpos(1, 1, n, t);
		//printf("%d %d %d\n", i, a[i].first, pos);
		ans[pos] = a[i].first;
		modify(b[pos]);
	}
	return 1;
}

int main() {
	int T; scanf("%d", &T);
	for (int cs = 1; cs <= T; cs++) {
		scanf("%d", &n);
		init(1, 1, n);
		for (int i = 1; i <= n; i++)
			scanf("%d%d", &a[i].first, &a[i].second);
			
		printf("Case #%d:", cs);
		if (solve()) {
			for (int i = 1; i <= n; i++)
				printf(" %d", ans[i]);
			puts("");
		} else puts(" impossible");
	}
	return 0;
}
