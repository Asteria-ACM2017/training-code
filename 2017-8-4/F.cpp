#include <bits/stdc++.h>
using namespace std;

const int maxn = 1e5 + 5;

int n, m, T, ans, L;
int val[maxn], c[maxn], last[maxn], pre[maxn];
int f[maxn], g[maxn], pos[maxn], a[maxn << 2];
struct rev
{
	int u, id;
	bool operator < (const rev &a) const{
		return u < a.u;
	}
}v[maxn];

void init()
{
	m = ans = 0;
	memset(last, 0, sizeof(last));
}

void build(int k, int l, int r)
{
	a[k] = 0;
	if(l == r)
	{
		pos[l] = k;
		return;
	}
	int mid = (l + r) >> 1;
	build(k << 1, l, mid);
	build(k << 1 ^ 1, mid + 1, r);
}

void recycle(int k, int l, int r)
{
	if(a[k] == 0)
		return;
	a[k] = 0;
	if(l == r)
		return;	
	int mid = (l + r) >> 1;
	recycle(k << 1, l, mid);
	recycle(k << 1 ^ 1, mid + 1, r);
}

int query(int k, int l, int r, int s, int t)
{
	if(s > t || !a[k])
		return 0;
	if(s == l && t == r)
		return a[k];
	int mid = (l + r) >> 1;
	if(t <= mid)
		return query(k << 1, l, mid, s, t);
	if(s > mid)
		return query(k << 1 ^ 1, mid + 1, r, s, t);
	return max(query(k << 1, l, mid, s, mid), query(k << 1 ^ 1, mid + 1, r, mid + 1, t));
}

void update(int k, int x)
{
	for(; k; k >>= 1)
		a[k] = max(a[k], x);
}

void add(int k, int x)
{
	a[k] = x;
	for(k >>= 1; k; k >>= 1)
		a[k] = max(a[k << 1], a[k << 1 ^ 1]);
}

void lis1()
{
	recycle(1, 1, m);
	for(int i = 1; i <= n; i++)
	{
		f[i] = query(1, 1, m, 1, val[i] - 1) + 1;
		update(pos[val[i]], f[i]);
	}
}

void lis2()
{
	recycle(1, 1, m);
	for(int i = n; i >= 1; i--)
	{
		g[i] = query(1, 1, m, val[i] + 1, m) + 1;
		update(pos[val[i]], g[i]);
	}
}

void solve()
{
	recycle(1, 1, m);
	for(int i = 1; i <= n - L; i++)
	{
		update(pos[val[i]], f[i]);
		ans = max(ans, f[i]);
	}
	for(int i = n; i > L; i--)
	{
		add(pos[val[i - L]], f[pre[i - L]]);
		ans = max(ans, g[i] + query(1, 1, m, 1, val[i] - 1));
	}
}

int main()
{
	scanf("%d", &T);
	for(int cs = 1; cs <= T; cs++)
	{
		init();
		scanf("%d%d", &n, &L);
		for(int i = 1; i <= n; i++)
		{
			scanf("%d", &v[i].u);
			v[i].id = i;
		}
		sort(v + 1, v + 1 + n);
		for(int i = 1; i <= n; i++)
			{
				val[v[i].id] = val[v[i - 1].id];
				if(i == 1 || v[i].u != v[i - 1].u)
					val[v[i].id]++;
			}
		for(int i = 1; i <= n; i++)
		{
			pre[i] = last[val[i]];
			last[val[i]] = i;
		}
		m = val[v[n].id];
		build(1, 1, m);
		lis1();
		lis2();
		solve();
		printf("Case #%d: %d\n", cs, ans);
	}	
	return 0;
}

