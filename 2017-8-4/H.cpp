#include<cstdio>
#include<algorithm>
using namespace std;

int main() {
	int T; scanf("%d", &T);
	int tmp, l, r;
	for (int cs = 1; cs <= T; cs++) {
		scanf("%d%d%d", &tmp, &l, &r);
		long long n = tmp;
		long long ans = ++n;
		int cnt = 0;
		while (n) {
			cnt += (n & 1);
			n /= 2;
		}
		if (cnt > r) {
			ans--;
			n = ans = ans + (ans & (-ans));
			cnt = 0;
			while (n) {
				cnt += (n & 1);
				n /= 2;
			}
		}
		int t = max(l - cnt, 0);
		printf("Case #%d: %lld\n", cs, ans + (1LL << t) - 1);
	}
	return 0;
}
