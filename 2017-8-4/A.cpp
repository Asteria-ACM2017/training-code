#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

const int N = 1e5 + 5;
const int M = 505;
const int inf = 1e5;

struct point {
	int x, y;
	point(int x = 0, int y = 0) : x(x), y(y) {}
	bool cmpd(const point &a, const point &b) const {
		if (a.x == x && b.x == 0) return  a.y - y < 0 || b.y > 0;
		return 1LL * (a.y - y) * b.x <= 1LL * (a.x - x) *  b.y; 
	}
	bool cmpu(const point &a, const point &b) const {
		if (a.x == x && b.x == 0) return a.y - y > 0 || b.y < 0;
		return 1LL * (a.y - y) * b.x >= 1LL * (a.x - x) *  b.y; 
	}
	point operator - (const point &rhs) const {
		return point(x - rhs.x, y - rhs.y);
	}
};

int n, m;
point a[M], p[N], D[M], U[M];
int idD[M], idU[M], f[M], g[M], ans;
bool b[M];

bool cmpD(int i, int j) {
	return a[i].x < a[j].x || (a[i].x == a[j].x && a[i].y > a[j].y);
}
bool cmpU(int i, int j) {
	return a[i].x < a[j].x || (a[i].x == a[j].x && a[i].y < a[j].y);
}

void solve(int now) {
	if (n == 1 && p[1].x == a[now].x && p[1].y == a[now].y) ans = 1;
	
	for (int i = 1; i <= m; i++)
		f[i] = g[i] = inf;
	
	int t;	
	f[now] = 1;
	for (int i = 1; i <= m; i++)
		if (idD[i] == now) {
			t = i; break;
		}
	for (int k = t; k <= m; k++) {
		int i = idD[k], j;
		for (int h = k + 1; h <= m; h++) {
			j = idD[h];
			if (a[j].x < a[i].x ||a[i].x == a[j].x && a[i].y == a[j].y) continue;
			if (a[i].cmpd(a[j], D[i])) f[j] = min(f[j], f[i] + 1);
		}
	}
	
	g[now] = 1;
	for (int i = 1; i <= m; i++)
		if (idU[i] == now) {
			t = i; break;
		}
	for (int k = t; k <= m; k++) {
		int i = idU[k], j;
		for (int h = k + 1; h <= m; h++) {
			j = idU[h];
			if (a[j].x < a[i].x ||a[i].x == a[j].x && a[i].y == a[j].y) continue;
			if (a[i].cmpu(a[j], U[i])) g[j] = min(g[j], g[i] + 1);
		}
	}
	
	for (int i = 1; i <= m; i++)
		if (i != now) ans = min(ans, f[i] + g[i] - 2);
}

int main() {
	int T; scanf("%d", &T);
	for (int cs = 1; cs <= T; cs++) {
		scanf("%d%d", &n, &m);
		for (int i = 1; i <= n; i++)
			scanf("%d%d", &p[i].x, &p[i].y);
		for (int i = 1; i <= m; i++)
			scanf("%d%d", &a[i].x, &a[i].y);
			
		for (int i = 1; i <= m; i++) idD[i] = idU[i] = i;
		sort(idD + 1, idD + m + 1, cmpD);
		sort(idU + 1, idU + m + 1, cmpD);
		
		for (int i = 1; i <= m; i++) {
			D[i] = point(0, 1);
			U[i] = point(0, -1);
			b[i] = 1;
			for (int j = 1; j <= n; j++) {
				if (p[j].x < a[i].x) {
					b[i] = 0; continue;
				}
				if (a[i].x == p[j].x && a[i].y == p[j].y) continue;
				if (a[i].cmpd(p[j], D[i])) D[i] = p[j] - a[i];
				if (a[i].cmpu(p[j], U[i])) U[i] = p[j] - a[i];
			}
		}
		/*for (int i = 1; i <= m; i++)
			printf("%d,%d...%d,%d\n", D[i].x, D[i].y, U[i].x, U[i].y);*/
		
		ans = inf;
		for (int i = 1; i <= m; i++)
			if (b[i]) solve(i);
		if (ans == inf) ans = -1;
		printf("Case #%d: %d\n", cs, ans);
	}
	return 0;
}
