#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

const int N = 32;
const int M = 1805;
const int inf = 1e8;

int n, m, a[N][N], b[N][N];
int f[N][N][M];

void init() {
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++) {
			b[i][j] = (n + m - 1) * a[i][j] * a[i][j];
			for (int k = 0; k <= 1800; k++)
				f[i][j][k] = inf;
		}
}

int solve() {
	f[1][1][a[1][1]] = b[1][1];
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++) {
			if (i > 1) {
				for (int k = a[i][j]; k <= 1800; k++)
					f[i][j][k] = min(f[i][j][k], f[i - 1][j][k - a[i][j]] + b[i][j]);
			}
			if (j > 1) {
				for (int k = a[i][j]; k <= 1800; k++)
					f[i][j][k] = min(f[i][j][k], f[i][j - 1][k - a[i][j]] + b[i][j]);
			}
		}
		
	int ans = inf;
	for (int k = 0; k <= 1800; k++)
		if (f[n][m][k] < inf) ans = min(ans, f[n][m][k] - k * k);
	return ans;
}

int main() {
	int T; scanf("%d", &T);
	for (int cs = 1; cs <= T; cs++) {
		scanf("%d%d", &n, &m);
		for (int i = 1; i <= n; i++)
			for (int j = 1; j <= m; j++)
				scanf("%d", &a[i][j]);
		init();
		printf("Case #%d: %d\n", cs, solve());
	}
	return 0;
}
