#include <bits/stdc++.h>
using namespace std;

const int maxn = 1e6 + 5;
int T, n, now, m[2], ans[5];
int id[2][maxn], s[2][maxn], sum[maxn], c[maxn];
bool vst[maxn];
struct node
{
	int u, id;
	bool operator < (const node &a) const{
		return u < a.u;
	}
} r[2][maxn];

void init()
{
	memset(s, 0, sizeof(s));
	memset(ans, 0, sizeof(ans));
}

bool cmp(int a, int b)
{
	return id[now][a] < id[now][b];
}

int solve(int u, int v)
{
	int res = 0, last = 0;
	memset(sum, 0, sizeof(sum));
	memset(vst, 0, sizeof(vst));
	now = u;
	for(int i = 1; i <= n; i++)
		c[i] = i;
	sort(c + 1, c + 1 + n, cmp);

	for(int i = 1; i <= n; i++)
	{
		if(id[u][c[i]] != id[u][c[i - 1]] || i == 1)
		{
			for(int j = last; j < i; j++)
				vst[id[v][c[j]]] = 0;
			last = i;
		}
		int pa = id[u][c[i]], tmp = id[v][c[i]];
		if(!vst[tmp])
		{
			vst[tmp] = 1;
			sum[pa] += s[v][tmp];
		}
	}
	for(int i = 1; i <= m[u]; i++)
		if(sum[i] == s[u][i])
			res++;
	return res;
}

int solve2()
{
	int res = 0;
	memset(sum, 0, sizeof(sum));
	for(int i = 1; i <= n; i++)
	{
		int pa = id[0][i], tmp = id[1][i];
		if(sum[pa] == 0)
		{
			if(s[1][tmp] == s[0][pa])
				sum[pa] = tmp;
			else
				sum[pa] = -1;
		}
		else if(sum[pa] != tmp)
			sum[pa] = -1;
	}
	for(int i = 1; i <= m[0]; i++)
		if(sum[i] > 0)
			res++;
	return res;
}

int main()
{
	scanf("%d", &T);
	for(int cs = 1; cs <= T; cs++)
	{
		init();
		scanf("%d", &n);
		for(int i = 1; i <= n; i++)
		{
			scanf("%d%d", &r[0][i].u, &r[1][i].u);
			r[0][i].id = r[1][i].id = i;
		}
		sort(r[0] + 1, r[0] + 1 + n);
		sort(r[1] + 1, r[1] + 1 + n);		
		for(int k = 0; k <= 1; k++)
		{
			for(int i = 1; i <= n; i++)
			{
				id[k][r[k][i].id] = id[k][r[k][i - 1].id];
				if(i == 1 || r[k][i].u != r[k][i - 1].u)
					id[k][r[k][i].id]++;
			}
			m[k] = id[k][r[k][n].id];
		}
		for(int k = 0; k <= 1; k++)
			for(int i = 1; i <= n; i++)
				s[k][id[k][i]]++;
		ans[1] = solve(0, 1);
		ans[2] = solve(1, 0);
		ans[3] = solve2();
		printf("Case #%d: %d %d %d\n", cs, ans[1] - ans[3], ans[2] - ans[3], ans[3]);
	}
	return 0;
}
