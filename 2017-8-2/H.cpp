#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

int m, n, ans = 0;

long long f(int x, int k) {
	long long res = 1;
	for (int i = 1; i <= k; i++)
		res *= x;
	return res;
}

int main() {
	freopen("H.in", "r", stdin);
	scanf("%d%d", &m, &n);
	for (int i = 0; i <= m; i++)
		for (int j = i; j <= m; j++)
			for (int k = j; k <= m; k++)
				if (f(i, 2) + f(j, 2) == f(k, 2)) ans++;
	
	for (int i = 3; i <= n; i++)
		ans += m + 1;
	printf("%d\n", ans);
}
