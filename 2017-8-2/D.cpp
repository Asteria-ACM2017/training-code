#include <bits/stdc++.h>
using namespace std;

int x, y, n, t, ans;
int a[5];
int d[4][2] = {1, 3, 2, 3, 2, 4, 1, 4};

void dfs(int k)
{
	if(k == 4)
	{
		for(int i = 1; i <= 4; i++)
			if(a[i] % t != 0)
				return;
		ans = 1;
		return;
	}
	for(int i = 0; i <= 1; i++)
	{
		a[d[k][i]]++;
		dfs(k + 1);
		a[d[k][i]]--;
		if(ans)
			return;
	}
}

bool check()
{
	ans = 0;
	a[1] = a[2] = x - 2;
	a[3] = a[4] = y - 2;
	dfs(0);
	if(ans)
		return 1;
	return 0;
}

int main()
{
	freopen("D.in", "r", stdin);
	scanf("%d%d%d", &x, &y, &n);
	for(int i = 1; i <= n; i++)
	{
		scanf("%d", &t);
		if(check())
			puts("YES");		
		else
			puts("NO");
	}
	return 0;
}
