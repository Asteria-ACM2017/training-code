#include <bits/stdc++.h>
using namespace std;

const int maxn = 60005;
const int maxs = 300;

int n, m, q, avg, N;
int a[maxn], c[maxn], id[maxn], l[maxn], r[maxn];
int tag[maxs], b[maxs][maxs];

void update(int i)
{
    for(int j = l[i]; j <= r[i]; j++)
	b[i][j - l[i] + 1] = a[j];
    sort(b[i] + 1, b[i] + r[i] - l[i] + 2);
}

void init()
{
    memset(id, 0, sizeof(id));
    memset(l, 0, sizeof(l));
    memset(r, 0, sizeof(r));
    memset(tag, 0, sizeof(tag));
    memcpy(c, a, sizeof(c));
    sort(c + 1, c + 1 + n);
    avg = c[(n + 1) / 2];
    N = sqrt(n);
    m = (n + N - 1) / N;
    for(int i = 1; i <= n; i++)
    {
	id[i] = (i - 1) / N + 1;
	r[id[i]] = i;
    }
    for(int i = 2; i <= m; i++)
	l[i] = r[i - 1] + 1;
    l[1] = 1;
    for(int i = 1; i <= m; i++)
	update(i);
}

void add(int id, int x, int y)
{
    for(int i = x; i <= y; i++)
	a[i]++;
    update(id);
}

int calc(int id, int num)
{
    int L = 0, R = r[id] - l[id] + 1;
    while(L < R)
    {
	int mid = ((L + R) >> 1) + 1;
	if(b[id][mid] <= num)
	    L = mid;
	else
	    R = mid - 1;
    }
    return L;
}

int cal(int num)
{
    int res = 0;
    for(int i = 1; i <= m; i++)
	res += calc(i, num - tag[i]);
    return res;
}

int solve(int x, int y)
{
    int s = id[x] + 1 - (id[x] != id[x - 1]);
    int t = id[y] - 1 + (id[y] != id[y + 1]);
    for(int i = s; i <= t; i++)
	tag[i]++;
    if(id[x] == id[y] && y - x != r[id[x]] - l[id[x]])
    {
	add(id[x], x, y);
    }
    else
    {
	if(id[x] != s)
	    add(id[x], x, r[id[x]]);
	if(id[y] != t)
	    add(id[y], l[id[y]], y);
    }
    int s1 = cal(avg), s2 = cal(avg - 1), mid = (n + 1) / 2;
    if(!(s1 >= mid && s2 < mid))
	avg++;
    return avg;
	
}

int main()
{
    freopen("F.in", "r", stdin);
    int x, y;
    while(scanf("%d%d", &n, &q) && n + q != 0)
    {
	for(int i = 1; i <= n; i++)
	    scanf("%d", &a[i]);
	init();
	for(int i = 1; i <= q; i++)
	{
	    scanf("%d%d", &x, &y);
	    printf("%d\n", solve(min(x, y), max(x, y)));	
	}
    }
    return 0;
}
