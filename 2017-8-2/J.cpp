#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

const int N = 3e4 + 5;
int n, m, x[N], y[N], p[N], d[N];
int l[N], r[N], h[N << 2], flag[N << 2];
bool b[N];

void modify(int x, int ll, int rr, int l, int r) {
	//printf("%d %d %d %d %d\n", x, ll, rr, l, r);
	if (ll == l && rr == r) {
		h[x] = rr - ll + 1 - h[x]; flag[x] ^= 1;
		return;
	}
	
	int m = (ll + rr) / 2, L = x * 2, R = L + 1;
	if (flag[x]) {
		flag[x] = 0;
		h[L] = m - ll + 1 - h[L]; flag[L] ^= 1;
		h[R] = rr - m - h[R]; flag[R] ^= 1;
	}
	if (r <= m) modify(L, ll, m, l, r);
	else if (l >= m + 1) modify(R, m + 1, rr, l, r);
	else {
		modify(L, ll, m, l, m);
		modify(R, m + 1, rr, m + 1, r);
	}
	h[x] = h[L] + h[R];
}

int main() {
	freopen("J.in", "r", stdin);
	scanf("%d", &n);
	scanf("%d", &m);
	for (int i = 1; i <= m; i++)
		scanf("%d%d%d%d", &x[i], &y[i], &p[i], &d[i]);
	
	int ans = 0, t;
	for (int i = 1; i <= n; i++) {
		for (int j = 1; j <= m; j++) {
			b[j] = 0;
			if (d[j] == 0) {
				t = i - x[j];
				if (t < 1 || t > p[j]) continue;
				l[j] = y[j] - p[j] + t; r[j] = y[j];
			} else if (d[j] == 1) {
				t = i - x[j];
				if (t < 1 || t > p[j]) continue;
				l[j] = y[j] + 1; r[j] = y[j] + p[j] - t + 1;
			} else if (d[j] == 2) {
				t = x[j] - i;
				if (t < 0 || t > p[j] - 1) continue;
				l[j] = y[j] + 1, r[j] = y[j] + p[j] - t;
			} else {
				t = x[j] - i;
				if (t < 0 || t > p[j] - 1) continue;
				l[j] = y[j] - p[j] + t + 1; r[j] = y[j];
			}
			//printf("%d %d %d %d\n", i, j, l, r);
			l[j] = max(1, l[j]); r[j] = min(n, r[j]);
			if (l[j] <= r[j]) b[j] = 1, modify(1, 1, n, l[j], r[j]);
		}
		ans += h[1];
		//printf("%d %d\n", i, h[1]);
		for (int j = 1; j <= m; j++)
			if (b[j]) modify(1, 1, n, l[j], r[j]);
	}
	printf("%d\n", ans);
	return 0;
}
