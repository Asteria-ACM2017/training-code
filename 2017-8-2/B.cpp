#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

const int N = 2e5 + 5;
int n, m, h[N], len, lcp[N];
int c[20], sa[N], sb[N], a[N], rnk[N], height[N];
char s[N];

void calsa(int n, int m) {
	for (int i = 1; i <= n; i++) rnk[i] = s[i] - '0';
	for (int i = 1; i <= m; i++) c[i] = 0;
	for (int i = 1; i <= n; i++) c[rnk[i]]++;
	for (int i = 2; i <= m; i++) c[i] += c[i - 1];
	for (int i = n; i >= 1; i--) sa[c[rnk[i]]--] = i;
	for (int k = 1; k < n; k <<= 1) {
		int t = 0, j = n - k + 1;
		for (int i = j; i <= n; i++) sb[++t] = i;
		for (int i = 1; i <= n; i++) if (sa[i] > k) sb[++t] = sa[i] - k;
		for (int i = 1; i <= m; i++) c[i] = 0;
		for (int i = 1; i <= n; i++) c[rnk[i]]++;
		for (int i = 2; i <= m; i++) c[i] += c[i - 1];
		for (int i = n; i >= 1; i--) sa[c[rnk[sb[i]]]--] = sb[i];
		a[sa[1]] = t = 1;
		for (int i = 2; i <= n; i++)
			if (rnk[sa[i]] == rnk[sa[i - 1]] && rnk[sa[i] + k] == rnk[sa[i - 1] + k])
				a[sa[i]] = t; else a[sa[i]] = ++t;
		for (int i = 1; i <= n; i++) rnk[i] = a[i];
		if (t == n) break; m = t;
	}
}

void calheight(int n) {
	int t = 0;
	for (int i = 1; i <= n; i++) {
		if (rnk[i] == 1) height[1] = t = 0;
		else {
			if (t > 0) t--;
			int j = sa[rnk[i] - 1];
			while (i + t <= n && j + t <= n && s[i + t] == s[j + t]) t++;
			height[rnk[i]] = t;
		}
	}
}

bool check(int k) {
	lcp[k] = n * 2;
	for (int i = k + 1; i <= n * 2; i++)
		lcp[i] = min(lcp[i - 1], height[i]);
	
	for (int i = 1; i <= len; i++) {
		int pos = i;
		int cnt = 0;
		while (pos < i + n) {
		//printf("%d %d\n", pos, cnt);
			if (cnt > m) break;
			if (rnk[pos > n ? pos - n : pos] <= k || lcp[rnk[pos > n ? pos - n : pos]] >= len) pos += len;
			else pos += len - 1;
			cnt++;
		}
		//puts("");
		if (cnt <= m) return 1;
	}
	return 0;
}

int main() {
	freopen("B.in", "r", stdin);
	scanf("%d%d", &n, &m);
	scanf("%s", s + 1);
	for (int i = 1; i <= n; i++) s[n + i] = s[i];
	len = (n - 1) / m + 1;
	
	calsa(n * 2, 9);
	calheight(n * 2);
	int t = 0;
	for (int i = 1; i <= n * 2; i++)
		if (sa[i] <= n) h[++t] = i;
	/*for (int i = 1; i <= n * 2; i++)
		printf("%d %d %d\n", sa[i], rnk[i], height[i]);*/
		
	int l = 1, r = n;
	while (l < r) {
		int m = (l + r) / 2;
		if (check(h[m])) r = m;
		else l = m + 1;
	}
	int ans = sa[h[l]];
	for (int i = 1; i <= len; i++)
		printf("%c", s[ans + i - 1]);
	puts("");
	return 0;
}
