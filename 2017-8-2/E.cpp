#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

const int N = 1e5 + 5;

int n, minx, miny, maxx, maxy;
int X, Y, D;
int x[N], y[N];

int calc(int p, int q) {
	int mn = 1e8;
	for (int i = 1; i <= n; i++)
		mn = min(mn, abs(x[i] - p) + abs(y[i] - q));
	return mn;
}

int main() {
	freopen("E.in", "r", stdin);
	scanf("%d", &n);
	minx = miny = 1e8;
	maxx = maxy = -1e8;
	for (int i = 1; i <= n; i++) {
		scanf("%d%d", &x[i], &y[i]);
		minx = min(minx, x[i]);
		maxx = max(maxx, x[i]);
		miny = min(miny, y[i]);
		maxy = max(maxy, y[i]);
	}
	
	minx--; maxx++;
	miny--; maxy++;
	X = 2 * (maxx - minx);
	Y = 2 * (maxy - miny);
	
	D = 0;
	D += calc(minx, miny) - 1;
	D += calc(minx, maxy) - 1;
	D += calc(maxx, miny) - 1;
	D += calc(maxx, maxy) - 1;
	printf("%.6lf\n", X + Y + D * (sqrt(2.0) - 2));
	return 0;
}
