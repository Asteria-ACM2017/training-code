#include <bits/stdc++.h>
using namespace std;

#define mp make_pair

typedef pair<int, int> pii;

const int maxn = 10005;

int n, ans;
int a[maxn], l[maxn], r[maxn];
set <pii> s;

void update(int x, int val)
{
	if(a[x] < 0)
		s.erase(mp(a[x], x));
	a[x] -= val;
	if(a[x] < 0)
		s.insert(mp(a[x], x));
}

int main()
{
	freopen("A.in", "r", stdin);
	scanf("%d", &n);
	for(int i = 1; i <= n; i++)
	{
		scanf("%d", &a[i]);
		if(a[i] < 0)
			s.insert(mp(a[i], i));
	}
	for(int i = 1; i < n; i++)
		r[i] = i + 1;
	for(int i = 2; i <= n; i++)
		l[i] = i - 1;
	l[1] = n, r[n] = 1;
	while(!s.empty())
	{
		ans++;
		pii u = *s.rbegin();
		s.erase(u);
		int id = u.second, val = -u.first;
		a[id] = val;
		update(l[id], val);
		update(r[id], val);
	}
	printf("%d\n", ans);
	return 0;
}
