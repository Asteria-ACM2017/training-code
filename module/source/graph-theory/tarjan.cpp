//强连通分量
void tarjan(int u)
{
	d[u] = l[u] = ++timer;
	vst[u] = 1;
	st[++top] = u;
	for(int i = 1; i <= n; i++)
		if(mp[u][i])
		{
			if(!d[i])
			{
				tarjan(i);
				l[u] = min(l[u], l[i]);
			}
			else if(vst[i])
			{
				l[u] = min(l[u], d[i]);
			}
		}
	if(l[u] == d[u])
	{
		int v;
		m++;
		while(true)
		{
			v = st[top];
			top--;
			id[v] = m;
			vst[v] = 0;
			size[m]++;
			if(u == v)
				break;
		}
	}
}
