int up[205], down[205];
long long c[205], a[205];
int m;
long long n;
//know a0, a1, ..., a(m-1)
//a(n) = c(0) * a(n - m) + ... + c(m - 1) * a(n - 1)
//a(n) = v(0) * a(0) + ... + v(m - 1) * a(m - 1)
void linear_recurrence(long long n, int m) {
	long long v[M] = {1 % mod}, u[M << 1], msk = !!n;
	for (long long i = n; i > 1; i >>= 1) {
		msk <<= 1;
	}
	for (long long x = 0; msk; msk >>= 1, x <<= 1) {
		memset(u, 0, sizeof(u));
		int b = (!!(n & msk));
		x |= b;
		if(x < m) {
			u[x] = 1 % mod;
		}
		else {
			for (int i = 0; i < m; i++) {
				for (int j = 0, t = i + b; j < m; j++, t++) {
					u[t] = (u[t] + v[i] * v[j]) % mod;
				}
			}
			for (int i = ((m << 1) - 1); i >= m; i--) {
				for (int j = 0, t = i - m; j < m; j++, t++) {
					u[t] = (u[t] + c[j] * u[i]) % mod;
				}
			}
		}
		std::copy(u, u + m, v);
	}
	ans = 0ll;
	for (int i = 0; i < m; i++) {
		ans = (ans + v[i] * a[i] % mod) % mod;
	}

}
