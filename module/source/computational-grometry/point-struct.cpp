struct edge
{
	int v, c, nex;
}e[maxm];

const double pi = acos(-1.0);
const double eps = 1e-6;
int sign(double x) {
	return (x < -eps) ? -1 : (x > eps);
}
struct point{
	double x, y;
	point (double x = 0, double y = 0) : x(x), y(y) {}
	point operator + (const point & rhs) const {
		return point(x + rhs.x, y + rhs.y);
	}
	point operator - (const point & rhs) const {
		return point(x - rhs.x, y - rhs.y);
	}
	point operator * (const double &k) const {
		return point(x * k, y * k); 
	}
	point operator / (const double &k) const {
		return point(x / k, y / k); 
	}
	double len2() const{
		return x * x + y * y;
	}
	double len() const {
		return sqrt(len2());
	}
	point turn90() {
		return point(-y, x);
	}
};

struct line {
	point a, b;
	line() {}
	line(point a, point b) : a(a), b(b) {}
};

double dot(const point &a, const point &b) {
	return a.x * b.x + a.y * b.y;
}

double det(const point &a, const point &b) {
	return a.x * b.y - a.y * b.x;
}

bool isLL(const line &l1, const line &l2, point &p) {
	double s1 = det(l2.b - l2.a, l1.a - l2.a);
	double s2 = -det(l2.b - l2.a, l1.b - l2.a);
	if (!sign(s1 + s2)) return false;
	p = (l1.a * s2 + l1.b * s1) / (s1 + s2);	
	return true;
}

bool onSeg(const point &p, const line &l) {
	return sign(det(p - l.a, l.b - l.a)) == 0 && sign(dot(p - l.a, p - l.b)) <= 0;
}


