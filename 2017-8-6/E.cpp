#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

const int maxn = 5e4 + 5;
const int maxm = 2e5 + 5;
const int maxl = 65;

int T, n, m, cnt, tot;
int tail[maxn];
bool vst[maxn];
ll ans;
ll val[maxn], a[maxm], b[maxl];
struct edge
{
	int v, next;
	ll c;
}e[maxm];

void init()
{
	tot = cnt = ans = 0;
	memset(tail, 0, sizeof(tail));
	memset(vst, 0, sizeof(vst));
	memset(val, 0, sizeof(val));
}

void ins(int u, int v, ll c)
{
	e[++cnt] = (edge){v, tail[u], c};
	tail[u] = cnt;
}

void dfs(int u)
{
	vst[u] = 1;
	for(int i = tail[u]; i; i = e[i].next)
	{
		if(!vst[e[i].v])
		{
			val[e[i].v] = val[u] ^ e[i].c;
			dfs(e[i].v);
		}
		else
			a[++tot] = val[u] ^ val[e[i].v] ^ e[i].c;
	}
}

ll solve()
{
	ll res = 0;
	memset(b, 0, sizeof(b));
	for(int i = 1; i <= tot; i++)
		for(int j = 60; j >= 0; j--)
			if((a[i] >> j) & 1)
			{
				if(!b[j])
				{
					b[j] = a[i];
					break;
				}
				a[i] ^= b[j];
			}
	for(int i = 60; i >= 0; i--)
		res = max(res, res ^ b[i]);
	return res;
}

int main()
{
	int u, v;
	ll c;
	scanf("%d", &T);
	for(int cs = 1; cs <= T; cs++)
	{
		init();
		scanf("%d%d", &n, &m);
		for(int i = 1; i <= m; i++)
		{
			scanf("%d%d%lld", &u, &v, &c);
			ins(u, v, c);
			ins(v, u, c);
		}
		dfs(1);
		ans = solve();
		printf("Case #%d: %lld\n", cs, ans);
	}
	return 0;
}
