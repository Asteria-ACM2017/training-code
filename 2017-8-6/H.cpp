#include <bits/stdc++.h>
using namespace std;

#define mp make_pair
#define pii pair<int, int>

const int maxn = 10;

int T, base, m, ans;
int bin[1 << 5];
int vx[maxn], vy[maxn], vz[maxn], id[maxn][maxn];
pii b[maxn * maxn];
char c[maxn][maxn];

void add(int x, int y, int num)
{
	num = 1 << num;
	vx[x] ^= num;
	vy[y] ^= num;
	vz[id[x][y]] ^= num;
}

void init()
{
	for(int i = 1; i <= 4; i++)
		vx[i] = vy[i] = vz[i] = base;
	ans = m = 0;
}

void print()
{
	for(int i = 1; i <= 4; i++)
	{
		for(int j = 1; j <= 4; j++)
			printf("%c", c[i][j]);
		puts("");
	}
}

void dfs(int k)
{
	if(k > m)
	{
		ans = 1;
		return;
	}
	if(ans)
		return;
	int x = b[k].first, y = b[k].second;
	int u = vx[x] & vy[y] & vz[id[x][y]];
	for(int tmp = u, i = tmp & -tmp; tmp; tmp -= i, i = tmp & -tmp)
	{
		int j = bin[i];
		c[x][y] = j + '0';
		add(x, y, j);
		dfs(k + 1);
		if(ans)
			return;
		add(x, y, j);
	}
}

int main()
{
	for(int i = 1, now = 0; i <= 3; i += 2)
		for(int j = 1; j <= 3; j += 2)
		{
			now++;
			for(int k = i; k < i + 2; k++)
				for(int l = j; l < j + 2; l++)
					id[k][l] = now;
		}
	for(int i = 1; i <= 4; i++)
	{
		base += 1 << i;
		bin[1 << i] = i;
	}	
	scanf("%d", &T);
	for(int cs = 1; cs <= T; cs++)
	{
		printf("Case #%d:\n", cs);
		init();
		for(int i = 1; i <= 4; i++)
			scanf("%s", c[i] + 1);
		for(int i = 1; i <= 4; i++)
			for(int j = 1; j <= 4; j++)
				if(c[i][j] == '*')
					b[++m] = mp(i, j);
				else
					add(i, j, c[i][j] - '0');
		dfs(1);
		print();
	}
	return 0;
}
