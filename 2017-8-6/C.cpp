#include <bits/stdc++.h>
using namespace std;

const int maxn = 1005;
const int N = 1001;
const int mod = 1e9 + 7;

int n, m, ans, T;
int f[maxn][maxn], a[maxn], c[maxn];
struct rev
{
	int u, id;
	bool operator< (const rev &a) const{
		return u < a.u;
	}
} r[maxn];

int query(int x)
{
	x++;
	int res = 0;
	for(int i = x; i > 0; i -= i & -i)
	{
		res += c[i];
		if(res >= mod)
			res -= mod;
	}
	return res;
}

void add(int x, int val)
{
	x++;
	for(int i = x; i <= n + 2; i += i & -i)
	{
		c[i] += val;
		if(c[i] >= mod)
			c[i] -= mod;
	}
}

void init()
{
	ans = 0;
	memset(f, 0, sizeof(f));
	memset(c, 0, sizeof(c));
	add(0, 1);
}

int main()
{
	scanf("%d", &T);
	for(int cs = 1; cs <= T; cs++)
	{
		init();
		scanf("%d%d", &n, &m);
		for(int i = 1; i <= n; i++)
		{
			scanf("%d", &r[i].u);
			r[i].id = i;
		}
		sort(r + 1, r + 1 + n);
		for(int i = 1; i <= n; i++)
		{
			a[r[i].id] = a[r[i - 1].id];
			if(i == 1 || r[i].u != r[i - 1].u)
				a[r[i].id]++;
		}
/*		for(int i = 1; i <= n; i++)
			printf("%d ", a[i]);
		puts("");*/
		for(int j = 1; j <= m; j++)
		{
			for(int i = 1; i <= n; i++)
			{
				f[i][j] = query(a[i] - 1);
				add(a[i], f[i][j - 1]);
//				printf("%d %d %d\n", i, j, f[i][j]);
			}
			memset(c, 0, sizeof(c));
		}
		for(int i = 1; i <= n; i++)
		{
			ans += f[i][m];
			if(ans >= mod)
				ans -= mod;
		}
		printf("Case #%d: %d\n", cs, ans);
	}
	return 0;
}

