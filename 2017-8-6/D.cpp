#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

const int N = 1005;
const int M = 2005;

struct line {
	int l, v;
} a[N];
bool cmp(line a, line b) {
	return a.l < b.l;
}

int n, L;
long long f[N][M];

int main() {
	int T; scanf("%d", &T);
	for (int cs = 1; cs <= T; cs++) {
		scanf("%d%d", &n, &L);
		for (int i = 1; i <= n; i++)
			scanf("%d%d", &a[i].l, &a[i].v);
		sort(a + 1, a + n + 1, cmp);
		
		memset(f, 0, sizeof(f));
		for (int i = 1; i <= n; i++)
			for (int j = 0; j <= L; j++) {
				f[i][j] = f[i - 1][j];
				if (j >= a[i].l) f[i][j] = max(f[i][j], f[i - 1][j - a[i].l] + a[i].v);
			}

		long long ans = 0;
		for (int i = 1; i <= n; i++) ans = max(ans, (long long)a[i].v);
		for (int i = 1; i < n; i++)
			for (int j = i + 1; j <= n; j++)
				if (L - (a[i].l + a[j].l + 1) / 2 >= 0)
					ans = max(ans, f[i - 1][L - (a[i].l + a[j].l + 1) / 2] + a[i].v + a[j].v);
		printf("Case #%d: %lld\n", cs, ans);
	}
	return 0;
}
