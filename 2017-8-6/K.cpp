#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

const int maxn = 4005;
const ll inf = 1e17;

int T, n;
int p[2][maxn];
ll ans;
ll s[2][maxn], t[2][maxn], L[2][maxn], R[2][maxn], f[2][maxn];

void init()
{
	ans = inf;
	memset(s, 0, sizeof(s));
	memset(t, 0, sizeof(t));
	memset(L, 0, sizeof(L));
	memset(R, 0, sizeof(R));
}

ll getl(int l, int r, int k)
{
	if(l > r)
		return 0;
	ll len = r - l + 1;
	return L[k][r] - L[k][l - 1] - len * s[k][l - 1];
}

ll getr(int l, int r, int k)
{
	if(l > r)
		return 0;
	ll len = r - l + 1;
	return R[k][l] - R[k][r + 1] - len * t[k][r + 1];
}

ll cost(int l, int r, int k)
{
	if(l > r)
		return 0;
	int len = r - l + 1, mid = l + len / 2;
	return getr(l, mid - 1, k) + getl(mid, r, k);
}

int main()
{
	scanf("%d", &T);
	for(int cs = 1; cs <= T; cs++)
	{
		init();
		scanf("%d", &n);
		for(int i = 1; i <= n; i++)
			scanf("%d%d", &p[0][i], &p[1][i]);
		for(int k = 0; k < 2; k++)
		{
			for(int i = 1; i <= n; i++)
			{
				s[k][i] = s[k][i - 1] + p[k][i];
				L[k][i] = s[k][i] + L[k][i - 1];
			}
			for(int i = n; i >= 1; i--)
			{
				t[k][i] = t[k][i + 1] + p[k][i];
				R[k][i] = t[k][i] + R[k][i + 1];
			}
		}
		for(int i = 1; i <= n; i++)
			for(int k = 0; k < 2; k++)
			{
				f[k][i] = getl(1, i, k ^ 1);
				for(int j = 1; j < i; j++)
					f[k][i] = min(f[k][i], f[k ^ 1][j] + cost(j + 1, i, k ^ 1));
			}
		for(int i = 1; i < n; i++)
			for(int k = 0; k < 2; k++)
				ans = min(ans, f[k][i] + getr(i + 1, n, k));			
		printf("Case #%d: %lld\n", cs, ans);
	}
	return 0;
}
