#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<queue>
using namespace std;

const int N = 2e5 + 5;
const long long inf = 1e17;

int n, m, x[N], y[N], c[N], tp[N];
long long dis[N], cst[N];
int tot, id[N], nxt[N], lst[N];
bool vis[N];
priority_queue<pair<int, int> > Q;

void add(int x, int y, int c) {
	id[++tot] = y; nxt[tot] = lst[x]; lst[x] = tot; cst[tot] = c;
}

void dij(int S) {
	for (int i = 1; i <= m + 1; i++) dis[i] = inf, vis[i] = 0;
	while (!Q.empty()) Q.pop();
	dis[S] = 0; Q.push(make_pair(0, S));
	while (!Q.empty()) {
		int x = Q.top().second; Q.pop();
		if (vis[x]) continue;
		vis[x] = 1;
		for (int y, i = lst[x]; i; i = nxt[i]) {
			y = id[i];
			if (vis[y]) continue;
			if (dis[x] + cst[i] < dis[y]) {
				dis[y] = dis[x] + cst[i];
				Q.push(make_pair(-dis[y], y));
			}
		}
	}
}

int main() {
	int T; scanf("%d", &T);
	for (int cs = 1; cs <= T; cs++) {
		tot = 0; memset(lst, 0, sizeof(lst));
		scanf("%d%d", &n, &m);
		for (int i = 1; i <= n; i++)
			scanf("%d", &x[i]);
		for (int i = 1; i <= n; i++)
			scanf("%d", &y[i]);
		for (int i = 1; i <= n; i++)
			scanf("%d", &c[i]);
		for (int i = 1; i <= n; i++)
			add(y[i], x[i], c[i]);
		for (int i = 1; i <= m; i++) {
			scanf("%d", &tp[i]);
			if (tp[i] == 0) add(m + 1, i, 0);
		}
		dij(m + 1);
		long long ans = 0;
		bool bo = 1;
		for (int i = 1; i <= m; i++)
			if (tp[i] == 2) {
				if (dis[i] == inf) bo = 0;
				else ans += dis[i];
			}
		if (!bo) ans = -1;
		printf("Case #%d: %lld\n", cs, ans);
	}
	return 0;
}
