#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<cmath>
using namespace std;

const int N = 1005;
const double inf = 1e9;
const double eps = 1e-9;
const double PI = acos(-1.0);

int sign(double x) {
	return x < -eps ? -1 : x > eps;
}
struct point {
	double x, y;
	point (double x = 0, double y = 0) : x(x), y(y) {}
	point operator + (const point &rhs) const {
		return point(x + rhs.x, y + rhs.y);
	}
	point operator - (const point &rhs) const {
		return point(x - rhs.x, y - rhs.y);
	}
	point operator * (double k) const {
		return point(x * k, y * k);
	}
	point operator / (double k) const {
		return point(x / k, y / k);
	}
	double len2() const {
		return x * x + y * y;
	}
	double len() const {
		return sqrt(len2());
	}
};
double dot(const point &a, const point &b) {
	return a.x * b.x + a.y * b.y;
}
double det(const point &a, const point &b) {
	return a.x * b.y - b.x * a.y;
}
struct line {
	point a, b;
	line () {}
	line (point a, point b) : a(a), b(b) {}
};

double ang(const point &d1, const point &d2) {
	if (sign(det(d1, d2)) == 0)
		return (sign(dot(d1, d2)) > 0) ? 0 : PI;
	if (sign(det(d1, d2)) < 0)
		return acos(dot(d1, d2) / d1.len() / d2.len());
	else return 2 * PI - acos(dot(d1, d2) / d1.len() / d2.len());
}
bool isLL(const line &l1, const line &l2, point &p) {
	double s1 = det(l2.b - l2.a, l1.a - l2.a);
	double s2 = -det(l2.b - l2.a, l1.b - l2.a);
	if (!sign(s1 + s2)) return false;
	p = (l1.a * s2 + l1.b * s1) / (s1 + s2);
	return true;
}
bool onSeg(const point &p, const line &l) {
	return sign(det(p - l.a, l.b - l.a)) == 0 && sign(dot(p - l.a, p - l.b)) < 0;
}

int n, tp;
point p[N];
line l[N];
double v[N], f[N];

int get_type() {
	double ang0 = 0, ang1 = 0, tmp;
	for (int i = 1; i <= n; i++) {
		tmp = ang(p[i - 1] - p[i], p[i + 1] - p[i]);
		ang0 += tmp;
		ang1 += 2 * PI - tmp;
	}
	return ang0 > ang1;
}
double operator*(point a,point b){
	return a.x*b.y-a.y*b.x;
}
double operator^(point a,point b){
	return a.x*b.x+a.y*b.y;
}
bool onSeg(point p,point a,point b){
	return sign(det(p - a, b - a)) == 0 && sign((a-p)^(b-p))<0;
}
bool intersect(point a,point b,point c,point d){
	if(sign((c-a)*(b-a))*sign((d-a)*(b-a))<0
		&&sign((a-c)*(d-c))*sign((b-c)*(d-c))<0)
			return true;
	if(onSeg(a,c,d)||onSeg(b,c,d)||
		onSeg(c,a,b)||onSeg(d,a,b))
			return true;
	return false;
}
bool between(double l,double m,double r){
	return sign(l-m)<=0 and sign(m-r)<=0;
}
bool onAng(point p,point a,point b,point c){
	//TODO
	if(tp)swap(a,c);
	double t=atan2(p.y-b.y,p.x-b.x);
	double t1=atan2(a.y-b.y,a.x-b.x);
	double t2=atan2(c.y-b.y,c.x-b.x);
	if(!tp){
		if(t1>t2)t2+=2*PI;
		if(between(t1,t,t2)||between(t1,t-2*PI,t2)||between(t1,t+2*PI,t2))
			return true;
		return false;
	}else{
		
	}
}
bool check(int u, int v) {
	if (u == v + 1) return true;
	//int t = sign(ang(p[v - 1] - p[v], p[u] - p[v]) - ang(p[v - 1] - p[v], p[v + 1] - p[v]));
	//if ((t < 0 && tp == 0) || (t > 0 && tp == 1)) return false;
	//point tmp;
	if(!onAng(p[v],p[u - 1],p[u],p[u + 1])){
		return false;
	}
	if(!onAng(p[u],p[v - 1],p[v],p[v + 1])){
		return false;
	}
	for (int i = 1; i <= n; i++)
		if (intersect(p[u], p[v], p[i], p[i + 1])) return false;
		//if (isLL(l[i], line(p[u], p[v]), tmp) && onSeg(tmp, l[i]) && onSeg(tmp, line(p[u], p[v]))) return false;
	return true;
}

int main() {
	int T; scanf("%d", &T);
	for (int cs = 1; cs <= T; cs++) {
		scanf("%d", &n);
		for (int i = 1; i <= n; i++)
			scanf("%lf%lf%lf", &p[i].x, &p[i].y, &v[i]);
		p[0] = p[n]; p[n + 1] = p[1];
		for (int i = 1; i <= n; i++)
			l[i] = line(p[i], p[i + 1]);
		tp = get_type();
		
		f[1] = v[1];
		double ans = f[1];
		for (int i = 2; i <= n; i++) {
			f[i] = -inf;
			for (int j = 1; j < i; j++)
				if (f[j] + v[i] - (p[i] - p[j]).len() > f[i] && check(i, j))// printf("%d %d\n", i, j);
					f[i] = max(f[i], f[j] + v[i] - (p[i] - p[j]).len());
			ans = max(ans, f[i]);
		}
		
		printf("Case #%d: %.6f\n", cs, ans);
	}
	return 0;
}
