#include <bits/stdc++.h>
using namespace std;

int T, n;

int main()
{
	scanf("%d", &T);
	for(int cs = 1; cs <= T; cs++)
	{
		scanf("%d", &n);
		printf("Case #%d: ", cs);
		printf("%d\n", 2 * n - 1);
	}
	return 0;
}

