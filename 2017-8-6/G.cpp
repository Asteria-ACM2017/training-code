#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

const int dx[4] = {0, 0, -1, 1};
const int dy[4] = {-1, 1, 0, 0};
char s[15][15];
bool vis[15][15];

bool dfs(int x, int y) {
	vis[x][y] = 1;
	bool bo = 1;
	for (int i = 0; i < 4; i++) {
		int xx = x + dx[i];
		int yy = y + dy[i];
		if (vis[xx][yy]) continue;
		if (s[xx][yy] == '.') bo = 0;
		if (s[xx][yy] == 'o') bo &= dfs(xx, yy);
	}
	return bo;
}

bool check() {
	memset(vis, 0, sizeof(vis));
	for (int i = 1; i <= 9; i++)
		for (int j = 1; j <= 9; j++) {
			if (!vis[i][j] && s[i][j] == 'o')
				if (dfs(i, j)) return 1;
		}
	return 0;
}

int main() {
	int T; scanf("%d", &T);
	for (int cs = 1; cs <= T; cs++) {
		for (int i = 1; i <= 9; i++)
			scanf("%s", s[i] + 1);
		for (int i = 1; i <= 9; i++) {
			s[0][i] = s[i][0] = 'x';
			s[10][i] = s[i][10] = 'x';
		}
		bool bo = 0;
		for (int i = 1; i <= 9; i++) {
			for (int j = 1; j <= 9; j++) {
				if (s[i][j] != '.') continue;
				s[i][j] = 'x';
				if (check()) {
					bo = 1;
					//printf("%d %d\n", i, j);
					break;
				}
				s[i][j] = '.';
			}
			if (bo) break;
		}
		if (bo) printf("Case #%d: Can kill in one move!!!\n", cs);
		else printf("Case #%d: Can not kill in one move!!!\n", cs);
	}
	return 0;
}
