#include <bits/stdc++.h>
using namespace std;

int T;
int a[5][5], b[5][5], c[5][5];

void turn()
{
	c[1][2] = a[1][1];
	c[2][2] = a[1][2];
	c[2][1] = a[2][2];
	c[1][1] = a[2][1];
	memcpy(a, c, sizeof(a));
}

bool ok()
{
	for(int i = 1; i <= 2; i++)
		for(int j = 1; j <= 2; j++)
			if(a[i][j] != b[i][j])
				return 0;
	return 1;
}

bool check()
{
	if(ok())
		return 1;
	for(int i = 1; i <= 3; i++)
	{
		turn();
		if(ok())
			return 1;
	}
	return 0;
}

int main()
{
	scanf("%d", &T);
	for(int cs = 1; cs <= T; cs++)
	{
		scanf("%d%d%d%d", &a[1][1], &a[1][2], &a[2][1], &a[2][2]);
		scanf("%d%d%d%d", &b[1][1], &b[1][2], &b[2][1], &b[2][2]);
		printf("Case #%d: ", cs);
		if(check())
			puts("POSSIBLE");
		else
			puts("IMPOSSIBLE");
	}
	return 0;
}
