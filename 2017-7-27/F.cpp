#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

long long n, f[35][2][2];
int b[35];

long long solve(long long n) {
	int t = 30;
	memset(b, 0, sizeof(b));
	while (n) {
		b[t--] = n % 5;
		n /= 5;
	}
	
	memset(f, 0, sizeof(f));
	f[0][0][1] = 1;
	for (int i = 1; i <= 30; i++) {
		for (int j = 0; j <= 1; j++) {
			for (int k = 0; k < 5; k++)
				f[i][j ^ (((i & 1) * k) & 1)][0] += f[i - 1][j][0];
			for (int k = 0; k <= b[i]; k++)
				f[i][j ^ (((i & 1) * k) & 1)][k == b[i]] += f[i - 1][j][1];
		}
	}
	
	long long ans = 0;
	for (int i = 0; i <= 1; i++)
			ans += f[30][0][i];
	return ans;
}

int main() {
	while (scanf("%lld", &n), n != -1) {
		printf("%lld\n", solve(n));
	}
	return 0;
}
