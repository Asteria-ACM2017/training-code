#include <bits/stdc++.h>
using namespace std;

const int maxn = 10;
const int maxm = 20005;

int n, day, m[maxn];
char a[maxm];
string dic[maxn][maxm];
map <string, int> f;
struct node
{
	int f;
	string s;
	node(){}
	node (int fr, string t)
	{
		f = fr;
		s = t;
	}
	bool operator < (const node & a) const {
		if(f != a.f)
			return f > a.f;
		return s < a.s;
	}
};
set <node> st;
set <node>:: iterator it;

void del(int x)
{
	for(int i = 1; i <= m[x]; i++)
	{
		int fre = f[dic[x][i]];
		f[dic[x][i]]--;
		node now = node(fre, dic[x][i]);
		st.erase(now);
		now.f--;
		if(fre > 1)
			st.insert(now);
	}
	m[x] = 0;
}

void ins(string s, int d)
{
	dic[d][++m[d]] = s;
	node now = node(f[s], s);
	if(f[s] > 0)
		st.erase(now);
	now.f++;
	st.insert(now);
	f[s]++;
}

void query(int n)
{
	printf("<top %d>\n", n);
	int last = 0;
	for(it = st.begin(); it != st.end(); it++)
	{
		node now = *it;
		if(n <= 0 && now.f != last)
			break;
		n--;
		cout << now.s << " " << now.f << endl;
		last = now.f;
	}
	puts("</top>");
}

int main()
{
	while(scanf("%s", a + 1) != EOF)
	{
		if(a[3] == 'o')
		{
			scanf("%d", &n);
			query(n);
			scanf("%s", a + 1);
		}
		else
		{
			if(++day == 7)
				day = 0;
			del(day);
			string str = "";
			while(true)
			{
				cin >> str;
				if(str == "</text>")
					break;
				if(str.length() < 4)
					continue;
				ins(str, day);
			}
		}
	} 
	return 0;
}
<<<<<<< HEAD


=======
>>>>>>> 5d0a8f86cee2be5f45b90491111e3e280f395b49
