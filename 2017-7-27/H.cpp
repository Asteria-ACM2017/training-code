#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

const int N = 1e5 + 5;
const int M = 21092013;
int n, m, t;
int l[N], r[N], f[N];
char S[N], T[N];

int main() {
	int cases; scanf("%d", &cases);
	for (int cs = 1; cs <= cases; cs++) {
		scanf("%s%s", S + 1, T + 1);
		n = strlen(S + 1);
		m = 0;
		for (int i = 1; i <= n; i++)
			if (S[i] != 'U') S[++m] = S[i];
			else if (m > 0) m--;
		n = strlen(T + 1);
		
		t = n + 1;
		for (int i = n; i >= 0; i--) {
			l[i] = t;
			if (T[i] == 'L') t = i;
		}
		t = n + 1;
		for (int i = n; i >= 0; i--) {
			r[i] = t;
			if (T[i] == 'R') t = i;
		}
		
		f[n + 1] = 0;
		for (int i = n; i >= 0; i--)
			f[i] = (f[l[i]] + f[r[i]] + 1) % M;
		
		int ans = f[0];
		for (int i = 1; i <= n; i++) {
			if (m <= 0) break;
			if (T[i] != 'U') continue;
			if (S[m--] == 'L') ans = (ans + f[r[i]] + 1) % M;
			else ans = (ans + f[l[i]] + 1) % M;
		}
		printf("Case %d: %d\n", cs, ans);
	}
	return 0;
}
<<<<<<< HEAD
=======
« Back

>>>>>>> 5d0a8f86cee2be5f45b90491111e3e280f395b49
