#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <algorithm>
using namespace std;
const int N = 105;
struct node {
	int id, num;
}a[N];
bool map[N][N];
int n, edge[N][N], size[N], color[N];
int visit[N][5];
char s[N];
void add(int x, int y) {
	if(!map[x][y]) {
		map[x][y] = 1;
		edge[x][++size[x]] = y;
		a[y].num++;
	}
	return;
}
bool cmp1(int u, int v) {
	return a[u].num > a[v].num || (a[u].num == a[v].num && a[u].id < a[v].id);
}
bool cmp2(node u, node v) {
	return u.num > v.num || (u.num == v.num && u.id < v.id);
}
void init() {
	for (int i = 1; i <= n; i++) {
		a[i].num = 0;
		a[i].id = i;
	}
	memset(map, 0, sizeof(map));
	memset(size, 0, sizeof(size));
	memset(visit, 0, sizeof(visit));
	return;
}
bool dfs(int now) {
	if(now == 1) {
		color[a[now].id] = 1;
		for (int j = 1; j <= size[a[now].id]; j++) {
				visit[edge[a[now].id][j]][1]++;
		}
		return dfs(now + 1);
	}
	if(now == n) {
		for (int i = 1; i <= 4; i++) {
			if(!visit[a[now].id][i]) {
				color[a[now].id] = i;
				//dfs(now + 1);
				return true;
			}
		}
		color[a[now].id] = 0;
		return false;
	}
	else {
		for (int i = 1; i <= 4; i++) {
			if(!visit[a[now].id][i]) {
				color[a[now].id] = i;
				for (int j = 1; j <= size[a[now].id]; j++) {
					visit[edge[a[now].id][j]][i]++;
				}
				if(dfs(now + 1))return true;
				for (int j = 1; j <= size[a[now].id]; j++) {
					visit[edge[a[now].id][j]][i]--;
				}
				
			}
		}
		color[a[now].id] = 0;
		return false;
	}
}
int main() {
	int cas = 0, nn;
	scanf("%d", &nn);
	n = nn;
	while(true) {
		
		cas++;
		
		n = nn;
		init();
		bool ret = true;
		while(scanf("%s", s) > 0) {
			if(s[0] == '\n') break;
			int len = strlen(s), tmp = 0, x, y;
			bool flag = false;
			for (int i = 0; i < len; i++) {
				if(s[i] == '-') {
					flag = true;
					x = tmp;
					tmp = 0;
				}
				else tmp = tmp * 10 + s[i] - '0';
			}
			y = tmp;
			ret = false;
			if(!flag) {
				nn = tmp;
				break;
			}
			add(x, y);
			add(y, x);
			ret = true;
		}
		for (int i = 1; i <= n; i++) {
			sort(edge[i] + 1, edge[i] + size[i] + 1, cmp1);
		}
		sort(a + 1, a + n + 1, cmp2);
		memset(color, 0, sizeof(color));
		if(!dfs(1))assert(false);
		if(cas != 1) puts("");
		for (int i = 1; i <= n; i++) {
			printf("%d %d\n", i, color[i]);
		}
		if(ret) return 0;
	}
	return 0;
}
