#include <bits/stdc++.h>
using namespace std;

typedef unsigned long long ll;

#define mp make_pair
#define pii pair<int, int> 

const int maxn = 200;
const int base = 23333;
const int mod[2] = {1000000007, 100000081};

int n, now, cnt, cs, flag;
int wd[maxn];
string ch[maxn], str;
char c;
map <pii, int> ln[6];
map <string, int> id;

void init()
{
    id.clear();
    for(int i = 1; i <= 5; i++)
	ln[i].clear();
    cnt = 1;
    id["joe"] = 1;
    ln[1][mp(1, 1)] = 1;
}

void add()
{
    now++;
    ch[now] = str;
    for(int i = str.length() - 1; i >= 0; i--)
	if(str[i] >= 'A' && str[i] <= 'Z')
	    str[i] = str[i] - 'A' + 'a';
    if(id.count(str) == 0)
	id[str] = ++cnt;
    wd[now] = id[str];
}

void learn()
{
    for(int len = 1; len <= n; len++)
	for(int i = 1; i + len - 1 <= now; i++)
	{
	    ll tmp[2] = {0, 0};
	    for(int k = 0; k <= 1; k++)
		for(int j = i; j <= i + len - 1; j++)
		    tmp[k] = (1ll * tmp[k] * base + wd[j]) % mod[k];
	    ln[len][mp(tmp[0], tmp[1])] = 1;
	}
}

int check(char c)
{
    if(c == '.' || c == ',' || c == ':' || c == ';' || c == '?' || c == '!' || c == '*' || c == '#')
	return 0; //sign
    return 1;
}

int get(char sp)  //get_sentence
{
    flag = now = 0;
    while(true)
    {
	str = "";
	c = getchar();
	while(c == ' ' || c == '\n')
	    c = getchar();
	while(check(c))
	{
	    if(c == ' ' || c == '\n')
		break;
	    str += c;
	    c = getchar();
	}
	if(str != "")
	    add();
	if(c == sp)
	    flag = 1;
	if(!check(c))
	    break;
    }
    return 1;
}

bool check()
{
    int len = min(n, now);
    for(int i = 1; i + len - 1 <= now; i++)
	for(int k = 0; k <= 1; k++)
	{
	    ll tmp[2] = {0, 0};
	    for(int k = 0; k <= 1; k++)
		for(int j = i; j <= i + len - 1; j++)
		    tmp[k] = (1ll * tmp[k] * base + wd[j]) % mod[k];
	    if(ln[len].count(mp(tmp[0], tmp[1])) == 0)
		return 0;
	}
    return 1;
}

void query()
{
    if(check())
	return;
    for(int i = 1; i <= now; i++)
    {
	pii u = mp(wd[i], wd[i]);
	if(ln[1].count(u) == 0)
	{
	    cout << "What does the word "<< "\"" << ch[i] << "\"" << " mean?" << endl;
	    ln[1][u] = 1;
	}
    }
    if(now <= 1)
    {
	learn();
	return;
    }
    cout << "What does the sentence " << "\"";
    for(int i = 1; i < now; i++)
	cout << ch[i] << " ";
    cout << ch[now];
    printf("\" mean?\n");
    learn();
}

int main()
{
//    freopen("e.in", "r", stdin);
    while(scanf("%d", &n) != EOF)
    {
	cs++;
	if(cs != 1) {
	    puts("");
	}
	printf("Learning case %d\n", cs);
	init();
	while(get('*'))
	{
	    learn();
	    if(flag)
		break;
	}
	while(get('#'))
	{
	    query();
	    if(flag)
		break;
	}
    }
    return 0;
}
