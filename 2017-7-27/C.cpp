#include <bits/stdc++.h>
using namespace std;

const int maxn = 205;

int n, m, now, q, T;
int f[maxn][maxn], ans[maxn << 1];
char op[maxn];
double d[maxn][maxn];
double x[maxn], y[maxn], z[maxn];

double sq(double x)
{
	return 1.0 * x * x;
}

double dist(int u, int v)
{
	return sqrt(sq(x[u] - x[v]) + sq(y[u] - y[v]) + sq(z[u] - z[v]));
}

void floyd()
{
	for(int k = 1; k <= n; k++)
		for(int i = 1; i <= n; i++)
			if(i != k)
				for(int j = 1; j <= n; j++)
					if(i != j && j != k)
					{
						if(d[i][j] > d[i][k] + d[k][j])
						{
							f[i][j] = k;
							d[i][j] = d[i][k] + d[k][j];
						}
					}
}

void update(int u, int v, double x)
{
	d[u][v] = min(d[u][v], x);
}

void init()
{
	for(int i = 1; i <= n; i++)
		for(int j = 1; j <= n; j++)
			d[i][j] = 1e15;
	memset(f, 0, sizeof(f));
	for(int i = 1; i <= n; i++)
		d[i][i] = 0;	
}

void print(int u, int v)
{
	if(ans[now] != u)
		ans[++now] = u;
	if(f[u][v])
	{
		print(u, f[u][v]);
		print(f[u][v], v);
	}
	if(ans[now] != v)
		ans[++now] = v;
}

int main()
{
	int u, v, cs = 0;
	while(scanf("%d%d", &n, &m) != EOF)
	{
		cs++;
		init();
		for(int i = 1; i <= n; i++)
		{
			scanf("%lf%lf%lf", &x[i], &y[i], &z[i]);
			x[i] *= 5;
		}
		for(int i = 1; i <= m; i++)
		{
			scanf("%d%d%s", &u, &v, op);
			u++, v++;
			double dis = dist(u, v);
			if(op[0] == 'l')
			{
				update(u, v, 1);
				update(v, u, 1);
			}
			else if(op[0] == 'e')
			{
				update(u, v, 1);
				update(v, u, 3 * dis);
			}
			else
			{
				update(u, v, dis);
				update(v, u, dis);				
			}
		}
		floyd();
		scanf("%d", &q);
		if(cs != 1) puts("");
		while(q--)
		{
			
			now = 0;
			scanf("%d%d", &u, &v);
			memset(ans, -1, sizeof(ans));
			print(u + 1, v + 1);
			for(int i = 1; i <= now; i++)
				printf("%d%c", ans[i] - 1, (i == now ? '\n' : ' '));
		}
		//puts("");
	}
	return 0;
}
<<<<<<< HEAD
=======
« Back

>>>>>>> 5d0a8f86cee2be5f45b90491111e3e280f395b49
