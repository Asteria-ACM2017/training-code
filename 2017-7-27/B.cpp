#include <bits/stdc++.h>
using namespace std;

const int maxn = 205;
const int maxm = 70000;
const int inf = 1e9;

int n, m, cnt, s, t, S, T, Ts;
int st[maxn], ed[maxn], p[maxn], cl[maxn][maxn];
int tail[maxn], cur[maxn], d[maxn];
struct edge
{
    int v, c, next;
} e[maxm];

void init()
{
    cnt = 1;
    memset(tail, 0, sizeof(tail));
}	

void ins(int u, int v, int c)
{
    e[++cnt] = (edge){v, c, tail[u]};
    tail[u] = cnt;
    e[++cnt] = (edge){u, 0, tail[v]};
    tail[v] = cnt;
}

void Ins(int u, int v, int b, int c)
{
    ins(S, v, b);
    ins(u, T, b);
    ins(u, v, c - b);    
}

bool bfs()
{
    queue <int> q;
    memset(d, -1, sizeof(d));
    q.push(S);
    d[S] = 0;
    while(!q.empty())
    {
	int u = q.front();
	q.pop();
	for(int i = tail[u]; i; i = e[i].next)
	    if(d[e[i].v] == -1 && e[i].c > 0)
	    {
		d[e[i].v] = d[u] + 1;
		q.push(e[i].v);
	    }
    }
    return d[T] != -1;
}

int dfs(int u, int flow)
{
    if(u == T)
	return flow;
    int used = 0;
    for(int &i = cur[u]; i; i = e[i].next)
	if(d[e[i].v] == d[u] + 1 && e[i].c > 0)
	{
	    int w = dfs(e[i].v, min(flow - used, e[i].c));
	    e[i].c -= w;
	    e[i ^ 1].c += w;
	    used += w;
	    if(used == flow)
		return used;
	}
    if(!used)
	d[u] = -1;
    return used;
}

void dinic()
{
    while(bfs())
    {
	memcpy(cur, tail, sizeof(tail));
	dfs(S, inf);
    }
}

int solve()
{
    for(int i = 1; i <= n; i++)
    {
	Ins(s, i, 0, inf);
	Ins(n + i, t, 0, inf);
	Ins(i, n + i, p[i], p[i]);
    }
    for(int i = 1; i <= n; i++)
	for(int j = 1; j <= n; j++)
	    if(ed[i] + cl[i][j] < st[j])
		Ins(n + i, j, 0, p[i]);
    dinic();
    Ins(t, s, 0, inf);
    dinic();
    return e[tail[t] ^ 1].c;
}

int main()
{
//    freopen("b.in", "r", stdin);
    scanf("%d", &Ts);
    for(int cs = 1; cs <= Ts; cs++)
    {
	scanf("%d%d", &n, &m);
	init();
	s = 0;
	t = 2 * n + 1;
	S = t + 1;
	T = t + 2;
	for(int i = 1; i <= n; i++)
	{
	    scanf("%d%d%d", &st[i], &ed[i], &p[i]);
	    p[i] = (p[i] + m - 1) / m;		
	}
	for(int i = 1; i <= n; i++)
	    for(int j = 1; j <= n; j++)
		scanf("%d", &cl[i][j]);
	printf("Case %d: %d\n", cs, solve());
    }
    return 0;
}
