#include <bits/stdc++.h>
using namespace std;

const int maxn = 1e5 + 5;

int n, m, cnt;
int tail[maxn], dep[maxn], pa[maxn][17], f[maxn][17];
struct edge
{
	int v, next, o;
}e[maxn << 1];

void ins(int u, int v, int o)
{
	e[++cnt] = (edge){v, tail[u], o};
	tail[u] = cnt;
}

void dfs(int u)
{
	for(int k = 1; k <= 16 && pa[u][k - 1]; k++)
	{
		pa[u][k] = pa[pa[u][k - 1]][k - 1];
		if(!pa[u][k])
			break;
		if(f[u][k - 1] == f[pa[u][k - 1]][k - 1])
			f[u][k] = f[u][k - 1];
		else
			f[u][k] = -1;
	}
	for(int i = tail[u]; i; i = e[i].next)
		if(e[i].v != pa[u][0])
		{
			pa[e[i].v][0] = u;
			f[e[i].v][0] = e[i].o;
			dep[e[i].v] = dep[u] + 1;
			dfs(e[i].v);
		}
}

int lca(int u, int v)
{
	if(dep[v] < dep[u])
		swap(u, v);
	for(int x = dep[v] - dep[u], i = 0; x; x >>= 1, i++)
		if(x & 1)
			v = pa[v][i];
	if(u == v)
		return u;
	for(int k = 16; k >= 0; k--)
		if(pa[u][k] != pa[v][k])
		{	
			u = pa[u][k];
			v = pa[v][k];
		}
	return pa[u][0];
}

bool query(int u, int v)
{
	int z = lca(u, v);
	for(int x = dep[u] - dep[z], i = 0; x; x >>= 1, i++)
		if(x & 1)
		{
			if(f[u][i] != 0)
				return 0;
			u = pa[u][i];
		}
	for(int x = dep[v] - dep[z], i = 0; x; x >>= 1, i++)
		if(x & 1)
		{
			if(f[v][i] != 1)
				return 0;
			v = pa[v][i];
		}
	return 1;
}

int main()
{
	int u, v;
	scanf("%d", &n);
	for(int i = 1; i < n; i++)
	{
		scanf("%d%d", &u, &v);
		ins(u, v, 1);
		ins(v, u, 0);
	}
	dfs(1);
	scanf("%d", &m);
	for(int i = 1; i <= m; i++)
	{
		scanf("%d%d", &u, &v);
		if(query(u, v))
			puts("Yes");
		else
			puts("No");
	}
	return 0;
}


