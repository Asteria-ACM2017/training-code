#include <bits/stdc++.h>
using namespace std;

typedef unsigned long long ll;

#define pii pair<int, int>
#define mp make_pair

const int maxn = 5e5 + 5;
const int base = 233;

int n, m, top, cnt;
int ans[maxn], t[maxn], tail[maxn];
ll h[maxn], bin[maxn] = {1};
char a[maxn];
struct query
{
    int l, r, id;
}q[maxn];
struct edge
{
    int v, next;
}e[maxn];
set <int> s;

void ins(int u, int v)
{
    if(u > n)
	return;
    e[++cnt] = (edge){v, tail[u]};
    tail[u] = cnt;
}

bool cmp(query a, query b)
{
    return a.r < b.r;
}

ll get(int l, int r)
{
    return h[r] - h[l - 1] * bin[r - l + 1];    
}

int lcp(int s, int t)
{
    int l = 0, r = min(n - s + 1, n - t + 1);
    while(l < r)
    {
		int mid = ((l + r) >> 1) + 1;
		if(get(s, s + mid - 1) == get(t, t + mid - 1))
			l = mid;
		else
			r = mid - 1;
    }
    return l;
}

bool check(int s, int t)  // s~n < t~n : return 1
{
    int len = lcp(s, t);
    return a[s + len] < a[t + len];
}

int main()
{
//    freopen("d.in", "r", stdin);
    scanf("%s", a + 1);   
    scanf("%d", &m);
    for(int i = 1; i <= m; i++)
    {
		scanf("%d%d", &q[i].l, &q[i].r);
		q[i].id = i;
    }
    sort(q + 1, q + 1 + m, cmp);
    n = strlen(a + 1);
    for(int i = 1; i <= n; i++)
    {
		h[i] = h[i - 1] * base + a[i];
		bin[i] = bin[i - 1] * base;
    }
    for(int i = 1, now = 1; i <= n; i++)
    {
		while(top && check(t[top], i))
		{
			ins(i + lcp(t[top], i), t[top]);
			top--;
		}
		t[++top] = i;
		s.insert(i);
		for(int j = tail[i]; j; j = e[j].next)
			s.erase(e[j].v);
		while(q[now].r == i)
		{
			ans[q[now].id] = *s.lower_bound(q[now].l);
			now++;
		}
    }
    for(int i = 1; i <= m; i++)
		printf("%d\n", ans[i]);
    return 0;
}

