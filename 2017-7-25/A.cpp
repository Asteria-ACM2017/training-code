#include <bits/stdc++.h>
using namespace std;

const int maxn = 1e4 + 5;
const int maxm = 1e5 + 5;
const int inf = 2e9;

int n, m, k, N;
int ans[maxm];
struct node
{
	int s, v;
	bool operator < (const node &a) const{
		if(s != a.s)
			return s > a.s;
		return v > a.v;
	}
	bool operator == (const node &a) const{
		return s == a.s && v == a.v;
	}
	void print(){
		printf("s = %d v = %d\n", s, v);
	}
}a[maxn], b[maxn];
struct query
{
	int id, p, s;
	bool operator < (const query &a) const{
		return s > a.s;
	}
}q[maxm];

int find(node x)
{
	int l = 1, r = n;
	while(l < r)
	{
		int mid = (l + r) >> 1;
		if(x < b[mid] || b[mid] == x)
			r = mid;
		else
			l = mid + 1;
	}
	return l;
}

int main()
{
	int x, y, st, rk;
	scanf("%d%d", &n, &k);
	for(int i = 1; i <= n; i++)
	{
		scanf("%d", &a[i].v);
		scanf("%d", &x);
		for(int j = 1; j <= x; j++)
		{
			scanf("%d", &y);
			a[i].s |= 1 << y - 1;
		}
	}
	scanf("%d", &m);
	for(int i = 1; i <= m; i++)
	{
		q[i].id = i;
		scanf("%d", &q[i].p);
		scanf("%d", &x);
		for(int j = 1; j <= x; j++)
		{
			scanf("%d", &y);
			q[i].s |= 1 << y - 1;
		}
	}
	sort(q + 1, q + 1 + m);
	for(int i = 1; i <= m; i++)
	{
		if(i == 1 || q[i].s != q[i - 1].s)
		{
			for(int j = 1; j <= n; j++)
			{
				b[j] = a[j];
				b[j].s &= q[i].s;
			}
			sort(b + 1, b + 1 + n);
		}
		node now = (node){a[q[i].p].s & q[i].s, inf};
		st = find(now);
		now.v = a[q[i].p].v;
		rk = find(now);
		ans[q[i].id] = rk - st + 1;
	}
	for(int i = 1; i <= m; i++)
		printf("%d\n", ans[i]);
	return 0;
}
