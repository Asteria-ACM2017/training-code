#include <bits/stdc++.h>
using namespace std;

#define pii pair<int, int>
#define mp make_pair

const int maxn = 5e6;

double ans;
int n, k, q, cnt, tot, now, x, y;
int t[maxn], a[maxn];
bool del[maxn];
char ch[maxn];
string str, c1, c2;
map <string, int> id;
map <pii, int> d;

int main()
{
	scanf("%d\n", &n);
	for(int i = 1; i <= n; i++)
	{
		gets(ch + 1);
		int len = strlen(ch + 1);
		for(int j = 1; j <= len; j++)
			if(ch[j] >= 'a' && ch[j] <= 'z')
				str += ch[j];
			else if(ch[j] >= 'A' && ch[j] <= 'Z')
				str += ch[j] - 'A' + 'a';
			else
			{
				if(str != "")
				{
					if(id.count(str) == 0)
						id[str] = ++cnt;
					a[++tot] = id[str];
				}
				str = "";
			}
		if(str != "")
		{
			if(id.count(str) == 0)
				id[str] = ++cnt;
			a[++tot] = id[str];
			str = "";
		}
	}
	
	scanf("%d", &k);
	for(int i = 1; i <= k; i++)
	{
		cin>>str;
		if(id.count(str) == 0)
			continue;
		int num = id[str];
		for(int j = 1; j <= tot; j++)
			if(a[j] == num)
				del[j] = 1;
	}
	for(int i = 1; i <= tot; i++)
		if(!del[i])
			a[++now] = a[i];
	tot = now;
	
	for(int i = 1; i <= tot; i++)
		t[a[i]]++;
	for(int i = 1; i < tot; i++)
	{
		pii u = mp(a[i], a[i + 1]);
		d[u]++;
	}
	
	scanf("%d", &q);
	for(int i = 1; i <= q; i++)
	{
		ans = 0;
		cin>>c1>>c2;
		x = id[c1], y = id[c2];
		pii u = mp(x, y);
		if(d.count(u) != 0)
			ans += d[u];
		if(x != y)
		{
			u = mp(y, x);
			if(d.count(u) != 0)
				ans += d[u];
		}
		ans *= 1.0 * tot / (tot - 1) * tot;
		ans /= t[x];
		ans /= t[y];
		printf("%.15f\n", ans);
	}
	return 0;
}
