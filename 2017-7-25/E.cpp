#include <bits/stdc++.h>
using namespace std;

const int N = 1e4 + 5;
int n, a[N], id[N];
vector<int> ans;

bool cmp(int i, int j) {
	return a[i] < a[j];
}

int main() {
	scanf("%d", &n);
	for (int i = 1; i <= n; i++) {
		double x; scanf("%lf", &x);
		a[i] = x * 100;
		id[i] = i;
	}
	sort(id + 1, id + n + 1, cmp);
	int cnt = 0, p = 0, q = n + 1;
	for (int i = 2; i <= n; i += 2)
		if (a[id[i]] <= -100) {
			cnt += 2;
			p = i;
		}
	for (int i = n; i >= 1; i--)
		if (a[id[i]] >= 100) {
			cnt++;
			q = i;
		}
	if (q - p > 2 && 1LL * a[id[p + 1]] * a[id[p + 2]] > 10000LL) {
		cnt += 2; p += 2;
	}
		
	ans.clear();
	if (cnt == 0) {
		long long x = 1LL * a[id[1]] * a[id[2]];
		long long y = 100LL * a[id[n]];
		if (x > y) {
			ans.push_back(id[1]);
			ans.push_back(id[2]);
		} else {
			ans.push_back(id[n]);
		}
	} else {
		int i;
		if (a[id[p]] == -100) {
			for (i = p; i >= 1; i -= 2)
				if (cnt > 2 && a[id[i - 1]] == -100) {
					cnt -= 2;
				} else break;
		} else i = p;
		for (int j = 1; j <= i; j++)
			ans.push_back(id[j]);
		for (i = q; i <= n; i++)
			if (cnt > 1 && a[id[i]] == 100) {
				cnt--;
			} else break;
		for (int j = i; j <= n; j++)
			ans.push_back(id[j]);
	}
	
	sort(ans.begin(), ans.end());
	printf("%d\n", (int)ans.size());
	for (int i = 0; i < (int)ans.size(); i++)
		printf("%d%c", ans[i], " \n"[i == (int)ans.size() - 1]);
	return 0;
}
