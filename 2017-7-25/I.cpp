#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

int main() {
	int n, m, x, y, p, q, ans;
	scanf("%d%d", &n, &m);
	scanf("%d%d", &x, &y);
	p = n / x;
	q = m / y;
	ans = p * q;
	n = n % x;
	m = m % y;
	int nowx = 0;
	for (int i = 1; i <= q; i++) {
		if (n > nowx) ans++, nowx = x;
		nowx -= n;
	}
	int nowy = 0;
	for (int i = 1; i <= p; i++) {
		if (m > nowy) ans++, nowy = y;
		nowy -= m;
	}
	if (nowx < n && nowy < m) ans++;
	printf("%d\n", ans);
	return 0;
}


