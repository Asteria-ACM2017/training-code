#include <bits/stdc++.h>
using namespace std;

const int maxn = 1e4 + 5;
const int maxm = 1e5 + 5;
const double eps = 1e-9;

int n, m, cnt, s, t, wt;
int deg[maxn], tail[maxn], out[maxn];
bool vst[maxn];
double lm[maxn], c[maxn];
struct edge
{
	int v, next;
}e[maxm];
queue <int> q;

void ins(int u, int v)
{
	e[++cnt] = (edge){v, tail[u]};
	tail[u] = cnt;
}

void bfs(int s)
{
	vst[s] = 1;
	q.push(s);
	while(!q.empty())
	{
		int u = q.front();
		q.pop();
		for(int i = tail[u]; i; i = e[i].next)
		{
			deg[e[i].v]++;
			if(!vst[e[i].v])
			{
				vst[e[i].v] = 1;
				q.push(e[i].v);
			}
		}
	}
}

int cmp(double x)
{
	if(x > eps)
		return 1;
	if(x < -eps)
		return -1;
	return 0;
}

int main()
{
	int u, v;
	scanf("%d%d", &n, &m);
	for(int i = 1; i <= n; i++)
		scanf("%lf%lf", &lm[i], &c[i]);
	for(int i = 1; i <= m; i++)
	{
		scanf("%d%d", &u, &v);
		ins(u, v);
		out[u]++;
	}
	scanf("%d%d%d", &s, &wt, &t);
	bfs(s);
	c[s] += wt;
	q.push(s);
	while(!q.empty())
	{
		int u = q.front();
		q.pop();
		if(cmp(c[u] - lm[u]) <= 0)
			continue;
		double x = 1.0 * (c[u] - lm[u]) / out[u];
		for(int i = tail[u]; i; i = e[i].next)
		{
			c[e[i].v] += x;
			if(--deg[e[i].v] == 0)
				q.push(e[i].v);			
		}
	}
	printf("%.8f\n", min(lm[t], c[t]));
	return 0;
}
