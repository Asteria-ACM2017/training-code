#include <bits/stdc++.h>
using namespace std;

const int maxn = 1005;

int n, sum, ans;
int a[maxn];

int main()
{
	scanf("%d", &n);
	for(int i = 1; i <= n; i++)
	{
		scanf("%d", &a[i]);
		sum += a[i];
	}
	for(int i = 1; i <= n; i++)
	{
		if(a[i] * n > sum)
			ans++;
	}
	printf("%d\n", ans);
	return 0;
}
