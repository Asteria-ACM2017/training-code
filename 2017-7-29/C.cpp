#include <bits/stdc++.h>
using namespace std;

const int maxn = 10;

int n;
int a[maxn][maxn], tmp[maxn], s[maxn];
bool del[maxn][maxn];

int check()
{
	int res = 0;
	memset(del, 0, sizeof(del));
	for(int i = 1; i <= 7; i++) //col
		for(int j = 1; j <= s[i]; j++)
			if(a[j][i] == s[i])
			{
				del[j][i] = 1;
				res = 1;
			}
	for(int i = 1; i <= 7; i++) //row
	{
		int l = 1, r, len;
		while(!a[i][l] && l <= 7)
				l++;
		r = l;
		while(l <= 7)
		{
			while(a[i][r + 1])
				r++;
			len = r - l + 1;
			for(int k = l; k <= r; k++)
				if(a[i][k] == len)
				{
					del[i][k] = 1;
					res = 1;
				}
			r++;
			while(!a[i][r] && r <= 7)
				r++;
			l = r;
		}
	}
	return res;
}

void ele()
{
	for(int i = 1; i <= 7; i++)
		for(int j = 1; j <= 7; j++)
			if(del[i][j])
				a[i][j] = 0;
	for(int i = 1; i <= 7; i++) //col
	{
		s[i] = 0;
		memset(tmp, 0, sizeof(tmp));
		for(int j = 1; j <= 7; j++)
			if(a[j][i])
				tmp[++s[i]] = a[j][i];
		for(int j = 1; j <= 7; j++)
			a[j][i] = tmp[j];
	}
}

bool solve(int num, int col)
{
	if(s[col] == 7)
		return 0;
	s[col]++;
	a[s[col]][col] = num;
	while(check())
		ele();
	return 1;
}

void init()
{
	memset(a, 0, sizeof(a));
	memset(s, 0, sizeof(s));
}

int main()
{
	int x, y;
	while(scanf("%d", &n) && n)
	{
		init();
		int flag = 1;
		for(int i = 1; i <= n; i++)
		{
			scanf("%d%d", &x, &y);
			if(flag)
				if(!solve(x, y))
					flag = 0;
		}
		if(!flag)
			puts("Game Over!");
		else
		{
			for(int i = 7; i >= 1; i--)
			{
				for(int j = 1; j <= 7; j++)
					if(a[i][j])
						printf("%d", a[i][j]);
					else
						printf("#");
				puts("");
			}
		}
		puts("");
	}
	return 0;
}
