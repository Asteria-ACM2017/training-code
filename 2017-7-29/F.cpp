#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
#include<vector>
#include<map>
using namespace std;

const int N = 505;
const int Min = -2147483648;
const int Max = 2147483647;

int n, nv, nl;
int f[N][N];
bool b[N][N];
int c[3];
char s[N];
string st;
vector<int> a[N][N][3];
map<string, int> mpv;
map<int, int> mpl;

void init() {
	mpv.clear();
	mpl.clear();
	nv = 0;
	nl = 0;
}

void solve() {
	for (int i = 1; i <= nl; i++) {
		for (int j = 1; j <= nv; j++) {
			for (int k = 0; k < 3; k++)
				c[k] = a[i][j][k].size();
			f[i][j] = 0;
			if (c[0] + c[1] + c[2] == 0) continue;
			int l = Min, r = Max, cnt = 0;
			for (int k = 0; k < c[0]; k++)
				l = max(l, a[i][j][0][k]);
			for (int k = 0; k < c[1]; k++)
				r = min(r, a[i][j][1][k]);
			sort(a[i][j][2].begin(), a[i][j][2].end());
			for (int k = 0; k < c[2]; k++)
				if ((k == 0 || a[i][j][2][k] > a[i][j][2][k - 1]) && a[i][j][2][k] >=l && a[i][j][2][k] <= r)
					cnt++;
			f[i][j] = min(c[0], 1) + min(c[1], 1) + cnt;
			b[i][j] = ((long long)r - l + 1 <= (long long)cnt);
			//printf("%d %d %d\n", l, r, cnt);
			//printf("%d %d %d %d\n", i, j, f[i][j], b[i][j]);
		}
	}
	
	int ans = 0;
	for (int i = 1; i <= nl; i++) {
		int mn = Max, sum = 0;
		bool bo = 0;
		for (int j = 1; j <= nv; j++) {
			if (f[i][j] == 0) continue;
			if (b[i][j]) {
				bo = 1;
				mn = min(mn, f[i][j]);
			}
			sum += f[i][j];
		}
		if (bo) ans += mn;
		else ans += sum;
	}
	printf("%d\n", n - ans);
}

int main() {
	while (scanf("%d", &n), n) {
		init();
		int x, y, l, v, op;
		for (int i = 1; i <= n; i++) {
			scanf("%s%s", s, s);
			st = ""; st = s;
			if (mpv.count(st) == 0) mpv[st] = ++nv;
			v = mpv[st];
			scanf("%s", s);
			if (s[0] == '<') op = 0;
			else if (s[0] == '>') op = 1;
			else op = 2;
			scanf("%d", &x);
			scanf("%s%s%d%s", s, s, &y, s);
			if (mpl.count(y) == 0) mpl[y] = ++nl;
			l = mpl[y];
			a[l][v][op].push_back(x);
		}
		
		solve();
		
		for (int i = 1; i <= nl; i++)
			for (int j = 1; j <= nv; j++)
				for (int k = 0; k < 3; k++)
					a[i][j][k].clear();
	}
	return 0;
}
« Back

