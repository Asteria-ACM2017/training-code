#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

const int N = 1005;
const int M = 1e6 + 5;
const int dx[8] = {0, 0, 1, -1, 1, 1, -1, -1};
const int dy[8] = {1, -1, 0, 0, 1, -1, 1, -1};

int n, m, Hx, Hy;
char s[N][N];
int n0, n1, id[2][M];
int c[N][N], x[M], y[M], dir[M];

int getdir(int x, int y) {
	if (x == Hx) {
		if (y < Hy) return 0;
		return 1;
	}
	if (y == Hy) {
		if (x < Hx) return 2;
		return 3;
	}
	if (x < Hx) {
		if (y < Hy) return 4;
		return 5;
	}
	if (x > Hx) {
		if (y < Hy) return 6;
		return 7;
	}
	return 0;
}

void walk(int tp, int n) {
	for (int i = 1; i <= n; i++) {
		int j = id[tp][i];
		c[x[j]][y[j]]--;
		x[j] += dx[dir[j]];
		y[j] += dy[dir[j]];
		c[x[j]][y[j]]++;
		if (x[j] == Hx || y[j] == Hy) dir[j] = getdir(x[j], y[j]);
	}
}

void work(int tp, int &n) {
	int t = 0;
	for (int i = 1; i <= n; i++) {
		int j = id[tp][i];
		if (c[x[j]][y[j]] < 2)
			id[tp][++t] = id[tp][i];
	}
	n = t;
}

void solve() {
	n0 = n1 = 0;
	int t = 0;
	for (int i = 1; i <= n; i++)
		for (int j = 1; j <= m; j++) {
			if (s[i][j] == '@') {
				Hx = i, Hy = j; c[i][j] = 1;
			} else if (s[i][j] == '+' || s[i][j] == '#') {
				t++; x[t] = i; y[t] = j;
				if (s[i][j] == '+') id[0][++n0] = t;
				else id[1][++n1] = t;
				c[i][j] = 1;
			} else if (s[i][j] == '*') c[i][j] = 1;
			else c[i][j] = 0;
		}
	
	for (int i = 1; i <= n0; i++) {
		int j = id[0][i];
		dir[j] = getdir(x[j], y[j]);
	}
	for (int i = 1; i <= n1; i++) {
		int j = id[1][i];
		dir[j] = getdir(x[j], y[j]);
	}
	
	while (n0 + n1 > 0) {
		walk(0, n0); walk(1, n1);
		work(0, n0); work(1, n1);
		walk(1, n1);
		work(0, n0); work(1, n1);
	}
	printf("%d\n", c[Hx][Hy] - 1);
}

int main() {
	while (scanf("%s", s[1] + 1) == 1) {
		n = 1;
		m = strlen(s[1] + 1);
		while (1) {
			scanf("%s", s[++n] + 1);
			if (s[n][1] == '$') {
				n--; break;
			}
		}
		solve();
	}
	return 0;
}
« Back

