#include <bits/stdc++.h>
using namespace std;

#define mset(a) memset(a, 0, sizeof(a))

const int maxn = 1005;
int n, m, top, timer, cnt, pos, ans, flag, now;
int d[maxn], l[maxn], st[maxn], id[maxn], size[maxn], mp[maxn][maxn], deg[maxn], tail[maxn];
bool vst[maxn];
char ch[maxn];

struct edge
{
	int v, next;
}e[maxn * maxn];

void init()
{
	flag = 1;
	m = top = timer = cnt = pos = ans = now = 0;
	mset(mp);
	mset(deg);
	mset(d);
	mset(l);
	mset(tail);
	mset(size);
	mset(vst);
}

void ins(int u, int v)
{
	e[++cnt] = (edge){v, tail[u]};
	tail[u] = cnt;
	deg[v]++;
}

void tarjan(int u)
{
	d[u] = l[u] = ++timer;
	vst[u] = 1;
	st[++top] = u;
	for(int i = 1; i <= n; i++)
		if(mp[u][i])
		{
			if(!d[i])
			{
				tarjan(i);
				l[u] = min(l[u], l[i]);
			}
			else if(vst[i])
			{
				l[u] = min(l[u], d[i]);
			}
		}
	if(l[u] == d[u])
	{
		int v;
		m++;
		while(true)
		{
			v = st[top];
			top--;
			id[v] = m;
			vst[v] = 0;
			size[m]++;
			if(u == v)
				break;
		}
	}
}

void dfs(int u)
{	
	vst[u] = 1;
	now++;
	for(int i = tail[u]; i; i = e[i].next)
		if(!vst[e[i].v])
			dfs(e[i].v);
}

int main()
{
	while(scanf("%d", &n) && n)
	{
		init();
		for(int i = 1; i < n; i++)
		{
			scanf("\n%s", ch + 1);
			for(int j = i + 1; j <= n; j++)
				if(ch[j - i] == '1')
					mp[i][j] = 1;
				else
					mp[j][i] = 1;
		}		
		for(int i = 1; i <= n; i++)
			if(!d[i])
				tarjan(i);
		for(int i = 1; i <= n; i++)
			for(int j = 1; j <= n; j++)
				if(mp[i][j] && id[i] != id[j])
					ins(id[i], id[j]);
		for(int i = 1; i <= m; i++)
			if(!deg[i])
			{
				if(pos)
					flag = 0;
				else
					pos = i;
			}
		if(flag)
			dfs(pos);
		if(now != m)
			flag = 0;
		if(!flag)
			puts("0");
		else
			printf("%d\n", size[pos]);
	}
	return 0;
}
