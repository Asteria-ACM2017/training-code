#include <bits/stdc++.h>
using namespace std;

const int maxn = 105;

int n, m, st, dir, ans, T;
int a[maxn];

int main()
{
	scanf("%d", &T);
	while(T--)
	{
		ans = 0;
		scanf("%d%d", &m, &n);
		for(int i = 1; i <= m; i++)
		{
			scanf("%d", &a[i]);
			if(a[i] == 2)
			{
				st = i;
				dir = 1;
				a[i] = 1;
			}
			else if(a[i] == 3)
			{
				st = i;
				dir = -1;
				a[i] = 1;
			}
		}
		while(n)
		{
			int nxt = st + dir;
			if(nxt > m || nxt < 1)
			{
				dir = -dir;
				nxt = st + dir;
			}
			ans += (a[nxt] == 0);
			st = nxt;
			n--;
		}
		printf("%d\n", ans);
	}
	return 0;
}
« Back

