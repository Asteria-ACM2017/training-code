#include <bits/stdc++.h>
using namespace std;

const int maxn = 30;

int n, ans, last;
int ti[25], a[25][maxn][3], r[maxn][maxn], b[3];
char ch[maxn], s[maxn][20];
int t[11][5][3] = {
    {0, 1, 0, 1, 0, 1, 0, 2, 0, 1, 0, 1, 0, 1, 0},
    {0, 2, 0, 2, 0, 1, 0, 2, 0, 2, 0, 1, 0, 2, 0},
    {0, 1, 0, 2, 0, 1, 0, 1, 0, 1, 0, 2, 0, 1, 0},
    {0, 1, 0, 2, 0, 1, 0, 1, 0, 2, 0, 1, 0, 1, 0},
    {0, 2, 0, 1, 0, 1, 0, 1, 0, 2, 0, 1, 0, 2, 0},
    {0, 1, 0, 1, 0, 2, 0, 1, 0, 2, 0, 1, 0, 1, 0},
    {0, 1, 0, 1, 0, 2, 0, 1, 0, 1, 0, 1, 0, 1, 0},
    {0, 1, 0, 2, 0, 1, 0, 2, 0, 2, 0, 1, 0, 2, 0},
    {0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0},
    {0, 1, 0, 1, 0, 1, 0, 1, 0, 2, 0, 1, 0, 1, 0},
    {0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0, 2, 0},
};

void init()
{
    ans = last = n = 0;
    memset(a, 0, sizeof(a));
    memset(b, 0, sizeof(b));
    memset(s, 0, sizeof(s));
    memset(ti, 0, sizeof(ti));
}

bool check(int num, int x, int y)
{
    for(int i = 0; i < 5; i++)
	for(int j = 0; j < 3; j++)
	{
	    if(!t[num][i][j])
		continue;
	    if(r[x + i][y + j] == 3)
		continue;
	    if(t[num][i][j] == r[x + i][y + j])
		continue;	    
	    return 0;
	}
    return 1;
}

int Read()
{
    scanf("%s\n", ch + 1);
    if(ch[1] == '$')
	return -1;
    if(ch[1] == '#')
	return -2;
    n++;
    int len = strlen(ch + 1);
    for(int i = 1; i <= len; i++)
	ti[n] = ti[n] * 10 + ch[i] - '0';
    ti[n] %= 1000;
    memset(s, 0, sizeof(s));
    for(int i = 0; i < 5; i++)
    {
	if(i & 1)
	{
	    scanf("%s", ch);s[i][0] = ch[0];
	    scanf("%s", ch);s[i][2] = ch[0];s[i][3] = ch[1];
	    scanf("%s", ch);s[i][5] = ch[0];s[i][6] = ch[1];
	    scanf("%s", ch);s[i][8] = ch[0];
	}
	else
	{
	    scanf("%s", ch);s[i][1] = ch[0];
	    scanf("%s", ch);s[i][4] = ch[0];
	    scanf("%s", ch);s[i][7] = ch[0];
	}
    }
    for(int i = 0 ; i < 5; i++)
    	for(int j = 0 ; j < 9; j++)
	{
	    if(s[i][j] == '.')
		r[i][j] = 2;
	    else if(s[i][j] == '*')
		r[i][j] = 3;
	    else
		r[i][j] = 1;
	}
    for(int k = 0; k < 3; k++)
	for(int i = 0; i <= 10; i++)
	    if(check(i, 0, k * 3))
		a[n][i][k] = 1;
    return 1;
}

bool ok(int T)
{
    for(int i = 1; i <= n; i++)
    {
	int now = (T + ti[i]) % 1000;
	for(int j = 2; j >= 0; j--, now /= 10)
	    b[j] = now % 10;
	for(int j = 0; j <= 1; j++)
	    if(b[j] == 0)
		b[j] = 10;
	    else
		break;
	for(int j = 0; j < 3; j++)
	    if(!a[i][b[j]][j])
		return 0;
    }
    return 1;
}

void work()
{
    ans = 0;
    for(int i = 0; i < 1000; i++)
	if(ok(i))
	{
	    ans++;
	    last = i;
	}
    if(ans == 1)
	printf("%d\n", last);
    else
	printf("? %d\n", ans);
}

int main()
{
    while(true)
    {
	int tmp = Read();
	if(tmp >= 0)
	    continue;
	work();
	init();
	if(tmp == -1)
	    break;
    }
    return 0;
}
