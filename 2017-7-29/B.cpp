#include <bits/stdc++.h>
using namespace std;

const int maxn = 20;

int T, res;
char a[maxn], b[maxn], c[maxn];

bool check(char s[], char t[])
{
	int n = strlen(s + 1);
	int m = strlen(t + 1);
	if(n != m)
		return 0;
	for(int i = 1; i <= n; i++)
		if(s[i] != t[i])
			return 0;
	return 1;
}

int solve()
{
	int n = strlen(a + 1), m = 0;
	if(check(a, b))
		return 0;
	memset(c, 0, sizeof(c));
	for(int i = 1; i <= n; i++)
		if(a[i] >= 'a' && a[i] <= 'z')
			c[i] = a[i] - 'a' + 'A';
		else if(a[i] >= 'A' && a[i] <= 'Z')
			c[i] = a[i] + 'a' - 'A';
		else
			c[i] = a[i];
	if(check(b, c))
		return 1;
	memset(c, 0, sizeof(c));
	for(int i = 1; i <= n; i++)
		if(!(a[i] >= '0' && a[i] <= '9'))
			c[++m] = a[i];
	if(check(b, c))
		return 2;
	for(int i = 1; i <= m; i++)
		if(c[i] >= 'a' && c[i] <= 'z')
			c[i] = c[i] - 'a' + 'A';
		else if(c[i] >= 'A' && c[i] <= 'Z')
			c[i] = c[i] + 'a' - 'A';
	if(check(b, c))
		return 3;
	return 4;
}

int main()
{
	scanf("%d", &T);
	for(int i = 1; i <= T; i++)
	{
		printf("Case %d: ", i);
		scanf("%s%s", a + 1, b + 1);
		res = solve();
		if(res == 0)
			puts("Login successful.");
		else if(res == 1)
			puts("Wrong password. Please, check your caps lock key.");
		else if(res == 2)
			puts("Wrong password. Please, check your num lock key.");
		else if(res == 3)
			puts("Wrong password. Please, check your caps lock and num lock keys.");
		else
			puts("Wrong password.");
	}
	return 0;
}
« Back

