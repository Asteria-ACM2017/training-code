#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

const int N = 3005;
int n, a[N], p[N], ans;

int main() {
	while (scanf("%d", &n), n) {
		for (int x, i = 1; i <= n; i++) {
			scanf("%d", &x);
			p[x] = i;
		}
		for (int x, i = 1; i <= n; i++) {
			scanf("%d", &x);
			a[i] = p[x];
		}
		
		int ans = 0;
		for (int i = 1; i < n; i++) {
			int mn = a[i], mx = a[i];
			for (int j = i + 1; j <= n; j++) {
				mn = min(mn, a[j]);
				mx = max(mx, a[j]);
				if (j - i == mx - mn) ans++;
			}
		}
		printf("%d\n", ans);
	}
	return 0;	
}
