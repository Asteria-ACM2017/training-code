#include <bits/stdc++.h>
using namespace std;

const int maxn = 105;
const int maxm = 3000;
const int lim = 1440;

int n, m, q, ans, last;
int a[maxn], b[maxn], c[maxn];
bool vst[maxm];

int find1(int x)
{
	int l = 1, r = m;
	if(b[m] < x)
		return -1;
	while(l < r)
	{
		int mid = (l + r) >> 1;
		if(b[mid] >= x)
			r = mid;
		else
			l = mid + 1;
	}
	return l;
}

int find2(int x)
{
	int l = 1, r = m;
	if(b[1] > x)
		return -1;
	while(l < r)
	{
		int mid = ((l + r) >> 1) + 1;
		if(b[mid] <= x)
			l = mid;
		else
			r = mid - 1;
	}
	return l;
}

void add(int x, int y, int t0)
{
	if(x > y)
		swap(x, y);
	int dt = y - x;
	if ((t0 + dt) & 1) return;
	int t = (t0 + dt) / 2 + x;
	t %= lim;
	vst[t] = 1;
}

bool check(int t0)
{
	memset(vst, 0, sizeof(vst));
	for(int i = 1; i <= n; i++)
	{
		for(int j = 1; j <= m; j++)
			if(abs(a[i] - b[j]) <= t0)
				add(a[i], b[j], t0);
	}
	for(int i = 1; i <= q; i++)
		if(!vst[c[i]])
			return 0;
	return 1;
}

int main()
{
	int hour, mi;
	while(scanf("%d%d", &n, &m) && n + m != 0)
	{
		ans = 0;
		for(int i = 1; i <= n; i++)
		{
			scanf("%d:%d", &hour, &mi);
			a[i] = 60 * hour + mi;
		}
		for(int i = 1; i <= m; i++)
		{
			scanf("%d:%d", &hour, &mi);
			b[i] = 60 * hour + mi;
		}
		scanf("%d", &q);
		for(int i = 1; i <= q; i++)
		{
			scanf("%d:%d", &hour, &mi);
			c[i] = 60 * hour + mi;
		}
		sort(a + 1, a + 1 + n);
		sort(b + 1, b + 1 + m);
		for(int i = 1; i < 1440; i++)
			if(check(i))
			{
				ans++;
				last = i;
			}
		if(ans == 0)
			puts("il bugiardo passeggeri!");
		else if(ans == 1)
		{
			hour = last / 60;
			mi = last % 60;
			if(hour < 10)
				printf("0");
			printf("%d:", hour);
			if(mi < 10)
				printf("0");
			printf("%d\n", mi);
		}
		else
			printf("%d scelte\n", ans);
	}
	return 0;
}
« Back

