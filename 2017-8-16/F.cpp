#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

const int N = 2e6 + 5;
int n;
char s[N];

int solve() {
	int pos = 0;
	for (int i = 1; i <= n; i++)
		if (s[i] == 'c') {
			pos = i;
			break;
		}
	if (pos == 0) return n / 2 + (n & 1);
	
	int ans = 0;
	int lst = pos + n - 1;
	for (int i = pos; i <= lst;) {
		i++;
		int cnt = 0;
		while (i <= lst && s[i] == 'f') i++, cnt++;
		if (cnt >= 2) ans++; else return -1;
	}
	return ans;
}

int main() {
	int T; scanf("%d", &T);
	for (int cs = 1; cs <= T; cs++) {
		scanf("%s", s + 1);
		n = strlen(s + 1);
		for (int i = 1; i <= n; i++)
			s[n + i] = s[i];
		
		printf("Case #%d: %d\n", cs, solve());
	}
	return 0;
}
