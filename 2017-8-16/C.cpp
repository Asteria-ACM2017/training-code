#include <bits/stdc++.h>
using namespace std;

const int maxn = 20005;
const int maxm = 200005;

int n, m, t, T, ans, timer, cnt, tot;
int in[maxn], out[maxn], c[maxn], tail[maxn], pa[maxn], s[maxn];
struct edge
{
	int v, next;
}e[maxn << 1];
struct Edge
{
	int u, v;
}a[maxm];
struct event
{
	int x, y, d;
}b[maxm];

namespace Reader
{
	const int L = (1 << 15) + 5;
	char buffer[L], *S, *T;
	__inline bool getchar(char &ch)
	{
		if(S == T)
		{
			T = (S = buffer) + fread(buffer, 1, L, stdin);
			if(S == T)
			{
				ch = EOF;
				return false;
			}
		}
		ch = *S++;
		return true;
	}
	__inline bool getint(int &x)
	{
		char ch;
		bool neg = 0;
		for(; getchar(ch) && (ch < '0' || ch > '9'); )
//		neg ^= ch == '-';
		if(ch == EOF)
			return false;
		x = ch - '0';
		for(; getchar(ch) , ch >= '0' && ch <= '9'; )
			x = x * 10 + ch - '0';
		return true;
	}
}

void init()
{
	ans = 300000;
	cnt = timer = 0;
	memset(tail, 0, sizeof(tail));
	memset(s, 0, sizeof(s));
}

void ins(int u, int v)
{
	e[++cnt] = (edge){v, tail[u]};
	tail[u] = cnt;
}

void dfs(int u)
{
	in[u] = ++timer;
	for(int i = tail[u]; i; i = e[i].next)
		if(e[i].v != pa[u])
		{
			pa[e[i].v] = u;
			dfs(e[i].v);
		}
	out[u] = timer;
}

bool cmp1(event a, event b)
{
	if(a.x != b.x)
		return a.x < b.x;
	return a.d < b.d;
}

bool cmp2(event a, event b)
{
	if(a.x != b.x)
		return a.x > b.x;
	return a.d < b.d;
}

void add(int x)
{
	for(int i = x; i <= n; i += i & -i)
		c[i]++;
}

int query(int x)
{
	int res = 0;
	for(int i = x; i > 0; i -= i & -i)
		res += c[i];
	return res;
}

void solve1()
{
	tot = 0;
	memset(c, 0, sizeof(c));
	for(int i = 1; i <= n; i++)
		b[++tot] = (event){in[i], out[i], 0};
	for(int i = 1; i <= t; i++)
		b[++tot] = (event){a[i].u, a[i].v, 1};
	sort(b + 1, b + 1 + tot, cmp1);
	for(int i = 1; i <= tot; i++)
		if(b[i].d)
			add(b[i].y);
		else
			s[b[i].x] += query(b[i].y) - query(b[i].x - 1);
}

void solve2()
{
	tot = 0;
	memset(c, 0, sizeof(c));
	for(int i = 1; i <= n; i++)
		b[++tot] = (event){out[i], in[i], 0};
	for(int i = 1; i <= t; i++)
		b[++tot] = (event){a[i].v, a[i].u, 1};
	sort(b + 1, b + 1 + tot, cmp2);
	for(int i = 1; i <= tot; i++)
		if(b[i].d)
			add(b[i].y);
		else
			s[b[i].y] += query(b[i].x) - query(b[i].y - 1);
}

int main()
{
	int u, v;
	Reader::getint(T);
//	scanf("%d", &T);
//	T = 5;
//	srand(time(0));
	for(int cs = 1; cs <= T; cs++)
	{
		init();
		Reader::getint(n);
		Reader::getint(m);
		//scanf("%d%d", &n, &m);
//		n = 20000;
	//	m = 200000;
		
		t = m - n + 1;
		for(int i = 1; i < n; i++)
		{
			Reader::getint(u);
			Reader::getint(v);
//			scanf("%d%d", &u, &v);
//			u = i + 1, v = rand() % i + 1;
			ins(u, v);
			ins(v, u);
		}
		for(int i = 1; i <= t; i++)
		{
			Reader::getint(a[i].u);
			Reader::getint(a[i].v);
			//scanf("%d%d", &a[i].u, &a[i].v);
//			a[i].u = rand() % n + 1;
//			a[i].v = rand() % n + 1;
		}
		dfs(1);
		for(int i = 1; i <= t; i++)
		{
			a[i].u = in[a[i].u];
			a[i].v = in[a[i].v];
			if(a[i].u > a[i].v)
				swap(a[i].u, a[i].v);
		}
		solve1();
		solve2();
		for(int i = 2; i <= n; i++)
			ans = min(ans, s[i] + 1);
		printf("Case #%d: %d\n", cs, ans);
	}
	return 0;
}
