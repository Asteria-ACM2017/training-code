#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

const int N = 5e6 + 5;
int n, a, b, t[N];
int mn, mx, a1, a2, b1, b2;

int main() {
	int T; scanf("%d", &T);
	for (int cs = 1; cs <= T; cs++) {
		scanf("%d%d%d", &n, &a, &b);
		for (int i = 1; i <= n; i++)
			scanf("%d", &t[i]);
		
		if (a >= 0) {
			mx = -1;
			for (int i = 1; i <= n; i++)
				if (abs(t[i]) > mx) {
					mx = abs(t[i]);
					a1 = i;	
				}
			mx = -1;
			for (int i = 1; i <= n; i++)
				if (abs(t[i]) > mx && i != a1) {
					mx = abs(t[i]);
					a2 = i;
				}
		} else {
			mn = N;
			for (int i = 1; i <= n; i++)
				if (abs(t[i]) < mn) {
					mn = abs(t[i]);
					a1 = i;
				}
			mn = N;
			for (int i = 1; i <= n; i++)
				if (abs(t[i]) < mn && i != a1) {
					mn = abs(t[i]);
					a2 = i;
				}
		}
		
		if (b >= 0) {
			mx = -N;
			for (int i = 1; i <= n; i++)
				if (t[i] > mx) {
					mx = t[i];
					b1 = i;
				}
			mx = -N;
			for (int i = 1; i <= n; i++)
				if (t[i] > mx && i != b1) {
					mx = t[i];
					b2 = i;
				}
		} else {
			mn = N;
			for (int i = 1; i <= n; i++)
			 	if (t[i] < mn) {
			 		mn = t[i];
			 		b1 = i;
			 	}
			 mn = N;
			 for (int i = 1; i <= n; i++)
			 	if (t[i] < mn && i != b1) {
			 		mn = t[i];
			 		b2 = i;
			 	}
		}
		
		long long ans;
		if (a1 != b1) ans = 1LL * a * t[a1] * t[a1] + 1LL * b * t[b1];
		else {
			ans = 1LL * a * t[a1] * t[a1] + 1LL * b * t[b2];
			ans = max(ans, 1LL * a * t[a2] * t[a2] + 1LL * b * t[b1]);
		}
		printf("Case #%d: %lld\n", cs, ans);
	}
}
