#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

const int maxn = 201400;
const int N = 201314;
const int mod = 530600414;

int n, T;
ll f[maxn], l[maxn], r[maxn], c[maxn], len[maxn];

int main()
{
	c[1] = len[1] = c[3] = 1;
	len[2] = 2;
	r[3] = len[3] = 3;
	for(int i = 4; i <= N; i++)
	{
		f[i] = (f[i - 1] + f[i - 2] + r[i - 2] * c[i - 1] + l[i - 1] * c[i - 2]) % mod;
		c[i] = (c[i - 1] + c[i - 2]) % mod;
		len[i] = (len[i - 1] + len[i - 2]) % mod;
		r[i] = (r[i - 2] + r[i - 1] + c[i - 2] * len[i - 1]) % mod;
		l[i] = (l[i - 2] + l[i - 1] + c[i - 1] * len[i - 2]) % mod;
	}
	scanf("%d", &T);
	for(int cs = 1; cs <= T; cs++)
	{
		scanf("%d", &n);
		printf("Case #%d: %lld\n", cs, f[n]);
	}	
	return 0;
}
