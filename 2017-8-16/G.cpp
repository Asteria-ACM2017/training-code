#include<cstdio>
#include<cstring>
#include<iostream>
#include<algorithm>
using namespace std;

const int d[20] = {6, 2, 5, 5, 4, 5, 6, 3, 7, 6, 6, 2, 5, 5, 4, 5, 6, 3, 7, 6};
int n, m;
long long f[150][555][2][2][2];

void Add(long long &x, long long y) {
	x = (x + y) % m;
}

int solve() {
	n -= 3;
	memset(f, 0, sizeof(f));
	f[0][0][0][0][0] = 1;
	for (int dig = 1; dig <= n / 4; dig++)
	for (int tot = 0; tot <= n; tot++)
	for (int add = 0; add <= 1; add++)
	for (int e1 = 0; e1 <= 1; e1++)
	for (int e2 = 0; e2 <= 1; e2++) {
		long long tmp = f[dig - 1][tot][add][e1][e2];
		if (tmp == 0) continue;
		if (e1 && e2) {
			if (add) Add(f[dig][tot + d[1]][0][1][1], tmp);
		} else if (e1) {
			for (int n2 = 0; n2 <= 9; n2++) {
				int _tot = tot + d[n2] + d[n2 + add], _add = (n2 + add >= 10);
				if (n2) Add(f[dig][_tot][_add][1][1], tmp);
				Add(f[dig][_tot][_add][1][0], tmp);
			}
		} else if (e2) {
			for (int n1 = 0; n1 <= 9; n1++) {
				int _tot = tot + d[n1] + d[n1 + add], _add = (n1 + add >= 10);
				if (n1) Add(f[dig][_tot][_add][1][1], tmp);
				Add(f[dig][_tot][_add][0][1], tmp);
			}
		} else {
			for (int n1 = 0; n1 <= 9; n1++)
				for (int n2 = 0; n2 <= 9; n2++) {
					int _tot = tot + d[n1] + d[n2] + d[n1 + n2 + add], _add = (n1 + n2 + add >= 10);
					if (n1) Add(f[dig][_tot][_add][1][0], tmp);
					if (n2) Add(f[dig][_tot][_add][0][1], tmp);
					if (n1 && n2) Add(f[dig][_tot][_add][1][1], tmp);
					Add(f[dig][_tot][_add][0][0], tmp);
				}
		}
	}
	
	long long ans = 0;
	for (int i = 1; i <= n / 4; i++)
		Add(ans, f[i][n][0][1][1]);
	
	/*for (int i = 1; i <= n / 4; i++)
		for (int j = 0; j <= n; j++)
			printf("%d %d %lld\n", i, j, f[i][j][0][1][1]);*/
	return ans;
}

int main() {
	int T; scanf("%d", &T);
	for (int cs = 1; cs <= T; cs++) {
		scanf("%d%d", &n, &m);
		printf("Case #%d: %d\n", cs, solve());
	}
	return 0;
}
