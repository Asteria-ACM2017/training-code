#include <bits/stdc++.h>
using namespace std;

#define ms(a) memset(a, 0, sizeof(a))

const int maxn = 30005;
const int maxm = 1e5 + 5;

int n, m, Q, cnt, timer, tot, T, now, num;
int tail[maxn], f[maxn], in[maxn], ans[maxm], t[maxm];
int pa[maxn], son[maxn], size[maxn], dep[maxn], top[maxn];
int val[maxn << 2], tag[maxn << 2];
struct Edge
{
	int u, v, del;
}a[maxm];
struct edge
{
	int v, next;
}e[maxn << 1], v[maxm];
struct query
{
	int op, u, v;
}q[maxm];
map <int, int> id[maxn];

namespace Reader
{
	const int L = (1 << 15) + 5;
	char buffer[L], *S, *T;
	__inline bool getchar(char &ch)
	{
		if(S == T)
		{
			T = (S = buffer) + fread(buffer, 1, L, stdin);
			if(S == T)
			{
				ch = EOF;
				return false;
			}
		}
		ch = *S++;
		return true;
	}
	__inline bool getint(int &x)
	{
		char ch;
		for(; getchar(ch) && (ch < '0' || ch > '9'); )
		if(ch == EOF)
			return false;
		x = ch - '0';
		for(; getchar(ch) , ch >= '0' && ch <= '9'; )
			x = x * 10 + ch - '0';
		return true;
	}
}
void init()
{
	cnt = timer = tot = now = num = 0;
	memset(tail, 0, sizeof(tail));
	memset(t, 0, sizeof(t));
	for(int i = 1; i <= n; i++)
		id[i].clear();
}

int find(int x)
{
	return f[x] == x ? x : f[x] = find(f[x]);
}

void ins(int u, int v)
{
	e[++cnt] = (edge){v, tail[u]};
	tail[u] = cnt;
}

void dfs(int u)
{
	size[u] = 1;
	son[u] = 0;
	for(int i = tail[u]; i; i = e[i].next)
		if(e[i].v != pa[u])
		{
			pa[e[i].v] = u;
			dep[e[i].v] = dep[u] + 1;
			dfs(e[i].v);
			size[u] += size[e[i].v];
			if(size[e[i].v] > size[son[u]])
				son[u] = e[i].v;
		}
}

void dfs(int u, int anc)
{
	in[u] = ++timer;
	top[u] = anc;
	if(son[u])
		dfs(son[u], anc);
	for(int i = tail[u]; i; i = e[i].next)
		if(e[i].v != pa[u] && e[i].v != son[u])
			dfs(e[i].v, e[i].v);
}

void build(int k, int l, int r)
{
	tag[k] = 0;
	if(l == r)
	{
		val[k] = (l != 1);
		return;
	}
	int mid = (l + r) >> 1;
	build(k << 1, l, mid);
	build(k << 1 ^ 1, mid + 1, r);
	val[k] = val[k << 1] + val[k << 1 ^ 1];
}

void add(int k, int l, int r, int s, int t)
{
	if(tag[k])
		return;
	if(s <= l && t >= r)
	{
		tag[k] = 1;
		val[k] = 0;
		return;
	}
	int mid = (l + r) >> 1;
	if(s <= mid)
		add(k << 1, l, mid, s, t);
	if(t > mid)
		add(k << 1 ^ 1, mid + 1, r, s, t);
	val[k] = val[k << 1] + val[k << 1 ^ 1];
}

int query(int k, int l, int r, int s, int t)
{
	if(tag[k])
		return 0;
	if(s <= l && t >= r)
		return val[k];
	int res = 0, mid = (l + r) >> 1;
	if(s <= mid)
		res += query(k << 1, l, mid, s, t);
	if(t > mid)
		res += query(k << 1 ^ 1, mid + 1, r, s, t);
	return res;
}

void del(int u, int v)
{	
	while(top[u] != top[v])
	{
		if(dep[top[v]] < dep[top[u]])
			swap(u, v);
		add(1, 1, n, in[top[v]], in[v]);
		v = pa[top[v]];
	}
	if(u == v)
		return;
	if(dep[v] < dep[u])
		swap(u, v);
	add(1, 1, n, in[son[u]], in[v]);
}

int solve(int u, int v)
{
	int res = 0;
	while(top[u] != top[v])
	{
		if(dep[top[v]] < dep[top[u]])
			swap(u, v);
		res += query(1, 1, n, in[top[v]], in[v]);
		v = pa[top[v]];
	}
	if(u == v)
		return res;
	if(dep[v] < dep[u])
		swap(u, v);
	res += query(1, 1, n, in[son[u]], in[v]);	
	return res;
}

int main()
{
	Reader::getint(T);
//	scanf("%d", &T);
	for(int cs = 1; cs <= T; cs++)
	{
		Reader::getint(n);			
		Reader::getint(m);
		Reader::getint(Q);
//		scanf("%d%d%d", &n, &m, &Q);
		init();
		for(int i = 1; i <= m; i++)
		{
			a[i].del = 0;
//			scanf("%d%d", &a[i].u, &a[i].v);
			Reader::getint(a[i].u);
			Reader::getint(a[i].v);
			if(a[i].u > a[i].v)
				swap(a[i].u, a[i].v);
			if(!id[a[i].u].count(a[i].v))
				id[a[i].u][a[i].v] = ++num;
			int u = id[a[i].u][a[i].v];
			v[++now] = (edge){i, t[u]};
			t[u] = now;
		}
		for(int i = 1; i <= Q; i++)
		{
//			scanf("%d%d%d", &q[i].op, &q[i].u, &q[i].v);
			Reader::getint(q[i].op);
			Reader::getint(q[i].u);
			Reader::getint(q[i].v);
			if(q[i].u > q[i].v)
				swap(q[i].u, q[i].v);
			if(q[i].op == 1)
			{
				int u = id[q[i].u][q[i].v];
				int j = v[t[u]].v;
				t[u] = v[t[u]].next;
				a[j].del = 1;
			}
		}
		for(int i = 1; i <= n; i++)
			f[i] = i;
		for(int i = 1; i <= m; i++)
			if(!a[i].del)
			{
				int fx = find(a[i].u);
				int fy = find(a[i].v);
				if(fx != fy)
				{
					f[fx] = fy;
					ins(a[i].u, a[i].v);
					ins(a[i].v, a[i].u);
					a[i].del = -1;
				}
			}
		dfs(1);
		dfs(1, 1);
		build(1, 1, n);
		for(int i = 1; i <= m; i++)
			if(!a[i].del)
			{
				del(a[i].u, a[i].v);
			}
		for(int i = Q; i >= 1; i--)
			if(q[i].op == 1)
				del(q[i].u, q[i].v);
			else
				ans[++tot] = solve(q[i].u, q[i].v);
		printf("Case #%d:\n", cs);
		for(int i = tot; i >= 1; i--)
			printf("%d\n", ans[i]);
	}
	return 0;
}
